﻿namespace XCommerce.Base.Clases
{
    public class ControlObligatorioDto
    {
        public object Control { get; set; }
        public string NombreControl { get; set; }
    }
}
