﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using XCommerce.Base.Clases;
using XCommerce.Base.Helpers;

namespace XCommerce.Base.Formularios
{
    public partial class FormularioAbm : FormularioBase
    {
        protected TipoOperacion Operacion;
        protected long? EntidadId;
        public bool RealizoAlgunaOperacion;

        private readonly List<ControlObligatorioDto> _listaControlesObligatorios;

        public FormularioAbm()
        {
            InitializeComponent();
            RealizoAlgunaOperacion = false;
            _listaControlesObligatorios = new List<ControlObligatorioDto>();
        }

        public FormularioAbm(TipoOperacion operacion, long? entidadId = null)
            : this()
        {
            EntidadId = entidadId;
            Operacion = operacion;
            AsignarImagenBotones();
        }

        private void AsignarImagenBotones()
        {
            btnEjecutar.Image = Operacion == TipoOperacion.Delete
                ? Properties.Resources.Borrar
                : Properties.Resources.Guardar;
            btnLimpiar.Image = Properties.Resources.Limpiar4;
            btnSalir.Image = Properties.Resources.Salir;
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnLimpiar_Click(object sender, System.EventArgs e)
        {
            if (VerificarDatosParaCerrarLimpiarFormulario() && Operacion != TipoOperacion.Delete)
            {
                if (Mensaje.Mostrar("Hay Datos sin Guardar. Desea limpiar los datos?", Mensaje.Tipo.Pregunta) == DialogResult.Yes)
                {
                    LimpiarControles();
                }
            }
            
        }

        private void btnEjecutar_Click(object sender, System.EventArgs e)
        {
            EjecutarComando();
        }

        // ============================================================================== //

        public virtual void AgregarControlesObligatorios(object control, string nombreControl)
        {
            _listaControlesObligatorios.Add(new ControlObligatorioDto
            {
                Control = control,
                NombreControl = nombreControl
            });

            AsignarErrorProvider(control);
        }

        private void AsignarErrorProvider(object control)
        {
            if (control is TextBox)
            {
                ((TextBox) control).Validated += TextBox_Validated;
            }
            else if (control is RichTextBox)
            {
                ((RichTextBox)control).Validated += RichTextBox_Validated;
            }
        }

        private void TextBox_Validated(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                error.SetError(((TextBox)sender), String.Empty);
            }
            else
            {
                error.SetError(((TextBox)sender), $"El campo es obligatorio.");
            }
        }

        private void RichTextBox_Validated(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(((RichTextBox)sender).Text))
            {
                error.SetError(((RichTextBox)sender), String.Empty);
            }
            else
            {
                error.SetError(((RichTextBox)sender), $"El campo es obligatorio.");
            }
        }

        public virtual void FormularioAbm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (VerificarDatosParaCerrarLimpiarFormulario() 
                && Operacion != TipoOperacion.Delete
                && !RealizoAlgunaOperacion)
            {
                if (Mensaje.Mostrar("Hay Datos sin Guardar. Desea Salir?", Mensaje.Tipo.Pregunta) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        // ============================================================================== //

        private bool VerificarDatosParaCerrarLimpiarFormulario()
        {
            foreach (var objeto in _listaControlesObligatorios)
            {
                if (objeto.Control is TextBox)
                {
                    if (!string.IsNullOrEmpty(((TextBox)objeto.Control).Text))
                    {
                        return true;
                    }
                }
                else if (objeto.Control is RichTextBox)
                {
                    if (!string.IsNullOrEmpty(((RichTextBox)objeto.Control).Text))
                    {
                        return true;
                    }
                }
                else if (objeto.Control is NumericUpDown)
                {
                    if (!string.IsNullOrEmpty(((NumericUpDown)objeto.Control).Text))
                    {
                        return true;
                    }
                }
                else if (objeto.Control is ComboBox)
                {
                    if (((ComboBox)objeto.Control).Items.Count <= 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public virtual void LimpiarControles()
        {
        }

        public virtual void Inicializador()
        {
            switch (Operacion)
            {
                case TipoOperacion.Insert:
                    btnEjecutar.Text = "Guardar";
                    break;
                case TipoOperacion.Update:
                    btnEjecutar.Text = "Modificar";
                    CargarDatos(EntidadId);
                    break;
                case TipoOperacion.Delete:
                    btnEjecutar.Text = "Eliminar";
                    CargarDatos(EntidadId);
                    break;
            }
        }

        public virtual void CargarDatos(long? entidadId)
        {
        }

        private void EjecutarComando()
        {
            switch (Operacion)
            {
                case TipoOperacion.Insert:
                    if (VerificarDatosObligatorios())
                    {
                        EjecutarComandoInsert();
                        LimpiarControles();
                        RealizoAlgunaOperacion = true;
                    }
                    else
                    {
                        Mensaje.Mostrar("Faltan Datos Obligatorios", Mensaje.Tipo.Error);
                    }
                    break;
                case TipoOperacion.Update:
                    if (VerificarDatosObligatorios())
                    {
                        EjecutarComandoUpdate();
                        RealizoAlgunaOperacion = true;
                        Close();
                    }
                    else
                    {
                        Mensaje.Mostrar("Faltan Datos Obligatorios", Mensaje.Tipo.Error);
                    }
                    break;
                case TipoOperacion.Delete:
                    EjecutarComandoDelete();
                    RealizoAlgunaOperacion = true;
                    Close();
                    break;
            }
        }

        public virtual bool VerificarDatosObligatorios()
        {
            foreach (var objeto in _listaControlesObligatorios)
            {
                if (objeto.Control is TextBox)
                {
                    if (string.IsNullOrEmpty(((TextBox) objeto.Control).Text))
                    {
                        return false;
                    }
                }
                else if (objeto.Control is RichTextBox)
                {
                    if (string.IsNullOrEmpty(((RichTextBox)objeto.Control).Text))
                    {
                        return false;
                    }
                }
                else if (objeto.Control is NumericUpDown)
                {
                    if (string.IsNullOrEmpty(((NumericUpDown)objeto.Control).Text))
                    {
                        return false;
                    }
                }
                else if (objeto.Control is ComboBox)
                {
                    if (((ComboBox)objeto.Control).Items.Count <= 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public virtual void EjecutarComandoInsert()
        {
        }

        public virtual void EjecutarComandoUpdate()
        {
        }

        public virtual void EjecutarComandoDelete()
        {
        }

        private void FormularioAbm_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = $@"Usuario: {Identidad.ApyNomEmpleadoLogin}";
        }
    }
}
