﻿using System.Windows.Forms;
using XCommerce.Base.Helpers;

namespace XCommerce.Base.Formularios
{
    public partial class FormularioConsultaConDetalle : FormularioBase
    {
        protected long? EntidadId;
        protected object ObjetoSeleccionado;

        public FormularioConsultaConDetalle()
        {
            InitializeComponent();
            AsignarImagenesBotones();
            AsignarEventoEnterLeave();
            EntidadId = null;
            ObjetoSeleccionado = null;
        }

        public FormularioConsultaConDetalle(string titulo)
            : this()
        {
            this.lblTitulo.Text = titulo;
        }

        private void AsignarImagenesBotones()
        {
            
        }

        private void AsignarEventoEnterLeave()
        {
            txtBuscar.Enter += ColorAlRecibirFoco;
            txtBuscar.Leave += ColorAlPerderFoco;
        }

        private void FormularioConsulta_Load(object sender, System.EventArgs e)
        {
            EjecutarEventoLoad();
            lblUsuario.Text = $"Usuario: {Identidad.ApyNomEmpleadoLogin}";
        }

        private void btnNuevo_Click(object sender, System.EventArgs e)
        {
            if (EjecutarNuevaEntidad())
            {
                ActualizarDatos(string.Empty);
            }
        }

        private void btnEliminar_Click(object sender, System.EventArgs e)
        {
            if (VerificarSiExistenDatosCargados())
            {
                if (VerificarSiSeleccionoAlgunRegistro())
                {
                    if (EjecutarEliminarEntidad(EntidadId))
                    {
                        ActualizarDatos(string.Empty);
                    }
                }
                else
                {
                    Mensaje.Mostrar("Por Favor seleccione un Registro.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("No hay Datos cargados.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnModificar_Click(object sender, System.EventArgs e)
        {
            if (VerificarSiExistenDatosCargados())
            {
                if (VerificarSiSeleccionoAlgunRegistro())
                {
                    if (EjecutarModificarEntidad(EntidadId))
                    {
                        ActualizarDatos(string.Empty);
                    }
                }
                else
                {
                    Mensaje.Mostrar("Por Favor seleccione un Registro.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("No hay Datos cargados.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnActualizar_Click(object sender, System.EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }

        private void btnImprimir_Click(object sender, System.EventArgs e)
        {
            if (VerificarSiExistenDatosCargados())
            {
                EjecutarImprimirEntidades();
            }
            else
            {
                Mensaje.Mostrar("No hay Datos cargados.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, System.EventArgs e)
        {
            ActualizarDatos(txtBuscar.Text);
        }

        public virtual void dgvGrilla_RowEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (VerificarSiExistenDatosCargados())
            {
                EntidadId = (long)dgvGrilla["Id", e.RowIndex].Value;
                ObjetoSeleccionado = dgvGrilla.Rows[e.RowIndex].DataBoundItem;
            }
            else
            {
                EntidadId = null;
                ObjetoSeleccionado = null;
            }
        }

        //==============================================================//

        public virtual void EjecutarEventoLoad()
        {
            ActualizarDatos(string.Empty);
        }

        public virtual bool VerificarSiSeleccionoAlgunRegistro()
        {
            return EntidadId.HasValue;
        }

        public virtual bool VerificarSiExistenDatosCargados()
        {
            return dgvGrilla.RowCount > 0;
        }

        public virtual void ActualizarDatos(string empty)
        {
        }

        public virtual bool EjecutarEliminarEntidad(long? entidadId)
        {
            return false;
        }

        public virtual void EjecutarImprimirEntidades()
        {
        }

        public virtual bool EjecutarModificarEntidad(long? entidadId)
        {
            return false;
        }

        public virtual bool EjecutarNuevaEntidad()
        {
            return false;
        }

        private void txtBuscar_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if ((char) Keys.Enter == e.KeyChar)
            {
                btnBuscar.PerformClick();
            }
        }
    }
}
