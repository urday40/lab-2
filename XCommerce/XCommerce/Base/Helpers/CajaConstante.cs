﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCommerce.Base.Helpers
{
    public static class CajaConstante
    {
        public static long CajaId { get; set; }

        public static decimal MontoApertura { get; set; }
    }
}
