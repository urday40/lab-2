//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XCommerce.AccesoDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ingrediente
    {
        public long Id { get; set; }
        public long ArticuloPadreId { get; set; }
        public long ArticuloHijoId { get; set; }
        public decimal Cantidad { get; set; }
    
        public virtual Articulo ArticuloPadre { get; set; }
        public virtual Articulo ArticuloHijo { get; set; }
    }
}
