//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XCommerce.AccesoDatos
{
    using System;
    
    public enum EstadoDelivery : int
    {
        EnProceso = 1,
        Enviado = 2,
        Cancelado = 3,
        Rechazado = 4,
        Pagado = 5
    }
}
