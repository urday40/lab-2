﻿using System;
using System.Windows.Forms;
using XCommerce.Base.Helpers;
using XCommerce.Core;
using XCommerce.LogicaNegocio.Seguridad;

namespace XCommerce
{
    public partial class Principal : Form
    {
        private readonly SeguridadServicio _seguridadServicio;

        public Principal()
            : this(new SeguridadServicio())
        {
            InitializeComponent();

            // Asigno el Click para Cambiar el Password
            ctrolEmpleado.cambiarContraseñaToolStripMenuItem.Click += cambiarPassword_Click;
            ctrolEmpleado.cerrarSesiónToolStripMenuItem.Click += cerrarSesion_Click;
        }

        public Principal(SeguridadServicio seguridadServicio)
        {
            _seguridadServicio = seguridadServicio;
        }

        private void AsignarDatosEmpleadoLogin()
        {
            ctrolEmpleado.imgFoto.Image = Identidad.Foto;
            ctrolEmpleado.lblEmpleadoLogin.Text = Identidad.ApyNomEmpleadoLogin;
        }

        private void QuitarDatosEmpleadoLogin()
        {
            ctrolEmpleado.imgFoto.Image = null;
            ctrolEmpleado.lblEmpleadoLogin.Text = string.Empty;
        }

        private void cambiarPassword_Click(object sender, System.EventArgs e)
        {
            var fCambiarPassword = new _00025_CambiarPassword();
            fCambiarPassword.ShowDialog();
        }

        private void cerrarSesion_Click(object sender, EventArgs e)
        {
            imgEmpresa.Image = null;
            lblEmpresa.Text = string.Empty;

            QuitarDatosEmpleadoLogin();

            var fLogin = new _00000_Login();
            fLogin.ShowDialog();
            if (fLogin.PuedeAccederAlSistema)
            {
                InicioPantallaPrincipal();
            }
            else
            {
                Application.Exit();
            }
        }

        private void consultaDeEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00001_Empleado();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00002_ABM_Empleado(TipoOperacion.Insert,null);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00003_Usuario();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDeCondicionIvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00004_CondicionDeIva();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaCondiciónIvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00005_ABM_CondicionDeIva(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00006_Empresa();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00007_ABM_Empresa(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            imgEmpresa.Image = null;
            lblEmpresa.Text = string.Empty;
            QuitarDatosEmpleadoLogin();

            InicioPantallaPrincipal();
        }

        private void InicioPantallaPrincipal()
        {
            if (Identidad.UsuarioLoginId != 0)
            {
                AsignarDatosEmpleadoLogin();

                var fSeleccionEmpresa = new _00010_EmpresaParaTrabajar(Identidad.EmpleadoLoginId);
                fSeleccionEmpresa.ShowDialog();

                imgEmpresa.Image = Identidad.LogoEmpresa;
                lblEmpresa.Text = Identidad.Empresa;
            }
        }

        private void consultaDeSalasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00011_Sala();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaSalaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00012_ABM_Sala(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void copnsultaDeMesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00013_Mesa();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaMesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00014_ABM_Mesa(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnVentaSalon_Click(object sender, EventArgs e)
        {
            var abrirCaja = new _00041_AbrirCaja();
            abrirCaja.ShowDialog();

            var fPuntoVentaSalon = new _00015_VentaSalon();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fPuntoVentaSalon, Identidad.UsuarioLogin))
            {
                fPuntoVentaSalon.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            var fPuntoVentaDelivery = new _00017_Delivery();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fPuntoVentaDelivery, Identidad.UsuarioLogin))
            {
                fPuntoVentaDelivery.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void formulariosVentanasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fFormularios = new _00018_Formulario();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularios, Identidad.UsuarioLogin))
            {
                fFormularios.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaPerfilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != -0)
            {
                var formulario = new _00019_Perfil();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoPerfilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00020_ABM_Perfil(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaTipoComprobanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00026_TipoComprobante();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoTipoDeComprobanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00027_ABM_TipoComprobante(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDeMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00028_Marca();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00029_ABM_Marca(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDeRubroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00030_Rubro();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoRubroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00031_ABM_Rubro(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDeSubRubroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00032_SubRubro();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoSubRubroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00033_ABM_SubRubro(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaListaPrecioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00039_ListaPrecio();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevaListaPrecioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00040_ABM_ListaPrecio(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDepositoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var formulario = new _00037_Deposito();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
                {
                    formulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoDepositoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00038_ABM_Deposito(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void configuracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormulario = new _00036_ConfiguracionSistema();
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormulario, Identidad.UsuarioLogin))
                {
                    fFormulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void consultaDeArticuloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formulario = new _00034_Articulo();
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(formulario, Identidad.UsuarioLogin))
            {
                formulario.ShowDialog();
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void nuevoArticuloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var nuevoArticulo = new _00035_ABM_Articulo(TipoOperacion.Insert);
            nuevoArticulo.ShowDialog();
        }

        private void asignarEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var asignarEmpleadoEmpresa = new _00008_AsignarEmpleadoEmpresa(Identidad.EmpresaId);
            asignarEmpleadoEmpresa.ShowDialog();
        }

        private void quitarEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var asignarEmpleadoEmpresa = new _00009_QuitarEmpleadoEmpresa(Identidad.EmpresaId);
            asignarEmpleadoEmpresa.ShowDialog();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var caja = new _00041_AbrirCaja();
            caja.ShowDialog();
            var quiosco = new _00042_PuntoVentaPorKiosco();
            quiosco.ShowDialog();
        }
    }
}
