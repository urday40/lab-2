﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Deposito.DTOs;

namespace XCommerce.LogicaNegocio.Deposito
{
    public class DepositoServicio
    {
        public long Insertar(DepositoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoDeposito = new AccesoDatos.Deposito
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminado = dto.EstaEliminado,
                    EmpresaId = dto.EmpresaId
                };

                context.Depositos.Add(nuevoDeposito);
                context.SaveChanges();

                return nuevoDeposito.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var depositoEliminar = context.Depositos
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (depositoEliminar == null) throw new Exception("No se encontro el Deposito");

                depositoEliminar.EstaEliminado = !depositoEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(DepositoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var depositoModificar = context.Depositos
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (depositoModificar == null) throw new Exception("No se encontro el Deposito");

                depositoModificar.Codigo = dto.Codigo;
                depositoModificar.Descripcion = dto.Descripcion;
                depositoModificar.EmpresaId = dto.EmpresaId;

                context.SaveChanges();
            }
        }

        public DepositoDto ObtenerPorId(long tipoComprobanteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var deposito = context.Depositos
                    .FirstOrDefault(x => x.Id == tipoComprobanteId);

                if (deposito == null) throw new Exception("No se encontro el Deposito");

                return new DepositoDto
                {
                    Id = deposito.Id,
                    Codigo = deposito.Codigo,
                    Descripcion = deposito.Descripcion,
                    EstaEliminado = deposito.EstaEliminado,
                    EmpresaId = deposito.EmpresaId
                };
            }
        }

        public IEnumerable<DepositoDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Depositos
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && x.EmpresaId == empresaId
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new DepositoDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado,
                        EmpresaId = x.EmpresaId
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Depositos.AsNoTracking().Any(x => x.EmpresaId == empresaId)
                    ? context.Depositos.Where(x => x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long empresaId, int codigo, string descripcion, long? depositoId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (depositoId.HasValue)
                {
                    return context.Depositos
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && x.Id != depositoId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
                else
                {
                    return context.Depositos
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
            }
        }
    }
}
