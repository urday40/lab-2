﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Marca.DTOs;

namespace XCommerce.LogicaNegocio.Marca
{
    public class MarcaServicio
    {
        public long Insertar(MarcaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoMarca = new AccesoDatos.Marca
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminada = dto.EstaEliminada,
                    EmpresaId = dto.EmpresaId
                };

                context.Marcas.Add(nuevoMarca);
                context.SaveChanges();

                return nuevoMarca.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var marcaEliminar = context.Marcas
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (marcaEliminar == null) throw new Exception("No se encontro la Marca");

                marcaEliminar.EstaEliminada = !marcaEliminar.EstaEliminada;
                context.SaveChanges();
            }
        }

        public void Modificar(MarcaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var marcaModificar = context.Marcas
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (marcaModificar == null) throw new Exception("No se encontro la Marca");

                marcaModificar.Codigo = dto.Codigo;
                marcaModificar.Descripcion = dto.Descripcion;
                marcaModificar.EmpresaId = dto.EmpresaId;

                context.SaveChanges();
            }
        }

        public MarcaDto ObtenerPorId(long tipoComprobanteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var marca = context.Marcas
                    .FirstOrDefault(x => x.Id == tipoComprobanteId);

                if (marca == null) throw new Exception("No se encontro la Marca");

                return new MarcaDto
                {
                    Id = marca.Id,
                    Codigo = marca.Codigo,
                    Descripcion = marca.Descripcion,
                    EstaEliminada = marca.EstaEliminada,
                    EmpresaId = marca.EmpresaId
                };
            }
        }

        public IEnumerable<MarcaDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Marcas
                    .AsNoTracking()
                    .Where(x => x.EstaEliminada == estado
                                && x.EmpresaId == empresaId
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new MarcaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminada = x.EstaEliminada,
                        EmpresaId = x.EmpresaId
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Marcas.AsNoTracking().Any(x => x.EmpresaId == empresaId)
                    ? context.Marcas.Where(x => x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long empresaId, int codigo, string descripcion, long? marcaId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (marcaId.HasValue)
                {
                    return context.Marcas
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && x.Id != marcaId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
                else
                {
                    return context.Marcas
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
            }
        }
    }
}
