﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Caja.DTOs;

namespace XCommerce.LogicaNegocio.Caja
{
    public class CajaServicio
    {
        public long Insert(CajaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaCaja = new AccesoDatos.Caja
                {
                    UsuarioAperturaId = dto.UsuarioAperturaId,

                    UsuarioCierreId = dto.UsuarioCierreId,

                    MontoApertura = dto.MontoApertura,

                    MontoCierre =
                    dto.MontoCierre,

                    FechaApertura = dto.FechaApertura,

                    FechaCierre = dto.FechaCierre,

                    MontoSistema = dto.MontoSistema,

                    Diferencia = dto.Diferencia,

                    EmpresaId = dto.EmpresaId
                };

                context.Cajas.Add(nuevaCaja);

                context.SaveChanges();

                return nuevaCaja.Id;
            }
        }

        public void Modificar(CajaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var modificarCaja = context.Cajas.FirstOrDefault(x => x.Id == dto.Id);

                if (modificarCaja == null)

                    throw new Exception("No se encontro la Caja solicitada.");
                modificarCaja.UsuarioAperturaId = dto.UsuarioAperturaId;
                modificarCaja.UsuarioCierreId = dto.UsuarioCierreId;
                modificarCaja.MontoApertura = dto.MontoApertura;
                modificarCaja.MontoCierre = dto.MontoCierre;
                modificarCaja.FechaApertura = dto.FechaApertura;
                modificarCaja.FechaCierre = dto.FechaCierre;
                modificarCaja.MontoSistema = dto.MontoSistema;
                modificarCaja.Diferencia = dto.Diferencia;
                modificarCaja.EmpresaId = dto.EmpresaId;

                context.SaveChanges();
            }
        }

        public CajaDto ObtenerPorId(long cajaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var caja = context.Cajas.FirstOrDefault(x => x.Id == cajaId);

                if (caja == null) throw new Exception("No se encontro la Caja");

                return new CajaDto
                {
                    Id = caja.Id,
                    UsuarioAperturaId = caja.UsuarioAperturaId,

                    MontoApertura = caja.MontoApertura,

                    MontoSistema = caja.MontoSistema,

                    FechaApertura = caja.FechaApertura,

                    EmpresaId = caja.EmpresaId
                };
            }
        }

        public IEnumerable<CajaDto> Obtener(long empresaId, string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                decimal.TryParse(cadenaBuscar, out var monto);

                return context.Cajas.AsNoTracking()
                        .Where(x => (x.EmpresaId == empresaId && x.MontoApertura == monto) ||(x.EmpresaId == empresaId && x.MontoCierre == monto ))
                    .Select(x=> new CajaDto
                    {
                        Id =x.Id,
                        EmpresaId = x.EmpresaId,
                        MontoApertura = x.MontoApertura,
                        FechaApertura = x.FechaApertura,
                        MontoCierre = x.MontoCierre,
                        Diferencia = x.Diferencia,
                        FechaCierre = x.FechaCierre,
                        MontoSistema = x.MontoSistema,
                        UsuarioAperturaId = x.UsuarioAperturaId,
                        UsuarioCierreId = x.UsuarioCierreId
                    }).ToList();
            }
        }
        public IEnumerable<CajaDto> ObtenerPorEmpresa(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {


                return context.Cajas.AsNoTracking()
                    .Where(x => x.EmpresaId == empresaId )
                    .Select(x => new CajaDto
                    {
                        Id = x.Id,
                        EmpresaId = x.EmpresaId,
                        MontoApertura = x.MontoApertura,
                        FechaApertura = x.FechaApertura,
                        MontoCierre = x.MontoCierre,
                        Diferencia = x.Diferencia,
                        FechaCierre = x.FechaCierre,
                        MontoSistema = x.MontoSistema,
                        UsuarioAperturaId = x.UsuarioAperturaId,
                        UsuarioCierreId = x.UsuarioCierreId
                    }).ToList();
            }
        }

        public bool VerificarSiexiste(long empresaId, long? cajaId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (cajaId.HasValue)
                {
                    return context.Cajas.AsNoTracking().Any(x => x.EmpresaId == empresaId && x.Id != cajaId);
                }
                else
                {
                    return context.Cajas.AsNoTracking().Any(x => x.EmpresaId == empresaId);
                }
            }
        }
    }
}
