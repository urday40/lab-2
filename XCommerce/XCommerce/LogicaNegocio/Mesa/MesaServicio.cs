﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Mesa.DTOs;

namespace XCommerce.LogicaNegocio.Mesa
{
    public class MesaServicio
    {
        public long Insertar(MesaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaMesa = new AccesoDatos.Mesa
                {
                    SalaId = dto.SalaId,
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstadoMesa = dto.EstadoMesa
                };

                context.Mesas.Add(nuevaMesa);
                context.SaveChanges();

                return nuevaMesa.Id;
            }
        }

        public void Eliminar(long MesaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var mesaEliminar = context.Mesas
                    .FirstOrDefault(x => x.Id == MesaId);

                if (mesaEliminar == null) throw new Exception("No se encontro la Mesa");

                mesaEliminar.EstadoMesa = EstadoMesa.FueraServicio;

                context.SaveChanges();
            }
        }

        public void Modificar(MesaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var mesaModificar = context.Mesas
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (mesaModificar == null) throw new Exception("No se encontro la Mesa");

                mesaModificar.Codigo = dto.Codigo;
                mesaModificar.Descripcion = dto.Descripcion;
                mesaModificar.SalaId = dto.SalaId;
                mesaModificar.EstadoMesa = dto.EstadoMesa;

                context.SaveChanges();
            }
        }

        public MesaDto ObtenerPorId(long MesaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var mesa = context.Mesas
                    .FirstOrDefault(x => x.Id == MesaId);

                if (mesa == null) throw new Exception("No se encontro la Condicion de Iva");

                return new MesaDto
                {
                    Id = mesa.Id,
                    Codigo = mesa.Codigo,
                    Descripcion = mesa.Descripcion,
                    SalaId = mesa.SalaId,
                    EstadoMesa = mesa.EstadoMesa,
                    Sala = mesa.Sala.Descripcion
                };
            }
        }

        public IEnumerable<MesaDto> ObtenerTodas(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Mesas
                    .AsNoTracking()
                    .Where(x => x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo)
                    .Select(x => new MesaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstadoMesa = x.EstadoMesa,
                        Sala = x.Sala.Descripcion,
                        SalaId = x.SalaId
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public IEnumerable<MesaDto> Obtener(long salaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Mesas
                    .Include(x => x.Comprobantes_Salon)
                    .AsNoTracking()
                    .Where(x => x.SalaId == salaId)
                    .Select(x => new MesaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstadoMesa = x.EstadoMesa,
                        Sala = x.Sala.Descripcion,
                        SalaId = x.SalaId,
                        Monto = x.EstadoMesa == EstadoMesa.Abierta ? x.Comprobantes_Salon.FirstOrDefault(c => c.MesaId == x.Id && c.Estado == EstadoSalon.EnProceso).Total : 1
  
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Mesas.AsNoTracking().Any()
                    ? context.Mesas.Max(x => x.Codigo) + 1
                    : 1;
            }
        }
    }
}
