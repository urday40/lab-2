﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Banco.DTOs;

namespace XCommerce.LogicaNegocio.Banco
{
    public class BancoServicio
    {
        public IEnumerable<BancoDto> Get(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int.TryParse(cadenaBuscar, out var codigo);

                return context.Bancos
                    .AsNoTracking()
                    .Where(x => x.Codigo == codigo
                                || x.Descripcion.Contains(cadenaBuscar)
                                || x.EstaEliminado)
                    .Select(x => new BancoDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado

                    }).ToList();
            }
        }
        public IEnumerable<BancoDto> GetNonActive(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int.TryParse(cadenaBuscar, out var codigo);

                return context.Bancos
                    .AsNoTracking()
                    .Where(x => x.Codigo == codigo
                                || x.Descripcion.Contains(cadenaBuscar)
                                || x.EstaEliminado==false)
                    .Select(x => new BancoDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado

                    }).ToList();
            }
        }

        public BancoDto GetById(long entity)
        {
            using (var context = new ModeloDatosContainer())
            {
                var banco = context.Bancos.FirstOrDefault(x => x.Id == entity);

                if (banco == null)
                    throw new Exception("El banco no fue encontrado");

                return new BancoDto
                {
                    Id = banco.Id,
                    Descripcion = banco.Descripcion,
                    Codigo = banco.Codigo,
                    EstaEliminado = banco.EstaEliminado
                };

            }
        }

        public long Insert(BancoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoBanco = new AccesoDatos.Banco
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminado = false
                };

                context.Bancos.Add(nuevoBanco);
                context.SaveChanges();

                return nuevoBanco.Id;
            }
        }

        public void Delete(long bancoId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var bancoDel = context.Bancos.FirstOrDefault(x => x.Id == bancoId);

                if(bancoDel == null)throw new Exception("El banco no fue encontrado");

                bancoDel.EstaEliminado = true;

                context.SaveChanges();

            }
        }

        public void Update(BancoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var entidad = context.Bancos.FirstOrDefault(x => x.Id == dto.Id);

                if(entidad == null)throw new Exception("No se encontro el banco");
                entidad.Codigo = dto.Codigo;
                entidad.Descripcion = dto.Descripcion;

                context.SaveChanges();


            }
        }

        


    }
}
