﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Empleado.DTOs;
using XCommerce.LogicaNegocio.Empresa.DTOs;
using XCommerce.LogicaNegocio.EmpresaEmpleado.Clases;

namespace XCommerce.LogicaNegocio.EmpresaEmpleado
{
    public class EmpresaEmpleadoServicio
    {
        public ICollection<EmpleadoDto> ObtenerEmpleadosNoAsignados(long empresaId, string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int legajo = -1;
                int.TryParse(cadenaBuscar, out legajo);

                var empleadosAsignados = context.Personas.OfType<AccesoDatos.Empleado>()
                    .AsNoTracking()
                    .Include(x => x.Empresas)
                    .Where(x => !x.EstaEliminado
                                && x.Empresas.Any(e => e.Id == empresaId))
                    .Select(x => new EmpleadoDto
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Celular = x.Celular,
                        Dni = x.Dni,
                        Domicilio = x.Domicilio,
                        Email = x.Email,
                        FechaIngreso = x.FechaIngreso,
                        FechaNacimiento = x.FechaNacimiento,
                        Legajo = x.Legajo,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        Foto = x.Foto,
                        Eliminado = x.EstaEliminado
                    })
                    .ToList();

                var empleados = context.Personas.OfType<AccesoDatos.Empleado>()
                    .AsNoTracking()
                    .Include(x => x.Empresas)
                    .Where(x => !x.EstaEliminado)
                    .Select(x => new EmpleadoDto
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Celular = x.Celular,
                        Dni = x.Dni,
                        Domicilio = x.Domicilio,
                        Email = x.Email,
                        FechaIngreso = x.FechaIngreso,
                        FechaNacimiento = x.FechaNacimiento,
                        Legajo = x.Legajo,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        Foto = x.Foto,
                        Eliminado = x.EstaEliminado
                    })
                    .ToList();

                return empleados.Except(empleadosAsignados, new EmpresaEmpleadoCompare())
                    .Where(x => x.Legajo == legajo
                                || x.Apellido.Contains(cadenaBuscar)
                                || x.Nombre.Contains(cadenaBuscar)
                                || x.Dni == cadenaBuscar)
                    .OrderBy(x => x.Apellido).ThenBy(x => x.Nombre)
                    .ToList();
            }
        }
        
        public void AsignarEmpleado(long empresaId, List<EmpleadoDto> listaEmpleadosNoAsignados)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empresa = context.Empresas.FirstOrDefault(x => x.Id == empresaId);

                if(empresa == null) throw new Exception("Ocurrió un error al obtener la Empresa");

                foreach (var emple in listaEmpleadosNoAsignados)
                {
                    var empleado = context.Personas.OfType<AccesoDatos.Empleado>()
                        .FirstOrDefault(x => x.Id == emple.Id);

                    if(empleado == null) throw new Exception("Ocurrió un error al obtener un empleado");

                    empresa.Empleados.Add(empleado);
                }

                context.SaveChanges();
            }

            
        }

        // ======================================================================================== //

        public ICollection<EmpleadoDto> ObtenerEmpleadosAsignados(long empresaId, string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int legajo = -1;
                int.TryParse(cadenaBuscar, out legajo);

                var empleadosAsignados = context.Personas.OfType<AccesoDatos.Empleado>()
                    .AsNoTracking()
                    .Include(x => x.Empresas)
                    .Where(x => !x.EstaEliminado
                                && x.Empresas.Any(e => e.Id == empresaId)
                                && (x.Legajo == legajo
                                || x.Apellido.Contains(cadenaBuscar)
                                || x.Nombre.Contains(cadenaBuscar)
                                || x.Dni == cadenaBuscar))
                    .Select(x => new EmpleadoDto
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Celular = x.Celular,
                        Dni = x.Dni,
                        Domicilio = x.Domicilio,
                        Email = x.Email,
                        FechaIngreso = x.FechaIngreso,
                        FechaNacimiento = x.FechaNacimiento,
                        Legajo = x.Legajo,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        Foto = x.Foto,
                        Eliminado = x.EstaEliminado
                    })
                    .OrderBy(x => x.Apellido).ThenBy(x => x.Nombre)
                    .ToList();

                return empleadosAsignados;
            }
        }

        public void QuitarEmpleado(long empresaId, List<EmpleadoDto> listaEmpleadosAsignados)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empresa = context.Empresas.FirstOrDefault(x => x.Id == empresaId);

                if (empresa == null) throw new Exception("Ocurrió un error al obtener la Empresa");

                foreach (var emple in listaEmpleadosAsignados)
                {
                    var empleado = context.Personas.OfType<AccesoDatos.Empleado>()
                        .FirstOrDefault(x => x.Id == emple.Id);

                    if (empleado == null) throw new Exception("Ocurrió un error al obtener un empleado");

                    empresa.Empleados.Remove(empleado);
                }

                context.SaveChanges();
            }
        }

        // ======================================================================================== //

        public ICollection<EmpresaDto> ObtenerEmpresasDelEmpleado(long empleadoId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Empresas
                    .AsNoTracking()
                    .Where(x => x.Empleados.Any(e=>e.Id == empleadoId))
                    .Select(x => new EmpresaDto
                    {
                        Id = x.Id,
                        RazonSocial = x.RazonSocial,
                        NombreFantasia = x.NombreFantasia,
                        Domicilio = x.Domicilio,
                        Email = x.Mail,
                        Telefono = x.Telefono,
                        Logo = x.Logo,
                        CondicionIvaId = x.CondicionIvaId,
                        Sucursal = x.Sucursal,
                        Cuit = x.Cuit
                    }).OrderBy(x => x.Sucursal).ThenBy(x => x.RazonSocial)
                    .ToList();
            }
        }
    }
}
