﻿using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Configuracion.DTOs;

namespace XCommerce.LogicaNegocio.Configuracion
{
    public class ConfiguracionServicio
    {
        public ConfiguracionDto Obtener(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var config = context.Configuraciones
                    .AsNoTracking()
                    .FirstOrDefault(x => x.EmpresaId == empresaId);

                if (config != null)
                {
                    return new ConfiguracionDto
                    {
                        Id = config.Id,
                        EmpresaId = config.EmpresaId,
                        TipoComprobantePorDefectoId = config.TipoComprobantePorDefectoId,
                        ListaPrecioPorDefectoId = config.ListaPrecioPorDefectoId,
                        DepositoPorDefectoId = config.DepositoPorDefectoId,
                        PuestoDeCajaSeparado = config.PuestoDeCajaSeparado,
                        SeCobraCubiertos = config.SeCobraCubiertos,
                        MontoPorCubierto = config.MontoPorCubierto,
                        GrabarProductoEnTodosDepositos = config.GrabarProductoEnTodosDepositos
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public void Insertar(ConfiguracionDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var config = context.Configuraciones
                    .FirstOrDefault(x => x.EmpresaId == dto.EmpresaId);

                if (config == null)
                {
                    var nuevaConfig = new AccesoDatos.Configuracion
                    {
                        EmpresaId = dto.EmpresaId,
                        TipoComprobantePorDefectoId = dto.TipoComprobantePorDefectoId,
                        ListaPrecioPorDefectoId = dto.ListaPrecioPorDefectoId,
                        DepositoPorDefectoId = dto.DepositoPorDefectoId,
                        PuestoDeCajaSeparado = dto.PuestoDeCajaSeparado,
                        SeCobraCubiertos = dto.SeCobraCubiertos,
                        MontoPorCubierto = dto.MontoPorCubierto,
                        GrabarProductoEnTodosDepositos = dto.GrabarProductoEnTodosDepositos
                    };

                    context.Configuraciones.Add(nuevaConfig);
                }
                else
                {
                    config.EmpresaId = dto.EmpresaId;
                    config.TipoComprobantePorDefectoId = dto.TipoComprobantePorDefectoId;
                    config.ListaPrecioPorDefectoId = dto.ListaPrecioPorDefectoId;
                    config.DepositoPorDefectoId = dto.DepositoPorDefectoId;
                    config.PuestoDeCajaSeparado = dto.PuestoDeCajaSeparado;
                    config.SeCobraCubiertos = dto.SeCobraCubiertos;
                    config.MontoPorCubierto = dto.MontoPorCubierto;
                    config.GrabarProductoEnTodosDepositos = dto.GrabarProductoEnTodosDepositos;
                }

                context.SaveChanges();
            }
        }
    }
}
