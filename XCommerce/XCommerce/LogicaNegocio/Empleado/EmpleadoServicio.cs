﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Empleado.DTOs;

namespace XCommerce.LogicaNegocio.Empleado
{
    public class EmpleadoServicio
    {
        public long Insertar(EmpleadoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoEmpleado = new AccesoDatos.Empleado
                {
                    Apellido = dto.Apellido,
                    Celular = dto.Celular,
                    Dni = dto.Dni,
                    Domicilio = dto.Domicilio,
                    Email = dto.Email,
                    FechaIngreso = dto.FechaIngreso,
                    FechaNacimiento = dto.FechaNacimiento,
                    Legajo = dto.Legajo,
                    Nombre = dto.Nombre,
                    Telefono = dto.Telefono,
                    Foto = dto.Foto,
                    EstaEliminado = false,
                };

                context.Personas.Add(nuevoEmpleado);
                context.SaveChanges();

                return nuevoEmpleado.Id;
            }
        }

        public void Eliminar(long empleadoId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empleadoEliminar = context.Personas.OfType<AccesoDatos.Empleado>()
                    .FirstOrDefault(x => x.Id == empleadoId);

                if (empleadoEliminar == null) throw new Exception("No se encontro el empleado");

                empleadoEliminar.EstaEliminado = !empleadoEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(EmpleadoDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empleadoModificar = context.Personas.OfType<AccesoDatos.Empleado>()
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (empleadoModificar == null) throw new Exception("no se encontro el empleado");

                empleadoModificar.Apellido = dto.Apellido;
                empleadoModificar.Celular = dto.Celular;
                empleadoModificar.Dni = dto.Dni;
                empleadoModificar.Domicilio = dto.Domicilio;
                empleadoModificar.Email = dto.Email;
                empleadoModificar.FechaIngreso = dto.FechaIngreso;
                empleadoModificar.FechaNacimiento = dto.FechaNacimiento;
                empleadoModificar.Legajo = dto.Legajo;
                empleadoModificar.Nombre = dto.Nombre;
                empleadoModificar.Telefono = dto.Telefono;
                empleadoModificar.Foto = dto.Foto;

                context.SaveChanges();
            }
        }

        public EmpleadoDto ObtenerPorId(long empleadoId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empleado = context.Personas.OfType<AccesoDatos.Empleado>()
                    .FirstOrDefault(x => x.Id == empleadoId);

                if (empleado == null) throw new Exception("No se encontro el empleado");

                return new EmpleadoDto
                {
                    Id = empleado.Id,
                    Apellido = empleado.Apellido,
                    Celular = empleado.Celular,
                    Dni = empleado.Dni,
                    Domicilio = empleado.Domicilio,
                    Email = empleado.Email,
                    FechaIngreso = empleado.FechaIngreso,
                    FechaNacimiento = empleado.FechaNacimiento,
                    Legajo = empleado.Legajo,
                    Nombre = empleado.Nombre,
                    Telefono = empleado.Telefono,
                    Foto = empleado.Foto,
                    Eliminado = empleado.EstaEliminado
                };
            }
        }

        public IEnumerable<EmpleadoDto> Obtener(string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int legajo = -1;
                int.TryParse(cadenaBuscar, out legajo);

                return context.Personas.OfType<AccesoDatos.Empleado>()
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && (x.Apellido.Contains(cadenaBuscar)
                                    || x.Nombre.Contains(cadenaBuscar)
                                    || x.Dni == cadenaBuscar
                                    || x.Legajo == legajo))
                    .Select(x => new EmpleadoDto
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Celular = x.Celular,
                        Dni = x.Dni,
                        Domicilio = x.Domicilio,
                        Email = x.Email,
                        FechaIngreso = x.FechaIngreso,
                        FechaNacimiento = x.FechaNacimiento,
                        Legajo = x.Legajo,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        Foto = x.Foto,
                        Eliminado = x.EstaEliminado
                    }).OrderBy(x => x.Apellido).ThenBy(x => x.Nombre)
                    .ToList();
            }
        }

        public int ObtenerSiguienteLegajo()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Personas.OfType<AccesoDatos.Empleado>().Any()
                    ? context.Personas.OfType<AccesoDatos.Empleado>().Max(x => x.Legajo) + 1
                    : 1;
            }
        }
    }
}
