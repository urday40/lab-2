﻿using System.Collections.Generic;
using XCommerce.LogicaNegocio.UsuarioPerfil.DTOs;

namespace XCommerce.LogicaNegocio.FormularioPerfil.DTOs
{
    public class FormularioPerfilCompare : IEqualityComparer<FormularioPerfilDto>
    {
        public bool Equals(FormularioPerfilDto x, FormularioPerfilDto y)
        {
            if (object.ReferenceEquals(x, y)) return true;

            if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null))
                return false;

            return x.FormularioId == y.FormularioId;
        }

        public int GetHashCode(FormularioPerfilDto obj)
        {
            return obj.FormularioId.GetHashCode();
        }
    }
}
