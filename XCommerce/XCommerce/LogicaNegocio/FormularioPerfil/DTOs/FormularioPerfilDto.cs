﻿namespace XCommerce.LogicaNegocio.FormularioPerfil.DTOs
{
    public class FormularioPerfilDto
    {
        public bool Item { get; set; }

        public long FormularioId { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
