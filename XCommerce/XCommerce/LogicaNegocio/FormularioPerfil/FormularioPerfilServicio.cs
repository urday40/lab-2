﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.FormularioPerfil.DTOs;

namespace XCommerce.LogicaNegocio.FormularioPerfil
{
    public class FormularioPerfilServicio
    {
        public IEnumerable<FormularioPerfilDto> ObtenerFormulariosNoAsignados(string cadenaBuscar, long perfilId)
        {
            using (var context = new ModeloDatosContainer())
            {
                // Obtengo TODOS los Formularios
                var usuarios = context.Formularios
                    .AsNoTracking()
                    .Select(x => new FormularioPerfilDto
                    {
                        FormularioId = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        Item = false
                    }).ToList();

                // Obtener los Formularios Asginados
                var usuariosAsignados = context.Formularios
                    .Include("Perfiles")
                    .AsNoTracking()
                    .Where(x => x.Perfiles.Any(p => p.Id == perfilId))
                    .Select(x => new FormularioPerfilDto
                    {
                        FormularioId = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        Item = false
                    }).ToList();

                var resultado = usuarios
                    .Except(usuariosAsignados, new FormularioPerfilCompare())
                    .Where(x => x.Descripcion.Contains(cadenaBuscar)
                                || x.Codigo == cadenaBuscar)
                    .ToList();

                return resultado;
            }
        }

        public IEnumerable<FormularioPerfilDto> ObtenerFormulariosAsignados(string cadenaBuscar, long perfilId)
        {
            using (var context = new ModeloDatosContainer())
            {

                // Obtener los Formularios Asginados
                var usuariosAsignados = context.Formularios
                    .Include("Empleado")
                    .Include("Perfiles")
                    .AsNoTracking()
                    .Where(x => x.Perfiles.Any(p => p.Id == perfilId))
                    .Select(x => new FormularioPerfilDto
                    {
                        FormularioId = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        Item = false
                    }).ToList();

                var resultado = usuariosAsignados
                    .Where(x => x.Descripcion.Contains(cadenaBuscar)
                                || x.Codigo == cadenaBuscar)
                    .ToList();

                return resultado;
            }
        }

        public void AsignarFormularios(long perfilId, List<FormularioPerfilDto> usuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfil = context.Perfiles.FirstOrDefault(x => x.Id == perfilId);

                if (perfil == null) throw new Exception("No se encontro el Perfil");

                foreach (var usu in usuarios)
                {
                    var usuario = context.Formularios.FirstOrDefault(x => x.Id == usu.FormularioId);

                    perfil.Formularios.Add(usuario);
                }

                context.SaveChanges();
            }
        }

        public void QuitarFormularios(long perfilId, List<FormularioPerfilDto> usuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfil = context.Perfiles.FirstOrDefault(x => x.Id == perfilId);

                if (perfil == null) throw new Exception("No se encontro el Perfil");

                foreach (var usu in usuarios)
                {
                    var usuario = context.Formularios.FirstOrDefault(x => x.Id == usu.FormularioId);

                    perfil.Formularios.Remove(usuario);
                }

                context.SaveChanges();
            }
        }
    }
}
