﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Perfil.DTOs;

namespace XCommerce.LogicaNegocio.Perfil
{
    public class PerfilServicio
    {
        public long Insertar(PerfilDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaPerfil = new AccesoDatos.Perfil
                {
                    Descripcion = dto.Descripcion,
                    EmpresaId = dto.EmpresaId
                };

                context.Perfiles.Add(nuevaPerfil);
                context.SaveChanges();

                return nuevaPerfil.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfilEliminar = context.Perfiles
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (perfilEliminar == null) throw new Exception("No se encontro la Condicion de Iva");

                context.Perfiles.Remove(perfilEliminar);
                context.SaveChanges();
            }
        }

        public void Modificar(PerfilDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfilModificar = context.Perfiles
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (perfilModificar == null) throw new Exception("No se encontro el perfil");

                perfilModificar.Descripcion = dto.Descripcion;
                perfilModificar.EmpresaId = dto.EmpresaId;


                context.SaveChanges();
            }
        }

        public PerfilDto ObtenerPorId(long perfilId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfil = context.Perfiles
                    .Include("Usuarios")
                    .AsNoTracking()
                    .FirstOrDefault(x => x.Id == perfilId);

                if (perfil == null) throw new Exception("No se encontro el Perfil");

                return new PerfilDto
                {
                    Id = perfil.Id,
                    Descripcion = perfil.Descripcion,
                    EmpresaId = perfil.EmpresaId,
                    CantidadUsuarios = perfil.Usuarios.Count
                };
            }
        }

        public IEnumerable<PerfilDto> Obtener(string cadenaBuscar, long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Perfiles
                    .Include("Usuarios")
                    .AsNoTracking()
                    .Where(x => x.EmpresaId == empresaId && x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new PerfilDto
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion,
                        EmpresaId = x.EmpresaId,
                        CantidadUsuarios = x.Usuarios.Count
                    }).OrderBy(x => x.Descripcion)
                    .ToList();
            }
        }

        public bool VerificarSiExiste(string descripcion, long empresaId, long? id = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (id == null)
                {
                    return context.Perfiles
                        .Any(x =>x.EmpresaId == empresaId && x.Descripcion == descripcion);
                }
                else
                {
                    return context.Perfiles
                        .Any(x => x.Id != id.Value && x.EmpresaId == empresaId  && x.Descripcion == descripcion);
                }
            }
        }
    }
}
