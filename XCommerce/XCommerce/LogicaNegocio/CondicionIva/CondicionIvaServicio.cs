﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.CondicionIva.DTOs;

namespace XCommerce.LogicaNegocio.CondicionIva
{
    public class CondicionIvaServicio
    {
        public long Insertar(CondicionIvaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaCondicionIva = new AccesoDatos.CondicionIva
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminada = dto.EstaEliminada
                };

                context.CondicionIvas.Add(nuevaCondicionIva);
                context.SaveChanges();

                return nuevaCondicionIva.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var condicionIvaEliminar = context.CondicionIvas
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (condicionIvaEliminar == null) throw new Exception("No se encontro la Condicion de Iva");

                condicionIvaEliminar.EstaEliminada = !condicionIvaEliminar.EstaEliminada;
                context.SaveChanges();
            }
        }

        public void Modificar(CondicionIvaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var condicionIvaModificar= context.CondicionIvas
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (condicionIvaModificar == null) throw new Exception("No se encontro la Condicion de Iva");

                condicionIvaModificar.Codigo = dto.Codigo;
                condicionIvaModificar.Descripcion = dto.Descripcion;

                context.SaveChanges();
            }
        }

        public CondicionIvaDto ObtenerPorId(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var condicionIva = context.CondicionIvas
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (condicionIva == null) throw new Exception("No se encontro la Condicion de Iva");

                return new CondicionIvaDto
                {
                    Id = condicionIva.Id,
                    Codigo = condicionIva.Codigo,
                    Descripcion = condicionIva.Descripcion,
                    EstaEliminada = condicionIva.EstaEliminada
                };
            }
        }

        public IEnumerable<CondicionIvaDto> Obtener(string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.CondicionIvas
                    .AsNoTracking()
                    .Where(x => x.EstaEliminada == estado
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new CondicionIvaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminada = x.EstaEliminada
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.CondicionIvas.AsNoTracking().Any()
                    ? context.CondicionIvas.Max(x => x.Codigo) + 1
                    : 1;
            }
        }
    }
}
