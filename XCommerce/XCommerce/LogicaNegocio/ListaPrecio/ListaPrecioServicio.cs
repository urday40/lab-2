﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.ListaPrecio.DTOs;

namespace XCommerce.LogicaNegocio.ListaPrecio
{
    public class ListaPrecioServicio
    {
        public long Insertar(ListaPrecioDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoListaPrecio = new AccesoDatos.ListaPrecio
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminada = dto.EstaEliminada,
                    EmpresaId = dto.EmpresaId,
                    Rentabilidad = dto.Rentabilidad
                };

                context.ListaPrecios.Add(nuevoListaPrecio);
                context.SaveChanges();

                return nuevoListaPrecio.Id;
            }
        }

        public void Eliminar(long listaPrecioId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var listaPrecioEliminar = context.ListaPrecios
                    .FirstOrDefault(x => x.Id == listaPrecioId);

                if (listaPrecioEliminar == null) throw new Exception("No se encontro la Lista de Precio");

                listaPrecioEliminar.EstaEliminada = !listaPrecioEliminar.EstaEliminada;
                context.SaveChanges();
            }
        }

        public void Modificar(ListaPrecioDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var listaPrecioModificar = context.ListaPrecios
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (listaPrecioModificar == null) throw new Exception("No se encontro la Lista de Precio");

                listaPrecioModificar.Codigo = dto.Codigo;
                listaPrecioModificar.Descripcion = dto.Descripcion;
                listaPrecioModificar.EmpresaId = dto.EmpresaId;
                listaPrecioModificar.Rentabilidad = dto.Rentabilidad;

                context.SaveChanges();
            }
        }

        public ListaPrecioDto ObtenerPorId(long tipoComprobanteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var listaPrecio = context.ListaPrecios
                    .FirstOrDefault(x => x.Id == tipoComprobanteId);

                if (listaPrecio == null) throw new Exception("No se encontro la Lista de Precio");

                return new ListaPrecioDto
                {
                    Id = listaPrecio.Id,
                    Codigo = listaPrecio.Codigo,
                    Descripcion = listaPrecio.Descripcion,
                    EstaEliminada = listaPrecio.EstaEliminada,
                    EmpresaId = listaPrecio.EmpresaId,
                    Rentabilidad = listaPrecio.Rentabilidad
                };
            }
        }

        public IEnumerable<ListaPrecioDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.ListaPrecios
                    .AsNoTracking()
                    .Where(x => x.EstaEliminada == estado
                                && x.EmpresaId == empresaId
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new ListaPrecioDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminada = x.EstaEliminada,
                        EmpresaId = x.EmpresaId,
                        Rentabilidad = x.Rentabilidad
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.ListaPrecios.AsNoTracking().Any(x => x.EmpresaId == empresaId)
                    ? context.ListaPrecios.Where(x => x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long empresaId, int codigo, string descripcion, long? listaPrecioId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (listaPrecioId.HasValue)
                {
                    return context.ListaPrecios
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && x.Id != listaPrecioId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
                else
                {
                    return context.ListaPrecios
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
            }
        }
    }
}
