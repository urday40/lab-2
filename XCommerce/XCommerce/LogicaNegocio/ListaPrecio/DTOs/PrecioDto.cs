﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCommerce.LogicaNegocio.ListaPrecio.DTOs
{
    public class PrecioDto
    {
        public long Id { get; set; }
        public string FechaActualizacion { get; set; }
        public decimal PrecioCosto { get; set; }
        public decimal PrecioPublico { get; set; }
        public decimal Rentabilidad { get; set; }
        public long EmpresaId { get; set; }
        public long ArticuloId { get; set; }
        public long ListaPrecioId { get; set; }
    }
}
