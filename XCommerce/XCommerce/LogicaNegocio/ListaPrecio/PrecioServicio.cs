﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.ListaPrecio.DTOs;

namespace XCommerce.LogicaNegocio.ListaPrecio
{
    public class PrecioServicio
    {
        public IEnumerable<PrecioDto> Obtener(long articuloId, long empresaId, long listaPrecioId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var precio = context.Precios
                    .AsNoTracking()
                    .Where(x => x.ArticuloId == articuloId
                                && x.EmpresaId == empresaId
                                && x.ListaPrecioId == listaPrecioId)
                    .Select(x => new PrecioDto
                    {
                        Id = x.Id,
                        ArticuloId = x.ArticuloId,
                        EmpresaId = x.EmpresaId,
                        ListaPrecioId = x.ListaPrecioId,
                        FechaActualizacion = x.FechaActualizacion,
                        PrecioCosto = x.PrecioCosto,
                        PrecioPublico = x.PrecioPublico,
                        Rentabilidad = x.Rentabilidad

                    }).ToList();
                return precio;
            }
        }

        public decimal Monto(long articuloId, long empresaId, long listaPrecioId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var monto = context.Precios.FirstOrDefault(x => x.ArticuloId == articuloId
                                                                && x.EmpresaId == empresaId
                                                                && x.ListaPrecioId == listaPrecioId);
                return monto.PrecioPublico;
            }           
        }
    }
}
