﻿using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Forms;
using XCommerce.AccesoDatos;
using XCommerce.Base.Clases;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Usuario.DTOs;

namespace XCommerce.LogicaNegocio.Seguridad
{
    public class SeguridadServicio
    {
        public bool VerificarSiTieneAccesoAlFormulario(Form obj, string usuarioLogin)
        {
            if (usuarioLogin == ConstanteSeguridad.UsuarioAdministrador)
                return true;

            using (var context = new ModeloDatosContainer())
            {
                return context.Perfiles
                    .AsNoTracking()
                    .Include("Usuarios")
                    .Include("Formularios")
                    .Any(p => p.Usuarios.Any(u => u.Nombre == usuarioLogin)
                              && p.Formularios.Any(f => f.DescripcionCompleta == obj.Name));
            }
        }

        public UsuarioDto VerificarAccesoAlSistema(string usuario, string password)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (usuario == ConstanteSeguridad.UsuarioAdministrador
                    && password == Encriptar.EncriptarCadena(ConstanteSeguridad.PasswordAdministrador))
                    return new UsuarioDto
                    {
                        Nombre = string.Empty,
                        Apellido = "Administrador",
                        Bloqueado = false,
                        EmpleadoId = 0,
                        Foto = Properties.Resources.Usuario,
                        NombreUsuario = ConstanteSeguridad.UsuarioAdministrador,
                        UsuarioId = 0
                    };

                var usuarioLogin = context.Usuarios
                    .AsNoTracking()
                    .Include("Empleado")
                    .FirstOrDefault(x => x.Nombre == usuario && x.Password == password);

                if (usuarioLogin != null)
                {
                    return new UsuarioDto
                    {
                        NombreUsuario = usuario,
                        Apellido = usuarioLogin.Empleado.Apellido,
                        Bloqueado = usuarioLogin.EstaBloqueado,
                        EmpleadoId = usuarioLogin.EmpleadoId,
                        Foto = Imagen.Convertir_Bytes_Imagen(usuarioLogin.Empleado.Foto),
                        Nombre = usuarioLogin.Empleado.Nombre,
                        UsuarioId = usuarioLogin.Id,
                    };
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
