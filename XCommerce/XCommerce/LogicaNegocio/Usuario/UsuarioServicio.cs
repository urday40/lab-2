﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.Base.Clases;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Usuario.DTOs;

namespace XCommerce.LogicaNegocio.Usuario
{
    public class UsuarioServicio
    {
        public IEnumerable<UsuarioDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                var legajo = -1;
                int.TryParse(cadenaBuscar, out legajo);

                return context.Personas.OfType<AccesoDatos.Empleado>()
                    .AsNoTracking()
                    .Include(x => x.Usuarios)
                    .Where(x => !x.EstaEliminado
                                && (x.Apellido.Contains(cadenaBuscar)
                                    || x.Nombre.Contains(cadenaBuscar)
                                    || x.Usuarios.Any(u => u.Nombre.Contains(cadenaBuscar))
                                    || x.Legajo == legajo
                                    || x.Dni == cadenaBuscar))
                    .Select(x => new UsuarioDto
                    {
                        Item = false,
                        EmpleadoId = x.Id,
                        Apellido = x.Apellido,
                        Nombre = x.Nombre,
                        UsuarioId = x.Usuarios.Any() ? x.Usuarios.FirstOrDefault().Id : (long?)null,
                        NombreUsuario = x.Usuarios.Any() ? x.Usuarios.FirstOrDefault().Nombre : "NO ASIGNADO",
                        Bloqueado = x.Usuarios.Any() && x.Usuarios.FirstOrDefault().EstaBloqueado
                    }).OrderBy(x => x.Apellido).ThenBy(x => x.Nombre)
                    .ToList();
            }
        }

        public void Crear(List<UsuarioDto> listaUsuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                foreach (var usuario in listaUsuarios)
                {
                    var nombreUsuario = GenerarNombreUsuario(usuario.Apellido, usuario.Nombre);

                    var usuarioNuevo = new AccesoDatos.Usuario
                    {
                        Nombre = nombreUsuario,
                        EstaBloqueado = false,
                        EmpleadoId = usuario.EmpleadoId,
                        Password = Encriptar.EncriptarCadena(ConstanteSeguridad.PasswordPorDefecto),
                    };

                    context.Usuarios.Add(usuarioNuevo);
                }

                context.SaveChanges();
            }
        }

        public void CambiarEstado(List<UsuarioDto> listaUsuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                foreach (var usuarioDto in listaUsuarios)
                {
                    var usuario = context.Usuarios
                        .FirstOrDefault(x => x.Id == usuarioDto.UsuarioId);

                    if (usuario == null) throw new Exception("No se encontro el Usuario");

                    usuario.EstaBloqueado = !usuario.EstaBloqueado;
                }

                context.SaveChanges();
            }
        }

        public void CambiarEstado(string usuario)
        {
            using (var context = new ModeloDatosContainer())
            {
                var usu = context.Usuarios.FirstOrDefault(x => x.Nombre == usuario);

                if (usu == null) throw new Exception("El usuario no Existe");

                usu.EstaBloqueado = true;

                context.SaveChanges();
            }
        }

        public void ResetPassword(List<UsuarioDto> listaUsuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                foreach (var usuarioDto in listaUsuarios)
                {
                    var usuario = context.Usuarios
                        .FirstOrDefault(x => x.Id == usuarioDto.UsuarioId);

                    if (usuario == null) throw new Exception("No se encontro el Usuario");

                    usuario.Password = Encriptar.EncriptarCadena(ConstanteSeguridad.PasswordPorDefecto);
                }

                context.SaveChanges();
            }
        }

        public void CambiarPassword(long usuarioId, string passwordActual, string passwordNuevo)
        {
            using (var context = new ModeloDatosContainer())
            {
                var usuario = context.Usuarios
                    .FirstOrDefault(x => x.Id == usuarioId);

                if (usuario == null) throw new Exception("No se encontro el Usuario");

                var passActual = Encriptar.EncriptarCadena(passwordActual);
                var passNuevo = Encriptar.EncriptarCadena(passwordNuevo);

                if (usuario.Password != passwordActual) throw new Exception("La contraseña actual no coincide.");

                usuario.Password = Encriptar.EncriptarCadena(passNuevo);

                context.SaveChanges();
            }
        }


        // ====================================================================== //
        // =================         Metodos Privados        ==================== //
        // ====================================================================== //
        private string GenerarNombreUsuario(string apellido, string nombre)
        {
            var contador = 1;

            var nombreUsuario =
                $"{nombre.Trim().ToLower().Substring(0, contador)}{apellido.Trim().ToLower()}";

            using (var context = new ModeloDatosContainer())
            {
                while (context.Usuarios.Any(x => x.Nombre == nombreUsuario))
                {
                    contador++;
                    nombreUsuario =
                        $"{nombre.Trim().ToLower().Substring(0, contador)}{apellido.Trim().ToLower()}";
                }
            }

            return nombreUsuario;
        }
    }
}
