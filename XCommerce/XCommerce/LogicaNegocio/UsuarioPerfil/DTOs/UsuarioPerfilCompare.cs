﻿using System.Collections.Generic;

namespace XCommerce.LogicaNegocio.UsuarioPerfil.DTOs
{
    public class UsuarioPerfilCompare : IEqualityComparer<UsuarioPerfilDto>
    {
        public bool Equals(UsuarioPerfilDto x, UsuarioPerfilDto y)
        {
            if (object.ReferenceEquals(x, y)) return true;

            if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null))
                return false;

            return x.UsuarioId == y.UsuarioId;
        }

        public int GetHashCode(UsuarioPerfilDto obj)
        {
            return obj.UsuarioId.GetHashCode();
        }
    }
}
