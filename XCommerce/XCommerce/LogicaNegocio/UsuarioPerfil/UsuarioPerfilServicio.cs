﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.UsuarioPerfil.DTOs;

namespace XCommerce.LogicaNegocio.UsuarioPerfil
{
    public class UsuarioPerfilServicio
    {
        public IEnumerable<UsuarioPerfilDto> ObtenerUsuariosNoAsignados(string cadenaBuscar, long perfilId)
        {
            using (var context = new ModeloDatosContainer())
            {
                // Obtengo TODOS los Usuarios
                var usuarios = context.Usuarios
                    .Include("Empleado")
                    .AsNoTracking()
                    .Select(x => new UsuarioPerfilDto
                    {
                        UsuarioId = x.Id,
                        Usuario = x.Nombre,
                        ApellidoEmpleado = x.Empleado.Apellido,
                        NombreEmpleado = x.Empleado.Nombre,
                        DniEmpleado = x.Empleado.Dni,
                        Item = false
                    }).ToList();

                // Obtener los Usuarios Asginados
                var usuariosAsignados = context.Usuarios
                    .Include("Empleado")
                    .Include("Perfiles")
                    .AsNoTracking()
                    .Where(x => x.Perfiles.Any(p => p.Id == perfilId))
                    .Select(x => new UsuarioPerfilDto
                    {
                        UsuarioId = x.Id,
                        Usuario = x.Nombre,
                        ApellidoEmpleado = x.Empleado.Apellido,
                        NombreEmpleado = x.Empleado.Nombre,
                        DniEmpleado = x.Empleado.Dni,
                        Item = false
                    }).ToList();

                var resultado = usuarios
                    .Except(usuariosAsignados, new UsuarioPerfilCompare())
                    .Where(x => x.ApellidoEmpleado.Contains(cadenaBuscar)
                                || x.NombreEmpleado.Contains(cadenaBuscar)
                                || x.DniEmpleado == cadenaBuscar
                                || x.Usuario.Contains(cadenaBuscar))
                    .ToList();

                return resultado;
            }
        }

        public IEnumerable<UsuarioPerfilDto> ObtenerUsuariosAsignados(string cadenaBuscar, long perfilId)
        {
            using (var context = new ModeloDatosContainer())
            {
                
                // Obtener los Usuarios Asginados
                var usuariosAsignados = context.Usuarios
                    .Include("Empleado")
                    .Include("Perfiles")
                    .AsNoTracking()
                    .Where(x => x.Perfiles.Any(p => p.Id == perfilId))
                    .Select(x => new UsuarioPerfilDto
                    {
                        UsuarioId = x.Id,
                        Usuario = x.Nombre,
                        ApellidoEmpleado = x.Empleado.Apellido,
                        NombreEmpleado = x.Empleado.Nombre,
                        DniEmpleado = x.Empleado.Dni,
                        Item = false
                    }).ToList();

                var resultado = usuariosAsignados
                    .Where(x => x.ApellidoEmpleado.Contains(cadenaBuscar)
                                || x.NombreEmpleado.Contains(cadenaBuscar)
                                || x.DniEmpleado == cadenaBuscar
                                || x.Usuario.Contains(cadenaBuscar))
                    .ToList();

                return resultado;
            }
        }

        public void AsignarUsuarios(long perfilId, List<UsuarioPerfilDto> usuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfil = context.Perfiles.FirstOrDefault(x => x.Id == perfilId);

                if(perfil == null) throw new Exception("No se encontro el Perfil");

                foreach (var usu in usuarios)
                {
                    var usuario = context.Usuarios.FirstOrDefault(x => x.Id == usu.UsuarioId);

                    perfil.Usuarios.Add(usuario);
                }

                context.SaveChanges();
            }
        }

        public void QuitarUsuarios(long perfilId, List<UsuarioPerfilDto> usuarios)
        {
            using (var context = new ModeloDatosContainer())
            {
                var perfil = context.Perfiles.FirstOrDefault(x => x.Id == perfilId);

                if (perfil == null) throw new Exception("No se encontro el Perfil");

                foreach (var usu in usuarios)
                {
                    var usuario = context.Usuarios.FirstOrDefault(x => x.Id == usu.UsuarioId);

                    perfil.Usuarios.Remove(usuario);
                }

                context.SaveChanges();
            }
        }
    }

}
