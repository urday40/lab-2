﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Sala.DTOs;

namespace XCommerce.LogicaNegocio.Sala
{
    public class SalaServicio
    {
        public long Insertar(SalaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaSala = new AccesoDatos.Sala
                {
                    EmpresaId = dto.EmpresaId,
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminada = dto.EstaEliminada
                };

                context.Salas.Add(nuevaSala);
                context.SaveChanges();

                return nuevaSala.Id;
            }
        }

        public void Eliminar(long SalaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var salaEliminar = context.Salas
                    .FirstOrDefault(x => x.Id == SalaId);

                if (salaEliminar == null) throw new Exception("No se encontro la Sala");

                salaEliminar.EstaEliminada = !salaEliminar.EstaEliminada;
                context.SaveChanges();
            }
        }

        public void Modificar(SalaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var salaModificar = context.Salas
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (salaModificar == null) throw new Exception("No se encontro la Sala");

                salaModificar.EmpresaId = dto.EmpresaId;
                salaModificar.Codigo = dto.Codigo;
                salaModificar.Descripcion = dto.Descripcion;
                salaModificar.EstaEliminada = dto.EstaEliminada;

                context.SaveChanges();
            }
        }

        public SalaDto ObtenerPorId(long SalaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var sala = context.Salas
                    .Include("Empresa")
                    .FirstOrDefault(x => x.Id == SalaId);

                if (sala == null) throw new Exception("No se encontro la Sala");

                return new SalaDto
                {
                    Id = sala.Id,
                    EmpresaId = sala.EmpresaId,
                    Codigo = sala.Codigo,
                    Descripcion = sala.Descripcion,
                    EstaEliminada = sala.EstaEliminada,
                    Empresa = sala.Empresa.RazonSocial
                };
            }
        }

        public IEnumerable<SalaDto> Obtener(string cadenaBuscar, long empresaId, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Salas
                    .Include("Empresa")
                    .Include("Mesas")
                    .AsNoTracking()
                    .Where(x => x.EmpresaId == empresaId
                                && x.EstaEliminada == estado
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new SalaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminada = x.EstaEliminada,
                        EmpresaId = x.EmpresaId,
                        Empresa = x.Empresa.RazonSocial,
                        CantidadMesas = x.Mesas.Count
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Salas.AsNoTracking().Any(x=>x.EmpresaId == empresaId)
                    ? context.Salas.Where(x=>x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }
    }
}
