﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Tarjeta.DTOs;

namespace XCommerce.LogicaNegocio.Tarjeta
{
    public class TarjetaServicio
    {
        public IEnumerable<TarjetaDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int.TryParse(cadenaBuscar, out var codigo);

                return context.Tarjetas
                    .AsNoTracking()
                    .Where(x => x.EstaEliminada == false
                                || x.Codigo == codigo
                                || x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new TarjetaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        EstaEliminada = x.EstaEliminada,
                        Descripcion = x.Descripcion
                    })
                    .OrderBy(x=> x.Codigo)
                    .ToList();
            }
        }
        public IEnumerable<TarjetaDto> ObtenerEliminadas(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int.TryParse(cadenaBuscar, out var codigo);

                return context.Tarjetas
                    .AsNoTracking()
                    .Where(x => x.EstaEliminada
                                || x.Codigo == codigo
                                || x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new TarjetaDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        EstaEliminada = x.EstaEliminada,
                        Descripcion = x.Descripcion
                    })
                    .OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public long Insert(TarjetaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaTarjeta = new AccesoDatos.Tarjeta
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminada = dto.EstaEliminada
                };

                context.Tarjetas.Add(nuevaTarjeta);
                context.SaveChanges();
                return nuevaTarjeta.Id;
            }
        }

        public void Delete(long entity)
        {
            using (var context = new ModeloDatosContainer())
            {
                var entityDelete = context.Tarjetas.FirstOrDefault(x => x.Id == entity);

                if(entityDelete == null)
                    throw new Exception("La tarjeta buscada no existe, por favor intente nuevamente");

                entityDelete.EstaEliminada = !entityDelete.EstaEliminada;
                context.SaveChanges();
            }
        }

        public void Update(TarjetaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var tarjetaMod = context.Tarjetas.FirstOrDefault(x => x.Id == dto.Id || x.Codigo == dto.Codigo);

                if(tarjetaMod == null) throw new Exception("La tarjeta buscada no existe");

                tarjetaMod.Descripcion = dto.Descripcion;
                tarjetaMod.Codigo = dto.Codigo;
                context.SaveChanges();
            }
        }

        public TarjetaDto ObtenerPorId(long entityId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var entidad = context.Tarjetas.FirstOrDefault(x => x.Id == entityId);

                if(entidad == null) throw new Exception("La tarjeta no existe");

                return new TarjetaDto
                {
                    Id = entidad.Id,
                    Codigo = entidad.Codigo,
                    Descripcion = entidad.Descripcion,
                    EstaEliminada = entidad.EstaEliminada
                };
            }
        }

        public int SiguienteCodigo()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Tarjetas.AsNoTracking().Any()
                    ? context.Tarjetas.Max(x => x.Codigo) + 1
                    : 1;
            }
        }
    }
}
