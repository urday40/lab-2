﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Tarjeta.DTOs;

namespace XCommerce.LogicaNegocio.Tarjeta
{
    public class PlanTarjetaServicio
    {
        public IEnumerable<PlanTarjetaDto> GetActive(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                decimal.TryParse(cadenaBuscar, out var alicuota);

                return context.PlanTarjetas
                    .AsNoTracking()
                    .Include("Tarjetas")
                    .Where(x => x.Alicuota == alicuota
                                ||x.Activo
                                || x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new PlanTarjetaDto
                    {
                        Id = x.Id,
                        Activo = x.Activo,
                        Descripcion = x.Descripcion,
                        Alicuota = x.Alicuota,
                        TarjetaId = x.TarjetaId

                    }).OrderBy(x=>x.Descripcion).ToList();
            }
        }
        public IEnumerable<PlanTarjetaDto> GetNonActive(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                decimal.TryParse(cadenaBuscar, out var alicuota);

                return context.PlanTarjetas
                    .AsNoTracking()
                    .Where(x => x.Alicuota == alicuota
                                || x.Activo == false
                                || x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new PlanTarjetaDto
                    {
                        Id = x.Id,
                        Activo = x.Activo,
                        Descripcion = x.Descripcion,
                        Alicuota = x.Alicuota,
                        TarjetaId = x.TarjetaId

                    }).OrderBy(x => x.Descripcion).ToList();
            }
        }

        public PlanTarjetaDto GetById(long entity)
        {
            using (var context = new ModeloDatosContainer())
            {
                var entidad = context.PlanTarjetas.FirstOrDefault(x => x.Id == entity);

                if (entidad == null) throw new Exception("Por favor vuelva a revisar los datos");

                return new PlanTarjetaDto
                {
                    Id = entidad.Id,
                    Descripcion = entidad.Descripcion,
                    Alicuota = entidad.Alicuota,
                    Activo = entidad.Activo,
                    TarjetaId = entidad.TarjetaId
                };

            }
        }

        public long Insert(PlanTarjetaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoPlan = new PlanTarjeta
                {
                    Alicuota = dto.Alicuota,
                    Descripcion = dto.Descripcion,
                    TarjetaId = dto.TarjetaId,
                    Activo = true
                };

                context.PlanTarjetas.Add(nuevoPlan);

                context.SaveChanges();

                return nuevoPlan.Id;
            }
        }

        public void Delete(long planId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var planDetele = context.PlanTarjetas.FirstOrDefault(x => x.Id == planId);

                if(planDetele == null)
                    throw new Exception("No se encontro el plan");

                planDetele.Activo = !planDetele.Activo;

                context.SaveChanges();


            }
        }

        public void Update(PlanTarjetaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var planMod =context.PlanTarjetas
                        .FirstOrDefault(x => x.Id == dto.Id
                                        || x.Descripcion.Contains(dto.Descripcion));
                if(planMod == null) throw new Exception("Por favor verifique los datos");

                planMod.Alicuota = dto.Alicuota;
                planMod.Descripcion = dto.Descripcion;
                context.SaveChanges();

            }
        }
    }
}
