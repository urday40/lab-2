﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCommerce.LogicaNegocio.Tarjeta.DTOs
{
    public class PlanTarjetaDto
    {
        public long Id { get; set; }
        public long TarjetaId { get; set; }
        public string Descripcion { get; set; }
        public decimal Alicuota { get; set; }
        public bool Activo { get; set; }
    }
}
