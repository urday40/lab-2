﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Empresa.DTOs;

namespace XCommerce.LogicaNegocio.Empresa
{
    public class EmpresaServicio
    {
        public long Insertar(EmpresaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevaEmpresa = new AccesoDatos.Empresa
                {
                    RazonSocial = dto.RazonSocial,
                    NombreFantasia = dto.NombreFantasia,
                    Domicilio = dto.Domicilio,
                    Mail = dto.Email,
                    Telefono = dto.Telefono,
                    Logo = dto.Logo,
                    CondicionIvaId = dto.CondicionIvaId,
                    Sucursal = dto.Sucursal,
                    Cuit = dto.Cuit
                };

                context.Empresas.Add(nuevaEmpresa);
                context.SaveChanges();

                return nuevaEmpresa.Id;
            }
        }

        public void Eliminar(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empresaEliminar = context.Empresas
                    .FirstOrDefault(x => x.Id == empresaId);

                if (empresaEliminar == null) throw new Exception("no se encontro el Empresa");

                context.Empresas.Remove(empresaEliminar);
                context.SaveChanges();
            }
        }

        public void Modificar(EmpresaDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empresaModificar = context.Empresas
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (empresaModificar == null) throw new Exception("no se encontro el Empresa");

                empresaModificar.RazonSocial = dto.RazonSocial;
                empresaModificar.NombreFantasia = dto.NombreFantasia;
                empresaModificar.Domicilio = dto.Domicilio;
                empresaModificar.Mail = dto.Email;
                empresaModificar.Telefono = dto.Telefono;
                empresaModificar.Logo = dto.Logo;
                empresaModificar.CondicionIvaId = dto.CondicionIvaId;
                empresaModificar.Sucursal = dto.Sucursal;
                empresaModificar.Cuit = dto.Cuit;

                context.SaveChanges();
            }
        }

        public EmpresaDto ObtenerPorId(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var empresa = context.Empresas
                    .Include("CondicionIva")
                    .FirstOrDefault(x => x.Id == empresaId);

                if (empresa == null) throw new Exception("No se encontro el Empresa");

                return new EmpresaDto
                {
                    Id = empresa.Id,
                    RazonSocial = empresa.RazonSocial,
                    NombreFantasia = empresa.NombreFantasia,
                    Domicilio = empresa.Domicilio,
                    Email = empresa.Mail,
                    Telefono = empresa.Telefono,
                    Logo = empresa.Logo,
                    CondicionIvaId = empresa.CondicionIvaId,
                    Sucursal = empresa.Sucursal,
                    Cuit = empresa.Cuit,
                    CondicionIva = empresa.CondicionIva.Descripcion
                };
            }
        }

        public IEnumerable<EmpresaDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloDatosContainer())
            {
                int.TryParse(cadenaBuscar, out var sucursal);

                return context.Empresas
                    .Include("CondicionIva")
                    .AsNoTracking()
                    .Where(x => x.RazonSocial.Contains(cadenaBuscar)
                                || x.NombreFantasia.Contains(cadenaBuscar)
                                || x.Cuit == cadenaBuscar
                                || x.Sucursal == sucursal)
                    .Select(x => new EmpresaDto
                    {
                        Id = x.Id,
                        RazonSocial = x.RazonSocial,
                        NombreFantasia = x.NombreFantasia,
                        Domicilio = x.Domicilio,
                        Email = x.Mail,
                        Telefono = x.Telefono,
                        Logo = x.Logo,
                        CondicionIvaId = x.CondicionIvaId,
                        Sucursal = x.Sucursal,
                        Cuit = x.Cuit,
                        CondicionIva = x.CondicionIva.Descripcion
                    }).OrderBy(x => x.Sucursal).ThenBy(x => x.RazonSocial)
                    .ToList();
            }
        }
    }
}
