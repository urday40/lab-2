﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Rubro.DTOs;

namespace XCommerce.LogicaNegocio.Rubro
{
    public class RubroServicio
    {
        public long Insertar(RubroDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoRubro = new AccesoDatos.Rubro
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminado = dto.EstaEliminado,
                    EmpresaId = dto.EmpresaId
                };

                context.Rubros.Add(nuevoRubro);
                context.SaveChanges();

                return nuevoRubro.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var rubroEliminar = context.Rubros
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (rubroEliminar == null) throw new Exception("No se encontro la Rubro");

                rubroEliminar.EstaEliminado = !rubroEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(RubroDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var rubroModificar = context.Rubros
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (rubroModificar == null) throw new Exception("No se encontro la Rubro");

                rubroModificar.Codigo = dto.Codigo;
                rubroModificar.Descripcion = dto.Descripcion;
                rubroModificar.EmpresaId = dto.EmpresaId;

                context.SaveChanges();
            }
        }

        public RubroDto ObtenerPorId(long rubroId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var rubro = context.Rubros
                    .FirstOrDefault(x => x.Id == rubroId);

                if (rubro == null) throw new Exception("No se encontro la Rubro");

                return new RubroDto
                {
                    Id = rubro.Id,
                    Codigo = rubro.Codigo,
                    Descripcion = rubro.Descripcion,
                    EstaEliminado = rubro.EstaEliminado,
                    EmpresaId = rubro.EmpresaId
                };
            }
        }

        public IEnumerable<RubroDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.Rubros
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && x.EmpresaId == empresaId
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new RubroDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado,
                        EmpresaId = x.EmpresaId
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Rubros.AsNoTracking().Any(x => x.EmpresaId == empresaId)
                    ? context.Rubros.Where(x => x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long empresaId, int codigo, string descripcion, long? rubroId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (rubroId.HasValue)
                {
                    return context.Rubros
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && x.Id != rubroId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
                else
                {
                    return context.Rubros
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
            }
        }
    }
}
