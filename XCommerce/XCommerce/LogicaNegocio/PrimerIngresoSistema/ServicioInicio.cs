﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using XCommerce.AccesoDatos;
using XCommerce.Base.Clases;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Empleado.DTOs;
using XCommerce.LogicaNegocio.Empresa.DTOs;

namespace XCommerce.LogicaNegocio.PrimerIngresoSistema
{
    public class ServicioInicio
    {
        public void Insertar(EmpleadoDto empleadoDto, EmpresaDto empresaDto, Assembly assembly)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoEmpleado = new AccesoDatos.Empleado
                {
                    Apellido = empleadoDto.Apellido,
                    Celular = empleadoDto.Celular,
                    Dni = empleadoDto.Dni,
                    Domicilio = empleadoDto.Domicilio,
                    Email = empleadoDto.Email,
                    FechaIngreso = empleadoDto.FechaIngreso,
                    FechaNacimiento = empleadoDto.FechaNacimiento,
                    Legajo = empleadoDto.Legajo,
                    Nombre = empleadoDto.Nombre,
                    Telefono = empleadoDto.Telefono,
                    Foto = empleadoDto.Foto,
                    EstaEliminado = false,
                    Usuarios = new List<AccesoDatos.Usuario>()
                };

                // Usuario
                var nombreUsuario = GenerarNombreUsuario(nuevoEmpleado.Apellido, nuevoEmpleado.Nombre);

                var usuarioNuevo = new AccesoDatos.Usuario
                {
                    Nombre = nombreUsuario,
                    EstaBloqueado = false,
                    Password = Encriptar.EncriptarCadena(ConstanteSeguridad.PasswordPorDefecto),
                };

                nuevoEmpleado.Usuarios.Add(usuarioNuevo);

                var nuevaEmpresa = new AccesoDatos.Empresa
                {
                    RazonSocial = empresaDto.RazonSocial,
                    NombreFantasia = empresaDto.NombreFantasia,
                    Domicilio = empresaDto.Domicilio,
                    Mail = empresaDto.Email,
                    Telefono = empresaDto.Telefono,
                    Logo = empresaDto.Logo,
                    CondicionIvaId = empresaDto.CondicionIvaId,
                    Sucursal = empresaDto.Sucursal,
                    Cuit = empresaDto.Cuit,
                    Empleados = new List<AccesoDatos.Empleado>(),
                    Perfiles = new List<AccesoDatos.Perfil>(),
                    Depositos = new List<AccesoDatos.Deposito>(),
                    ListaPrecios = new List<AccesoDatos.ListaPrecio>(),
                    TipoComprobantes = new List<AccesoDatos.TipoComprobante>(),
                    Configuraciones = new List<AccesoDatos.Configuracion>()
                };
                
                var nuevoPerfil = new AccesoDatos.Perfil
                {
                    Descripcion = "Administrador",
                    Usuarios = new List<AccesoDatos.Usuario>(),
                    Formularios = new List<AccesoDatos.Formulario>()
                };

                nuevoPerfil.Usuarios.Add(usuarioNuevo);

                // Formularios
                foreach (var obj in assembly.GetTypes())
                {
                    if (!obj.Name[0].Equals('_')) continue;

                    var formulario = new AccesoDatos.Formulario
                    {
                        Codigo = obj.Name.Substring(1, 5),
                        Descripcion = obj.Name.Substring(7, obj.Name.Length - 7),
                        DescripcionCompleta = obj.Name,
                        EstaEliminado = false
                    };

                    nuevoPerfil.Formularios.Add(formulario);
                }

                nuevaEmpresa.Perfiles.Add(nuevoPerfil);
                nuevaEmpresa.Empleados.Add(nuevoEmpleado);

                var deposito = new AccesoDatos.Deposito
                {
                    Descripcion = "Principal",
                    Codigo = 1,
                    EstaEliminado = false,
                };

                nuevaEmpresa.Depositos.Add(deposito);

                var tipoComprobante = new AccesoDatos.TipoComprobante
                {
                    Codigo = 1,
                    Descripcion = "Factura C",
                    EstaEliminado = false,
                    Letra = "C"
                };

                nuevaEmpresa.TipoComprobantes.Add(tipoComprobante);

                var listaPrecio = new AccesoDatos.ListaPrecio
                {
                    Codigo = 1,
                    Descripcion = "Efectivo",
                    EstaEliminada = false,
                    Rentabilidad = 30m
                };

                nuevaEmpresa.ListaPrecios.Add(listaPrecio);

                var configuracion = new AccesoDatos.Configuracion
                {
                    DepositoPorDefecto = deposito,
                    GrabarProductoEnTodosDepositos = true,
                    ListaPrecioPorDefecto = listaPrecio,
                    MontoPorCubierto = 0,
                    SeCobraCubiertos = false,
                    TipoComprobantePorDefecto = tipoComprobante
                };

                nuevaEmpresa.Configuraciones.Add(configuracion);

                context.Empresas.Add(nuevaEmpresa);

                var clienteConsumidorFinal = new AccesoDatos.Cliente
                {
                    EstaEliminado = false,
                    Visible = false,
                    Nombre = "",
                    Apellido = "Consumidor Final",
                    Celular = "1234567890",
                    Dni = "99999999",
                    Domicilio = "Sin Direccion",
                    Email = "Sin Mail",
                    EstaBloqueado = false,
                    FechaNacimiento = new DateTime(1970, 01,01),
                    MontoMaximoCompra = 0m,
                    Telefono = "1234567890",
                    TieneCuentaCorriente = false
                };

                context.Personas.Add(clienteConsumidorFinal);
                context.SaveChanges();
            }
        }

        public bool VerificarSiEsPrimerAcceso()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Personas.OfType<AccesoDatos.Empleado>().Any() 
                    && context.Empresas.Any();
            }
        }

        // Metodos Privados
        private string GenerarNombreUsuario(string apellido, string nombre)
        {
            var contador = 1;

            var nombreUsuario =
                $"{nombre.Trim().ToLower().Substring(0, contador)}{apellido.Trim().ToLower()}";

            using (var context = new ModeloDatosContainer())
            {
                while (context.Usuarios.Any(x => x.Nombre == nombreUsuario))
                {
                    contador++;
                    nombreUsuario =
                        $"{nombre.Trim().ToLower().Substring(0, contador)}{apellido.Trim().ToLower()}";
                }
            }

            return nombreUsuario;
        }
    }
}
