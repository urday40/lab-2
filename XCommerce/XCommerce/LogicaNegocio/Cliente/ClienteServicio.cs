﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Cliente.DTOs;

namespace XCommerce.LogicaNegocio.Cliente
{
    public class ClienteServicio
    {
        public long Insertar(ClienteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoCliente = new AccesoDatos.Cliente
                {
                    Apellido = dto.Apellido,
                    Celular = dto.Celular,
                    Dni = dto.Dni,
                    Domicilio = dto.Domicilio,
                    Email = dto.Email,
                    FechaNacimiento = dto.FechaNacimiento,
                    Nombre = dto.Nombre,
                    Telefono = dto.Telefono,
                    EstaEliminado = false,
                    EstaBloqueado = false,
                    MontoMaximoCompra = dto.MontoMaximoCompra,
                    TieneCuentaCorriente = dto.TieneCuentaCorriente
                };

                context.Personas.Add(nuevoCliente);
                context.SaveChanges();

                return nuevoCliente.Id;
            }
        }

        public void Eliminar(long clienteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var clienteEliminar = context.Personas.OfType<AccesoDatos.Cliente>()
                    .FirstOrDefault(x => x.Id == clienteId);

                if (clienteEliminar == null) throw new Exception("No se encontro el cliente");

                clienteEliminar.EstaEliminado = !clienteEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(ClienteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var clienteModificar = context.Personas.OfType<AccesoDatos.Cliente>()
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (clienteModificar == null) throw new Exception("no se encontro el cliente");

                clienteModificar.Apellido = dto.Apellido;
                clienteModificar.Celular = dto.Celular;
                clienteModificar.Dni = dto.Dni;
                clienteModificar.Domicilio = dto.Domicilio;
                clienteModificar.Email = dto.Email;
                clienteModificar.FechaNacimiento = dto.FechaNacimiento;
                clienteModificar.Nombre = dto.Nombre;
                clienteModificar.Telefono = dto.Telefono;
                clienteModificar.EstaEliminado = false;
                clienteModificar.EstaBloqueado = false;
                clienteModificar.MontoMaximoCompra = dto.MontoMaximoCompra;
                clienteModificar.TieneCuentaCorriente = dto.TieneCuentaCorriente;

                context.SaveChanges();
            }
        }

        public ClienteDto ObtenerPorId(long clienteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var cliente = context.Personas.OfType<AccesoDatos.Cliente>()
                    .FirstOrDefault(x => x.Id == clienteId);

                if (cliente == null) throw new Exception("No se encontro el cliente");

                return new ClienteDto
                {
                    Id = cliente.Id,
                    Apellido = cliente.Apellido,
                    Celular = cliente.Celular,
                    Dni = cliente.Dni,
                    Domicilio = cliente.Domicilio,
                    Email = cliente.Email,
                    FechaNacimiento = cliente.FechaNacimiento,
                    Nombre = cliente.Nombre,
                    Telefono = cliente.Telefono,
                    EstaEliminado = false,
                    EstaBloqueado = false,
                    MontoMaximoCompra = cliente.MontoMaximoCompra,
                    TieneCuentaCorriente = cliente.TieneCuentaCorriente
                };
            }
        }

        public IEnumerable<ClienteDto> Obtener(string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int legajo = -1;
                int.TryParse(cadenaBuscar, out legajo);

                return context.Personas.OfType<AccesoDatos.Cliente>()
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && (x.Apellido.Contains(cadenaBuscar)
                                    || x.Nombre.Contains(cadenaBuscar)
                                    || x.Dni == cadenaBuscar))
                    .Select(x => new ClienteDto
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Celular = x.Celular,
                        Dni = x.Dni,
                        Domicilio = x.Domicilio,
                        Email = x.Email,
                        FechaNacimiento = x.FechaNacimiento,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        EstaEliminado = false,
                        EstaBloqueado = false,
                        MontoMaximoCompra = x.MontoMaximoCompra,
                        TieneCuentaCorriente = x.TieneCuentaCorriente
                    }).OrderBy(x => x.Apellido).ThenBy(x => x.Nombre)
                    .ToList();
            }
        }
    }
}
