﻿namespace XCommerce.LogicaNegocio.Articulo.DTOs
{
    public class StockDto
    {
        public long Id { get; set; }
        public long DepositoId { get; set; }
      //  public string Deposito { get; set; }
        public long ArticuloId { get; set; }
        public decimal Cantidad { get; set; }
    }
}
