﻿namespace XCommerce.LogicaNegocio.Articulo.DTOs
{
    public class IngredienteDto
    {
        public long Id { get; set; }

        public long ArticuloPadreId { get; set; }

        public long ArticuloHijoId { get; set; }

        public decimal Cantidad { get; set; }
    }
}
