﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Articulo.DTOs;

namespace XCommerce.LogicaNegocio.Articulo
{
    public class ArticuloServicio
    {
        public long Insertar(ArticuloDto dto, long empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoArticulo = new AccesoDatos.Articulo
                {
                    Codigo = dto.Codigo,
                    CodigoBarra = dto.CodigoBarra,
                    Descripcion = dto.Descripcion,
                    Abreviatura = dto.Abreviatura,
                    MarcaId = dto.MarcaId,
                    SubRubroId = dto.SubRubroId,
                    Foto = dto.Foto,
                    ActivarLimiteVenta = dto.ActivarLimiteVenta,
                    CantidadLimiteVenta = dto.CantidadLimiteVenta,
                    PermiteStockNegativo = dto.PermiteStockNegativo,
                    EstaDiscontinuado = dto.EstaDiscontinuado,
                    TipoArticulo = dto.TipoArticulo,
                    StockMaximo = dto.StockMaximo,
                    StockMinimo = dto.StockMinimo,
                    DescuentaStock = dto.DescuentaStock,
                    SePuedeFraccionar = dto.SePuedeFraccionar,
                    Detalle = dto.Detalle,
                    EstaEliminado = dto.EstaEliminado,
                    
                    Stocks = new List<Stock>()
                };

                context.Articulos.Add(nuevoArticulo);

                // Obtengo la configuracion del sistema
                var configuracion = context.Configuraciones.FirstOrDefault(x => x.EmpresaId == empresaId);

                if (configuracion == null) throw new Exception("No se cargo la configuración del sistema.");

                if (configuracion.GrabarProductoEnTodosDepositos)
                {
                    var depositos = context.Depositos
                        .Where(x => x.EmpresaId == empresaId)
                        .ToList();

                    foreach (var deposito in depositos)
                    {
                        nuevoArticulo.Stocks.Add(new AccesoDatos.Stock
                        {
                            Cantidad = 0,
                            Deposito = deposito
                        });
                    }
                }

                context.SaveChanges();

                return nuevoArticulo.Id;
            }
        }

        public void Eliminar(long articuloId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var articuloEliminar = context.Articulos
                    .FirstOrDefault(x => x.Id == articuloId);

                if (articuloEliminar == null) throw new Exception("No se encontro la Articulo");

                articuloEliminar.EstaEliminado = !articuloEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(ArticuloDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var articuloModificar = context.Articulos
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (articuloModificar == null) throw new Exception("No se encontro el Articulo");

                articuloModificar.Codigo = dto.Codigo;
                articuloModificar.CodigoBarra = dto.CodigoBarra;
                articuloModificar.Descripcion = dto.Descripcion;
                articuloModificar.Abreviatura = dto.Abreviatura;
                articuloModificar.MarcaId = dto.MarcaId;
                articuloModificar.SubRubroId = dto.SubRubroId;
                articuloModificar.Foto = dto.Foto;
                articuloModificar.ActivarLimiteVenta = dto.ActivarLimiteVenta;
                articuloModificar.CantidadLimiteVenta = dto.CantidadLimiteVenta;
                articuloModificar.PermiteStockNegativo = dto.PermiteStockNegativo;
                articuloModificar.EstaDiscontinuado = dto.EstaDiscontinuado;
                articuloModificar.TipoArticulo = dto.TipoArticulo;
                articuloModificar.StockMaximo = dto.StockMaximo;
                articuloModificar.StockMinimo = dto.StockMinimo;
                articuloModificar.DescuentaStock = dto.DescuentaStock;
                articuloModificar.SePuedeFraccionar = dto.SePuedeFraccionar;
                articuloModificar.Detalle = dto.Detalle;
                articuloModificar.EstaEliminado = dto.EstaEliminado;

                context.SaveChanges();
            }
        }

        public ArticuloDto ObtenerPorId(long articuloId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var articulo = context.Articulos
                    .Include(x => x.Marca)
                    .Include(x => x.SubRubro)
                    .Include(x => x.SubRubro.Rubro)
                    .AsNoTracking()
                    .FirstOrDefault(x => x.Id == articuloId);

                if (articulo == null) throw new Exception("No se encontro el Articulo");

                return new ArticuloDto
                {
                    Id = articulo.Id,
                    Codigo = articulo.Codigo,
                    CodigoBarra = articulo.CodigoBarra,
                    Descripcion = articulo.Descripcion,
                    Abreviatura = articulo.Abreviatura,
                    MarcaId = articulo.MarcaId,
                    Marca = articulo.Marca.Descripcion,
                    SubRubroId = articulo.SubRubroId,
                    SubRubro = articulo.SubRubro.Descripcion,
                    RubroId = articulo.SubRubro.RubroId,
                    Rubro = articulo.SubRubro.Rubro.Descripcion,
                    Foto = articulo.Foto,
                    ActivarLimiteVenta = articulo.ActivarLimiteVenta,
                    CantidadLimiteVenta = articulo.CantidadLimiteVenta,
                    PermiteStockNegativo = articulo.PermiteStockNegativo,
                    EstaDiscontinuado = articulo.EstaDiscontinuado,
                    TipoArticulo = articulo.TipoArticulo,
                    StockMaximo = articulo.StockMaximo,
                    StockMinimo = articulo.StockMinimo,
                    DescuentaStock = articulo.DescuentaStock,
                    SePuedeFraccionar = articulo.SePuedeFraccionar,
                    Detalle = articulo.Detalle,
                    EstaEliminado = articulo.EstaEliminado
                };
            }
        }

        public IEnumerable<ArticuloDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Articulos
                    .Include(x => x.Marca)
                    .Include(x => x.SubRubro)
                    .Include(x => x.SubRubro.Rubro)
                    .Include(x => x.Stocks)
                    .Include("Stocks.Deposito")
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && x.Stocks.Any(s => s.Deposito.EmpresaId == empresaId)
                                && (x.Codigo == cadenaBuscar
                                    || x.CodigoBarra == cadenaBuscar
                                    || x.Abreviatura == cadenaBuscar
                                    || x.Descripcion.Contains(cadenaBuscar)))
                    .Select(x => new ArticuloDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        CodigoBarra = x.CodigoBarra,
                        Descripcion = x.Descripcion,
                        Abreviatura = x.Abreviatura,
                        MarcaId = x.MarcaId,
                        Marca = x.Marca.Descripcion,
                        SubRubroId = x.SubRubroId,
                        SubRubro = x.SubRubro.Descripcion,
                        RubroId = x.SubRubro.RubroId,
                        Rubro = x.SubRubro.Rubro.Descripcion,
                        Foto = x.Foto,
                        ActivarLimiteVenta = x.ActivarLimiteVenta,
                        CantidadLimiteVenta = x.CantidadLimiteVenta,
                        PermiteStockNegativo = x.PermiteStockNegativo,
                        EstaDiscontinuado = x.EstaDiscontinuado,
                        TipoArticulo = x.TipoArticulo,
                        StockMaximo = x.StockMaximo,
                        StockMinimo = x.StockMinimo,
                        DescuentaStock = x.DescuentaStock,
                        SePuedeFraccionar = x.SePuedeFraccionar,
                        Detalle = x.Detalle,
                        EstaEliminado = x.EstaEliminado,
                    }).OrderBy(x => x.Descripcion)
                    .ToList();
                
            }
        }

        public IEnumerable<ArticuloDto> ObtenerPorRubro(long empresaId, long rubroId, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.Articulos
                    .Include(x => x.Marca)
                    .Include(x => x.SubRubro)
                    .Include(x => x.SubRubro.Rubro)
                    .Include(x => x.Stocks)
                    .Include("Stocks.Deposito")
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && x.Stocks.Any(s => s.Deposito.EmpresaId == empresaId)
                                && x.SubRubro.RubroId == rubroId)
                    .Select(x => new ArticuloDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        CodigoBarra = x.CodigoBarra,
                        Descripcion = x.Descripcion,
                        Abreviatura = x.Abreviatura,
                        MarcaId = x.MarcaId,
                        Marca = x.Marca.Descripcion,
                        SubRubroId = x.SubRubroId,
                        SubRubro = x.SubRubro.Descripcion,
                        RubroId = x.SubRubro.RubroId,
                        Rubro = x.SubRubro.Rubro.Descripcion,
                        Foto = x.Foto,
                        ActivarLimiteVenta = x.ActivarLimiteVenta,
                        CantidadLimiteVenta = x.CantidadLimiteVenta,
                        PermiteStockNegativo = x.PermiteStockNegativo,
                        EstaDiscontinuado = x.EstaDiscontinuado,
                        TipoArticulo = x.TipoArticulo,
                        StockMaximo = x.StockMaximo,
                        StockMinimo = x.StockMinimo,
                        DescuentaStock = x.DescuentaStock,
                        SePuedeFraccionar = x.SePuedeFraccionar,
                        Detalle = x.Detalle,
                        EstaEliminado = x.EstaEliminado
                    }).OrderBy(x => x.Descripcion)
                    .ToList();
            }
        }
    }
}
