﻿using System;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Articulo.DTOs;

namespace XCommerce.LogicaNegocio.Articulo
{
    public class IngredienteServicio
    {
        public long Insertar(IngredienteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoIngrediente = new Ingrediente
                {
                    ArticuloPadreId = dto.ArticuloPadreId,
                    ArticuloHijoId = dto.ArticuloHijoId,
                    Cantidad = dto.Cantidad
                };

                context.Ingredientes.Add(nuevoIngrediente);

                context.SaveChanges();

                return nuevoIngrediente.Id;
            }
        }

        public void Modificar(IngredienteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var modificarIngrediente = context.Ingredientes.FirstOrDefault(x => x.Id == dto.Id);

                if(modificarIngrediente == null) throw new Exception("No se encontro el Ingrediente");

                modificarIngrediente.Cantidad = dto.Cantidad;

                context.SaveChanges();
            }
        }

        public IngredienteDto ObtenerPorId(long ingredienteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var ingrediente = context.Ingredientes.FirstOrDefault(x => x.Id == ingredienteId);

                if (ingrediente == null) throw new Exception("No se encontro el Ingrediente");

                return new IngredienteDto
                {
                    Id = ingrediente.Id,
                    ArticuloPadreId = ingrediente.ArticuloPadreId,
                    ArticuloHijoId = ingrediente.ArticuloHijoId,
                    Cantidad = ingrediente.Cantidad
                };
            }
        }
    }
}
