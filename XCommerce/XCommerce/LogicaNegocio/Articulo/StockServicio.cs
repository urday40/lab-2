﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Articulo.DTOs;

namespace XCommerce.LogicaNegocio.Articulo
{
    public class StockServicio
    {
        //public IEnumerable<StockDto> Obtener(string cadenaBuscar)
        //{
        //    using (var context = new ModeloDatosContainer())
        //    {
        //        long entrada = Convert.ToInt64(cadenaBuscar);
        //        decimal entrada2 = Convert.ToDecimal(cadenaBuscar);
        //        return context.Stocks.AsNoTracking()
        //            .Where(x => x.DepositoId == entrada
        //                        || x.ArticuloId == entrada
        //                        || x.Cantidad == entrada2)
        //            .Select(x => new Stock
        //            {
        //                Id = x.Id,
        //                Articulo = x.Articulo,
        //                Cantidad = x.Cantidad,
        //                ArticuloId = x.ArticuloId,
        //                BajasArticulos = new List<BajaArticulo>(),
        //                DepositoId = x.DepositoId,
        //                Deposito = x.Deposito
        //            }).ToList();
        //    }
        //}

        public void AgregarStock(long articuloId, long depositoId, decimal cantidad)
        {
            using (var context = new ModeloDatosContainer())
            {
                var agregarStock = new Stock
                {
                    ArticuloId = articuloId,
                    DepositoId = depositoId,
                    Cantidad = cantidad,                 
                };
                context.Stocks.Add(agregarStock);
               
            }

           
        }

        public StockDto ObtenerPorId(long stockId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var stock = context.Stocks.AsNoTracking().FirstOrDefault(x => x.Id == stockId);

                if (stock == null) throw new Exception("No se encontro el Stock");

                return new StockDto
                {
                    Id = stock.Id,

                    DepositoId = stock.DepositoId,

                    ArticuloId = stock.ArticuloId,

                    Cantidad = stock.Cantidad
                };
            }
        }

        public void Modificar(StockDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var modificarStock = context.Stocks.FirstOrDefault(x => x.Id == dto.Id);

                if (modificarStock == null) throw new Exception("No se encontro el Stock");
                modificarStock.DepositoId = dto.DepositoId;
                modificarStock.Cantidad = dto.Cantidad;

                context.SaveChanges();
            }
        }
    }
}
