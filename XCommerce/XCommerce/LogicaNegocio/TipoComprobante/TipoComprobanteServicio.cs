﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.TipoComprobante.DTOs;

namespace XCommerce.LogicaNegocio.TipoComprobante
{
    public class TipoComprobanteServicio
    {
        public long Insertar(TipoComprobanteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoTipoComprobante = new AccesoDatos.TipoComprobante
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminado = dto.EstaEliminado,
                    Letra = dto.Letra,
                    EmpresaId = dto.EmpresaId
                };

                context.TipoComprobantes.Add(nuevoTipoComprobante);
                context.SaveChanges();

                return nuevoTipoComprobante.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var condicionIvaEliminar = context.TipoComprobantes
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (condicionIvaEliminar == null) throw new Exception("No se encontro el Tipo de Comprobante");

                condicionIvaEliminar.EstaEliminado = !condicionIvaEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(TipoComprobanteDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var tipoComprobanteModificar = context.TipoComprobantes
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (tipoComprobanteModificar == null) throw new Exception("No se encontro el Tipo de Comprobante");

                tipoComprobanteModificar.Codigo = dto.Codigo;
                tipoComprobanteModificar.Descripcion = dto.Descripcion;
                tipoComprobanteModificar.Letra = dto.Letra;
                tipoComprobanteModificar.EmpresaId = dto.EmpresaId;

                context.SaveChanges();
            }
        }

        public TipoComprobanteDto ObtenerPorId(long tipoComprobanteId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var tipoComprobante = context.TipoComprobantes
                    .FirstOrDefault(x => x.Id == tipoComprobanteId);

                if (tipoComprobante == null) throw new Exception("No se encontro el Tipo de Comprobante");

                return new TipoComprobanteDto
                {
                    Id = tipoComprobante.Id,
                    Codigo = tipoComprobante.Codigo,
                    Descripcion = tipoComprobante.Descripcion,
                    EstaEliminado = tipoComprobante.EstaEliminado,
                    Letra = tipoComprobante.Letra,
                    EmpresaId = tipoComprobante.EmpresaId
                };
            }
        }

        public IEnumerable<TipoComprobanteDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.TipoComprobantes
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && x.EmpresaId == empresaId
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo))
                    .Select(x => new TipoComprobanteDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado,
                        Letra = x.Letra,
                        EmpresaId = x.EmpresaId
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo(long  empresaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.TipoComprobantes.AsNoTracking().Any(x=>x.EmpresaId == empresaId)
                    ? context.TipoComprobantes.Where(x=>x.EmpresaId == empresaId).Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long empresaId, int codigo, string descripcion, string letra, long? tipoComprobanteId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (tipoComprobanteId.HasValue)
                {
                    return context.TipoComprobantes
                        .AsNoTracking()
                        .Any(x =>x.EmpresaId == empresaId && x.Id != tipoComprobanteId && (x.Descripcion == descripcion || x.Codigo == codigo || x.Letra == letra));
                }
                else
                {
                    return context.TipoComprobantes
                        .AsNoTracking()
                        .Any(x => x.EmpresaId == empresaId && (x.Descripcion == descripcion || x.Codigo == codigo || x.Letra == letra));
                }
            }
        }
    }
}
