﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.SubRubro.DTOs;

namespace XCommerce.LogicaNegocio.SubSubRubro
{
    public class SubRubroServicio
    {
        public long Insertar(SubRubroDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var nuevoSubRubro = new AccesoDatos.SubRubro
                {
                    Codigo = dto.Codigo,
                    Descripcion = dto.Descripcion,
                    EstaEliminado = dto.EstaEliminado,
                    RubroId = dto.RubroId
                };

                context.SubRubros.Add(nuevoSubRubro);
                context.SaveChanges();

                return nuevoSubRubro.Id;
            }
        }

        public void Eliminar(long condicionIvaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var rubroEliminar = context.SubRubros
                    .FirstOrDefault(x => x.Id == condicionIvaId);

                if (rubroEliminar == null) throw new Exception("No se encontro la Sub-Rubro");

                rubroEliminar.EstaEliminado = !rubroEliminar.EstaEliminado;
                context.SaveChanges();
            }
        }

        public void Modificar(SubRubroDto dto)
        {
            using (var context = new ModeloDatosContainer())
            {
                var rubroModificar = context.SubRubros
                    .FirstOrDefault(x => x.Id == dto.Id);

                if (rubroModificar == null) throw new Exception("No se encontro la Sub-Rubro");

                rubroModificar.Codigo = dto.Codigo;
                rubroModificar.Descripcion = dto.Descripcion;
                rubroModificar.RubroId = dto.RubroId;

                context.SaveChanges();
            }
        }

        public SubRubroDto ObtenerPorId(long subRubroId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var subRubro = context.SubRubros
                    .Include("Rubro")
                    .FirstOrDefault(x => x.Id == subRubroId);

                if (subRubro == null) throw new Exception("No se encontro la Sub-Rubro");

                return new SubRubroDto
                {
                    Id = subRubro.Id,
                    Codigo = subRubro.Codigo,
                    Descripcion = subRubro.Descripcion,
                    EstaEliminado = subRubro.EstaEliminado,
                    RubroId = subRubro.RubroId,
                    Rubro = subRubro.Rubro.Descripcion
                };
            }
        }

        public IEnumerable<SubRubroDto> Obtener(long empresaId, string cadenaBuscar, bool estado = false)
        {
            using (var context = new ModeloDatosContainer())
            {
                int codigo = -1;
                int.TryParse(cadenaBuscar, out codigo);

                return context.SubRubros
                    .Include("Rubro")
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == estado
                                && (x.Descripcion.Contains(cadenaBuscar)
                                    || x.Codigo == codigo
                                    || x.Rubro.Descripcion.Contains(cadenaBuscar)))
                    .Select(x => new SubRubroDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado,
                        RubroId = x.RubroId,
                        Rubro = x.Rubro.Descripcion
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public IEnumerable<SubRubroDto> Obtener(long rubroId)
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.SubRubros
                    .Include("Rubro")
                    .AsNoTracking()
                    .Where(x => x.EstaEliminado == false
                                && x.RubroId == rubroId)
                    .Select(x => new SubRubroDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado,
                        RubroId = x.RubroId,
                        Rubro = x.Rubro.Descripcion
                    }).OrderBy(x => x.Codigo)
                    .ToList();
            }
        }

        public int ObtenerSiguienteCodigo()
        {
            using (var context = new ModeloDatosContainer())
            {
                return context.SubRubros.AsNoTracking().Any()
                    ? context.SubRubros.Max(x => x.Codigo) + 1
                    : 1;
            }
        }

        public bool VerificarSiExiste(long rubroId, int codigo, string descripcion, long? SubrubroId = null)
        {
            using (var context = new ModeloDatosContainer())
            {
                if (SubrubroId.HasValue)
                {
                    return context.SubRubros
                        .AsNoTracking()
                        .Any(x => x.Id != SubrubroId && x.RubroId == rubroId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
                else
                {
                    return context.SubRubros
                        .AsNoTracking()
                        .Any(x => x.RubroId == rubroId && (x.Descripcion == descripcion || x.Codigo == codigo));
                }
            }
        }
    }
}
