﻿using XCommerce.LogicaNegocio.Comprobante.DTOs;

namespace XCommerce.LogicaNegocio.Comprobante
{
    public class Comprobante
    {
        public virtual void Generar(ComprobanteDto dto)
        {
        }

        public virtual void AgregarItem(ItemDto item, long comprobanteId, long? mozoId, int cantidadComensales)
        {

        }
        public virtual void AgregarItemKiosco(ItemDto item, long comprobanteId)
        {

        }


        public virtual ComprobanteDto Obtener(long mesaId)
        {
            return null;
        }
    }
}
