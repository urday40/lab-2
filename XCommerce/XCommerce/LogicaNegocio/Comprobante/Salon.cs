﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.Base.Clases;
using XCommerce.LogicaNegocio.Comprobante.DTOs;

namespace XCommerce.LogicaNegocio.Comprobante
{
    public class Salon : Comprobante
    {
        public override void Generar(ComprobanteDto dto)
        {
            var salonDto = (SalonDto) dto;

            using (var context = new ModeloDatosContainer())
            {
                var cliente = context.Personas.OfType<AccesoDatos.Cliente>()
                    .FirstOrDefault(x => x.Visible == false
                                         && x.EstaEliminado == false
                                         && x.Dni == Constante.DniClientePorDefecto);

                if (cliente == null) throw new Exception("No se encontro el cliente consumidor final");

                var mesa = context.Mesas.FirstOrDefault(x => x.Id == salonDto.MesaId);

                if (mesa == null) throw new Exception("No se encontro la mesa");

                var nuevoComprobante = new AccesoDatos.Salon
                {
                    EmpresaId = salonDto.EmpresaId,
                    UsuarioId = salonDto.UsuarioId,
                    Cliente = cliente,
                    Comensales = salonDto.Comensales,
                    Estado = EstadoSalon.EnProceso,
                    Fecha = salonDto.Fecha,
                    MesaId = salonDto.MesaId,
                    Descuento = salonDto.Descuento,
                    Numero = 1,
                    TipoComprobanteId = salonDto.TipoComprobanteId,
                    SubTotal = salonDto.SubTotal,
                    Total = salonDto.Total,
                    DetalleComprobantes = new List<DetalleComprobante>()
                };

                context.Comprobantes.Add(nuevoComprobante);
                mesa.EstadoMesa = EstadoMesa.Abierta;

                context.SaveChanges();
            }
        }

        public override void AgregarItem(ItemDto item, long comprobanteId, long? mozoId, int cantidadComensales)
        {
            using (var context = new ModeloDatosContainer())
            {
                var comprobante = context.Comprobantes.OfType<AccesoDatos.Salon>()
                    .Include(x=>x.DetalleComprobantes)
                    .FirstOrDefault(x => x.Id == comprobanteId);

                if(comprobante == null) throw new Exception("No se encontro el comprobante.");

                if (mozoId.HasValue)
                    comprobante.EmpleadoId = mozoId.Value;

                comprobante.Comensales = cantidadComensales;

                if (comprobante.DetalleComprobantes.Any(x => x.ArticuloId == item.ArticuloId))
                {
                    // Se incrementa la Cantidad del Item.
                    var detalle = comprobante.DetalleComprobantes.First(x => x.ArticuloId == item.ArticuloId);
                    detalle.Cantidad = detalle.Cantidad + 1;
                }
                else
                {
                    var nuevoDetalle = new DetalleComprobante
                    {
                        Codigo = item.Codigo,
                        ArticuloId = item.ArticuloId,
                        Descripcion = item.Descripcion,
                        Cantidad = 1,
                        PrecioUnitario = item.PrecioUnitario
                    };

                    comprobante.DetalleComprobantes.Add(nuevoDetalle);
                }

                comprobante.SubTotal = comprobante.DetalleComprobantes.Any()
                    ? comprobante.DetalleComprobantes.Sum(x => x.Cantidad * x.PrecioUnitario)
                    : 0;

                comprobante.Total = comprobante.SubTotal;

                context.SaveChanges();
            }
        }

        public override ComprobanteDto Obtener(long mesaId)
        {
            using (var context = new ModeloDatosContainer())
            {
                var comprobante = context.Comprobantes.OfType<AccesoDatos.Salon>()
                    .Include(x => x.Mesa)
                    .Include(x => x.Empleado)
                    .Include(x => x.DetalleComprobantes)
                    .FirstOrDefault(x => x.MesaId == mesaId && x.Estado == EstadoSalon.EnProceso);

                if (comprobante != null)
                {
                    var comprobanteDto = new ComprobanteDto()
                    {
                        Id = comprobante.Id,
                        EmpresaId = comprobante.EmpresaId,
                        ClienteId = comprobante.ClienteId,
                        Comensales = comprobante.Comensales,
                        Descuento = comprobante.Descuento,
                        EmpleadoId = comprobante.EmpresaId,
                        Fecha = comprobante.Fecha,
                        UsuarioId = comprobante.UsuarioId,
                        TipoComprobanteId = comprobante.TipoComprobanteId,
                        MesaId = comprobante.MesaId,
                        Items = new List<ItemDto>()
                    };

                    foreach (var detalle in comprobante.DetalleComprobantes)
                    {
                        var item = new ItemDto
                        {
                             Codigo = detalle.Codigo,
                             Descripcion = detalle.Descripcion,
                             Id = detalle.Id,
                             ArticuloId = detalle.ArticuloId,
                             Cantidad = detalle.Cantidad,
                             PrecioUnitario = detalle.PrecioUnitario,
                        };

                        comprobanteDto.Items.Add(item);
                    }

                    return comprobanteDto;
                }

                return null;
            }
        }
    }
}
