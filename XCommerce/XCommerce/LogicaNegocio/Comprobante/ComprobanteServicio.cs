﻿using System;
using System.Collections.Generic;
using XCommerce.LogicaNegocio.Comprobante.DTOs;

namespace XCommerce.LogicaNegocio.Comprobante
{
    public class ComprobanteServicio
    {
        private readonly Dictionary<Type, string> _diccionarioComprobantes;

        public ComprobanteServicio()
        {
            _diccionarioComprobantes = new Dictionary<Type, string>();
            InicializadorDelDiccionario(ref _diccionarioComprobantes);
        }

        private void InicializadorDelDiccionario(ref Dictionary<Type, string> diccionarioComprobantes)
        {
            diccionarioComprobantes.Add(typeof(DeliveryDto), "XCommerce.LogicaNegocio.Comprobante.Delivery");
            diccionarioComprobantes.Add(typeof(SalonDto), "XCommerce.LogicaNegocio.Comprobante.Salon");
            diccionarioComprobantes.Add(typeof(VentaDto), "XCommerce.LogicaNegocio.Comprobante.Venta");
        }

        private Comprobante InstanciarUnObjeto(string valor)
        {
            var typeObject = Type.GetType(valor);
            var entidad = Activator.CreateInstance(typeObject) as Comprobante;
            return entidad;
        }

        public void Generar(ComprobanteDto dto)
        {
            string tipoEntidad;
            if (_diccionarioComprobantes.TryGetValue(dto.GetType(), out tipoEntidad))
                throw new Exception("No hay clases para instanciar");

            var comprobante = InstanciarUnObjeto(tipoEntidad);
            comprobante.Generar(dto);
        }

        public ComprobanteDto Obtener(long mesaId)
        {
            string tipoEntidad;
            if (!_diccionarioComprobantes.TryGetValue(typeof(SalonDto), out tipoEntidad))
                throw new Exception("No hay clases para instanciar");

            var comprobante = InstanciarUnObjeto(tipoEntidad);
            return comprobante.Obtener(mesaId);
        }

        public void AgregarItem(ItemDto itemDto, long comprobanteId, long? mozoId, int cantidadComensales)
        {
            string tipoEntidad;
            if (!_diccionarioComprobantes.TryGetValue(typeof(SalonDto), out tipoEntidad))
                throw new Exception("No hay clases para instanciar");

            var comprobante = InstanciarUnObjeto(tipoEntidad);
            comprobante.AgregarItem(itemDto, comprobanteId, mozoId, cantidadComensales);
        }

        public void AgregarItemKiosco(ItemDto itemDto, long comprobanteId)
        {
            string tipoEntidad;
            if (!_diccionarioComprobantes.TryGetValue(typeof(SalonDto), out tipoEntidad))
                throw new Exception("No hay clases para instanciar");

            var comprobante = InstanciarUnObjeto(tipoEntidad);
            comprobante.AgregarItemKiosco(itemDto, comprobanteId);
        }


    }
}
