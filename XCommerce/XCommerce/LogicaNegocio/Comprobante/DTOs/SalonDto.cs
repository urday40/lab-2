﻿namespace XCommerce.LogicaNegocio.Comprobante.DTOs
{
    public class SalonDto : ComprobanteDto
    {
        public new long MesaId { get; set; }
    }
}
