﻿using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Reflection;
using XCommerce.AccesoDatos;
using XCommerce.LogicaNegocio.Formulario.DTOs;

namespace XCommerce.LogicaNegocio.Formulario
{
    public class FormularioServicio
    {
        public IEnumerable<FormularioDto> Obtener(string cadenaBuscar, Assembly assembly)
        {
            var listaFormularios = new List<FormularioDto>();

            using (var context = new ModeloDatosContainer())
            {
                var formulariosDb = context.Formularios
                    .AsNoTracking()
                    .ToList();

                foreach (var obj in assembly.GetTypes())
                {
                    if(!obj.Name[0].Equals('_')) continue;

                    var formulario = new FormularioDto
                    {
                        Codigo = obj.Name.Substring(1,5),
                        Descripcion = obj.Name.Substring(7, obj.Name.Length - 7),
                        DescripcionCompleta = obj.Name,
                        Existe = formulariosDb.Any(x=>x.DescripcionCompleta == obj.Name)
                    };

                    listaFormularios.Add(formulario);
                }
            }

            return listaFormularios
                .Where(x=>x.Codigo == cadenaBuscar
                || x.Descripcion.Contains(cadenaBuscar))
                .ToList();
        }

        public void Insertar(List<FormularioDto> formularios)
        {
            using (var context = new ModeloDatosContainer())
            {
                foreach (var dto in formularios)
                {
                    context.Formularios.Add(new AccesoDatos.Formulario
                    {
                        Codigo =dto.Codigo,
                        Descripcion = dto.Descripcion,
                        DescripcionCompleta = dto.DescripcionCompleta,
                        EstaEliminado = false
                    });
                }
                context.SaveChanges();
            }
        }
    }
}
