﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Rubro;
using XCommerce.LogicaNegocio.Seguridad;
using XCommerce.LogicaNegocio.SubRubro.DTOs;
using XCommerce.LogicaNegocio.SubSubRubro;

namespace XCommerce.Core
{
    public partial class _00033_ABM_SubRubro : FormularioAbm
    {
        private readonly SubRubroServicio _subRubroServicio;
        private readonly RubroServicio _rubroServicio;
        private readonly SeguridadServicio _seguridadServicio;

        public _00033_ABM_SubRubro()
        {
            InitializeComponent();
        }

        public _00033_ABM_SubRubro(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _subRubroServicio = new SubRubroServicio();
            _rubroServicio = new RubroServicio();
            _seguridadServicio = new SeguridadServicio();

            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Sub-Rubro");
            AgregarControlesObligatorios(nudCodigo, "Código");
            AgregarControlesObligatorios(cmbRubro, "Rubro");

            CargarComboRubro();

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _subRubroServicio.ObtenerSiguienteCodigo();
        }

        private void CargarComboRubro()
        {
            cmbRubro.DataSource = _rubroServicio.Obtener(Identidad.EmpresaId, string.Empty);
            cmbRubro.DisplayMember = "Descripcion";
            cmbRubro.ValueMember = "Id";
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var subRubro = _subRubroServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = subRubro.Codigo;
                txtDescripcion.Text = subRubro.Descripcion;
                cmbRubro.SelectedValue = subRubro.Id;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Sub-Rubro", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_subRubroServicio.VerificarSiExiste((long) cmbRubro.SelectedValue, (int) nudCodigo.Value,
                    txtDescripcion.Text))
                {
                    var nuevaSubRubro = new SubRubroDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                        RubroId = (long) cmbRubro.SelectedValue
                    };

                    _subRubroServicio.Insertar(nuevaSubRubro);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_subRubroServicio.VerificarSiExiste((long) cmbRubro.SelectedValue, (int) nudCodigo.Value,
                    txtDescripcion.Text, EntidadId))
                {
                    var modificarSubRubro = new SubRubroDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                        RubroId = (long) cmbRubro.SelectedValue
                    };

                    _subRubroServicio.Modificar(modificarSubRubro);
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _subRubroServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _subRubroServicio.ObtenerSiguienteCodigo();
        }

        private void btnNuevoRubro_Click(object sender, System.EventArgs e)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fFormularioNuevo = new _00031_ABM_Rubro(TipoOperacion.Insert);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
                {
                    fFormularioNuevo.ShowDialog();
                    if (fFormularioNuevo.RealizoAlgunaOperacion)
                    {
                        CargarComboRubro();
                    }
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }
    }
}
