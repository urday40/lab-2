﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XCommerce.AccesoDatos;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.Core.Controles.FormulariosVarios;
using XCommerce.LogicaNegocio.Articulo;
using XCommerce.LogicaNegocio.Articulo.DTOs;
using XCommerce.LogicaNegocio.Comprobante;
using XCommerce.LogicaNegocio.Comprobante.DTOs;
using XCommerce.LogicaNegocio.Configuracion;
using XCommerce.LogicaNegocio.Configuracion.DTOs;
using XCommerce.LogicaNegocio.Empleado;
using XCommerce.LogicaNegocio.ListaPrecio;
using XCommerce.LogicaNegocio.TipoComprobante;

namespace XCommerce.Core
{
    public partial class _00042_PuntoVentaPorKiosco : FormularioBase
    {
        private readonly ComprobanteServicio _comprobanteServicio;
        private readonly ConfiguracionServicio _configuracionServicio;
        private readonly ArticuloServicio _articuloServicio;
        private readonly EmpleadoServicio _empleadoServicio;
        private readonly PrecioServicio _precioServicio;

        private long _kioscoId;
        private ConfiguracionDto _configuracionDto;

        private ComprobanteDto comprobante;

        public delegate void ActualizarTotal(decimal total);
        public event ActualizarTotal ActualizarTotalDelControlKiosco;
        

        public _00042_PuntoVentaPorKiosco()
        {
            InitializeComponent();

         
        }

        public _00042_PuntoVentaPorKiosco(ComprobanteServicio comprobanteServicio,
            ConfiguracionServicio configuracionServicio,
            ArticuloServicio articuloServicio, PrecioServicio precioServicio)
            : this()
        {
            _comprobanteServicio = comprobanteServicio;
            _configuracionServicio = configuracionServicio;
            _articuloServicio = articuloServicio;
            _precioServicio = precioServicio;
        }

        public _00042_PuntoVentaPorKiosco(long kioscoId)
            : this(new ComprobanteServicio(), new ConfiguracionServicio(), new ArticuloServicio(), new PrecioServicio())
        {
            _kioscoId = kioscoId;
        }

        private void AddFormInPanel(object formHijo)
        {
            if (pnlContenedor.Controls.Count > 0)
                pnlContenedor.Controls.RemoveAt(0);

            Form fh = formHijo as Form;
            fh.TopLevel = false;
            fh.FormBorderStyle = FormBorderStyle.None;
            fh.Dock = DockStyle.Fill;
            pnlContenedor.Controls.Add(fh);
            pnlContenedor.Tag = fh;
            fh.Show();
        }

        public virtual void OnActualizarTotalDelControlKiosco(decimal total)
        {
            ActualizarTotalDelControlKiosco?.Invoke(total);
        }

        private void btnVolver_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void btnArticulosPorControles_Click(object sender, System.EventArgs e)
        {
            var formArticulosPorControles = new FControlArticulos();
            formArticulosPorControles.AgregarArticuloParaLaVenta += AgregarArticuloParaLaVenta;
            AddFormInPanel(formArticulosPorControles);
        }

        private void btnArticulosPorLista_Click(object sender, System.EventArgs e)
        {
            var formArticulosPorLista = new FListaArticulos();

            AddFormInPanel(formArticulosPorLista);
        }

        private void _00042_PuntoVentaPorKiosco_Load(object sender, System.EventArgs e)
        {
            _configuracionDto = new ConfiguracionServicio().Obtener(Identidad.EmpresaId);
            btnArticulosPorControles.PerformClick();
           ObtenerComprobante();
        }
        
        // Necesario revisar
        private void ObtenerComprobante()
        {
            comprobante = new ComprobanteServicio().Obtener(_kioscoId);

            if (comprobante != null)
            {
                AsignarDatosComprobante(comprobante);
            }
            else
            {
                var nuevoComprobante = new ComprobanteDto
                {
                    EmpresaId = Identidad.EmpresaId,
                    UsuarioId = Identidad.UsuarioLoginId,
                    Fecha = DateTime.Now,
                    Descuento = 0m,
                    ClienteId = _configuracionDto.ClientePorDefectoId,
                    TipoComprobanteId = _configuracionDto.TipoComprobantePorDefectoId,
                    Items = new List<ItemDto>()

                };

                new ComprobanteServicio().Generar(nuevoComprobante);
                ObtenerComprobante();
                
            }
        }

        private void AsignarDatosComprobante(ComprobanteDto comprobante)
        {
            nudTotal.Value = comprobante.Total;

            lblUsuarioLogin.Text = comprobante.ApyNomEmpleado;
            

            dgvGrilla.DataSource = comprobante.Items.ToList();

            nudTotal.Value = comprobante.Total;

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgv.Columns["Descripcion"].Visible = true;
            dgv.Columns["Descripcion"].HeaderText = "Articulo";
            dgv.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgv.Columns["PrecioUnitario"].Visible = true;
            dgv.Columns["PrecioUnitario"].HeaderText = "Precio";
            dgv.Columns["PrecioUnitario"].Width = 100;

            dgv.Columns["Cantidad"].Visible = true;
            dgv.Columns["Cantidad"].HeaderText = "Cant.";
            dgv.Columns["Cantidad"].Width = 70;
        }

        private void AgregarArticuloParaLaVenta(ArticuloDto articulo)
        {
            var precio = _precioServicio.Monto(1, 1, 1);

            var item = new ItemDto
            {
                ArticuloId = articulo.Id,
                Cantidad = 1,
                Codigo = articulo.Codigo,
                Descripcion = articulo.Descripcion,
                PrecioUnitario = precio
            };

            _comprobanteServicio.AgregarItemKiosco(item, comprobante.Id);
            ObtenerComprobante();
        }


        private void txtCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {

            }
        }

        private void nudTotal_ValueChanged_1(object sender, EventArgs e)
        {
            OnActualizarTotalDelControlKiosco(nudTotal.Value);
        }
    }
}
