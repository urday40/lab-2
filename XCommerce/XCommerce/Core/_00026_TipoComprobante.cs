﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.TipoComprobante;

namespace XCommerce.Core
{
    public partial class _00026_TipoComprobante : FormularioConsulta
    {
        private readonly TipoComprobanteServicio _tipoComprobanteServicio;

        public _00026_TipoComprobante()
            : this(new TipoComprobanteServicio())
        {
            InitializeComponent();
        }

        public _00026_TipoComprobante(TipoComprobanteServicio tipoComprobanteServicio)
            : base("Lista Tipos de Comprobantes")
        {
            _tipoComprobanteServicio = tipoComprobanteServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _tipoComprobanteServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Letra"].Visible = true;
            dgvGrilla.Columns["Letra"].Width = 120;
            dgvGrilla.Columns["Letra"].HeaderText = @"Letra";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Tipo Comprobante";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadoStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadoStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoTipoComprobante = new _00027_ABM_TipoComprobante(TipoOperacion.Insert);
            fNuevoTipoComprobante.ShowDialog();
            return fNuevoTipoComprobante.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarTipoComprobante = new _00027_ABM_TipoComprobante(TipoOperacion.Delete, entidadId);
            fEliminarTipoComprobante.ShowDialog();
            return fEliminarTipoComprobante.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarTipoComprobante = new _00027_ABM_TipoComprobante(TipoOperacion.Update, entidadId);
            fModificarTipoComprobante.ShowDialog();
            return fModificarTipoComprobante.RealizoAlgunaOperacion;
        }
    }
}
