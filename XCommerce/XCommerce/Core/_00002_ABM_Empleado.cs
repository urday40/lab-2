﻿using System;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Empleado;
using XCommerce.LogicaNegocio.Empleado.DTOs;

namespace XCommerce.Core
{
    public partial class _00002_ABM_Empleado : FormularioAbm
    {
        private readonly EmpleadoServicio _empleadoServicio;

        public _00002_ABM_Empleado()
        {
            InitializeComponent();
        }

        public _00002_ABM_Empleado(TipoOperacion operacion, long? entidadId)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _empleadoServicio = new EmpleadoServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtApellido, "Apellido");
            AgregarControlesObligatorios(txtNombre, "Nombre");
            AgregarControlesObligatorios(txtDni, "DNI");
            AgregarControlesObligatorios(rtbDomicilio, "Domicilio");

            // Validacion de Datos
            txtApellido.KeyPress += Validar.NoInyeccion;
            txtNombre.KeyPress += Validar.NoInyeccion;

            txtDni.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
            };

            txtCelular.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
            };

            txtTelefono.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
            };

            if (Operacion == TipoOperacion.Insert)
                nudLegajo.Value = _empleadoServicio.ObtenerSiguienteLegajo();
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var empleado = _empleadoServicio.ObtenerPorId(entidadId.Value);

                rtbDomicilio.Text = empleado.Domicilio;
                txtApellido.Text = empleado.Apellido;
                txtCelular.Text = empleado.Celular;
                txtDni.Text = empleado.Dni;
                txtEMail.Text = empleado.Email;
                txtNombre.Text = empleado.Nombre;
                txtTelefono.Text = empleado.Telefono;
                nudLegajo.Value = empleado.Legajo;
                dtpFechaIngreso.Value = empleado.FechaIngreso;
                dtpFechaNacimiento.Value = empleado.FechaNacimiento;
                ctrolFotoEmpleado.imgFoto.Image = Imagen.Convertir_Bytes_Imagen(empleado.Foto);

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                    ctrolFotoEmpleado.btnObtenerImagen.Enabled = false;
                }
                else
                {
                    nudLegajo.Enabled = false;
                    txtApellido.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro el Empleado", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var nuevoEmpleado = new EmpleadoDto
                {
                    Apellido = txtApellido.Text,
                    Domicilio = rtbDomicilio.Text,
                    Celular = txtCelular.Text,
                    Dni = txtDni.Text,
                    Email = txtEMail.Text,
                    Nombre = txtNombre.Text,
                    Telefono = txtTelefono.Text,
                    Legajo = (int)nudLegajo.Value,
                    FechaIngreso = dtpFechaIngreso.Value,
                    FechaNacimiento = dtpFechaNacimiento.Value,
                    Foto = Imagen.Convertir_Imagen_Bytes(ctrolFotoEmpleado.imgFoto.Image)
                };

                _empleadoServicio.Insertar(nuevoEmpleado);
                LimpiarControles(this);
                txtApellido.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var nuevoEmpleado = new EmpleadoDto
                {
                    Id = EntidadId.Value,
                    Apellido = txtApellido.Text,
                    Domicilio = rtbDomicilio.Text,
                    Celular = txtCelular.Text,
                    Dni = txtDni.Text,
                    Email = txtEMail.Text,
                    Nombre = txtNombre.Text,
                    Telefono = txtTelefono.Text,
                    Legajo = (int)nudLegajo.Value,
                    FechaIngreso = dtpFechaIngreso.Value,
                    FechaNacimiento = dtpFechaNacimiento.Value,
                    Foto = Imagen.Convertir_Imagen_Bytes(ctrolFotoEmpleado.imgFoto.Image)
                };

                _empleadoServicio.Modificar(nuevoEmpleado);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _empleadoServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudLegajo.Value = _empleadoServicio.ObtenerSiguienteLegajo();
        }
    }
}
