﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.CondicionIva;
using XCommerce.LogicaNegocio.Empresa;
using XCommerce.LogicaNegocio.Empresa.DTOs;
using XCommerce.LogicaNegocio.Seguridad;

namespace XCommerce.Core
{
    public partial class _00007_ABM_Empresa : FormularioAbm
    {
        private readonly EmpresaServicio _empresaServicio;
        private readonly CondicionIvaServicio _condicionIvaServicio;
        private readonly SeguridadServicio _seguridadServicio;

        public _00007_ABM_Empresa()
        {
            InitializeComponent();
        }

        public _00007_ABM_Empresa(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _empresaServicio = new EmpresaServicio();
            _condicionIvaServicio = new CondicionIvaServicio();
            _seguridadServicio = new SeguridadServicio();

            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            PoblarComboCondicionIva();

            AgregarControlesObligatorios(txtRazonSocial, "Razon Social");
            AgregarControlesObligatorios(txtNombreFantasia, "Nombre Fantasia");
            AgregarControlesObligatorios(txtCuit, "CUIT");
            AgregarControlesObligatorios(rtbDomicilio, "Domicilio");

            // Validacion de Datos
            txtRazonSocial.KeyPress += Validar.NoInyeccion;
            txtNombreFantasia.KeyPress += Validar.NoInyeccion;

            txtCuit.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
            };

            txtTelefono.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
            };
        }

        private void PoblarComboCondicionIva()
        {
            cmbCondicionIva.DataSource = _condicionIvaServicio.Obtener(string.Empty);
            cmbCondicionIva.DisplayMember = "Descripcion";
            cmbCondicionIva.ValueMember = "Id";
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var empresa = _empresaServicio.ObtenerPorId(entidadId.Value);

                rtbDomicilio.Text = empresa.Domicilio;
                txtRazonSocial.Text = empresa.RazonSocial;
                txtNombreFantasia.Text = empresa.NombreFantasia;
                txtCuit.Text = empresa.Cuit;
                txtEMail.Text = empresa.Email;
                txtTelefono.Text = empresa.Telefono;
                ctrolFoto1.imgFoto.Image = Imagen.Convertir_Bytes_Imagen(empresa.Logo);

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                    ctrolFoto1.btnObtenerImagen.Enabled = false;
                }
                else
                {
                    numericUpDown1.Enabled = false;
                    txtRazonSocial.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro el Empresa", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var nuevoEmpresa = new EmpresaDto
                {
                    RazonSocial = txtRazonSocial.Text,
                    Domicilio = rtbDomicilio.Text,
                    Cuit = txtCuit.Text,
                    Email = txtEMail.Text,
                    NombreFantasia = txtNombreFantasia.Text,
                    Telefono = txtTelefono.Text,
                    Sucursal = (int)numericUpDown1.Value,
                    Logo = Imagen.Convertir_Imagen_Bytes(ctrolFoto1.imgFoto.Image),
                    CondicionIvaId = (long)cmbCondicionIva.SelectedValue
                };

                _empresaServicio.Insertar(nuevoEmpresa);
                LimpiarControles(this);
                txtRazonSocial.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var empresaModificar = new EmpresaDto
                {
                    Id = EntidadId.Value,
                    RazonSocial = txtRazonSocial.Text,
                    Domicilio = rtbDomicilio.Text,
                    Cuit = txtCuit.Text,
                    Email = txtEMail.Text,
                    NombreFantasia = txtNombreFantasia.Text,
                    Telefono = txtTelefono.Text,
                    Sucursal = (int)numericUpDown1.Value,
                    Logo = Imagen.Convertir_Imagen_Bytes(ctrolFoto1.imgFoto.Image),
                    CondicionIvaId = (long)cmbCondicionIva.SelectedValue
                };

                _empresaServicio.Modificar(empresaModificar);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _empresaServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
        }

        private void btnNuevaCondicionIva_Click(object sender, System.EventArgs e)
        {
            var fNuevaCondicionIva = new _00005_ABM_CondicionDeIva(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fNuevaCondicionIva, Identidad.UsuarioLogin))
            {
                fNuevaCondicionIva.ShowDialog();
                if (fNuevaCondicionIva.RealizoAlgunaOperacion)
                {
                    PoblarComboCondicionIva();
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }
    }
}
