﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Rubro;

namespace XCommerce.Core
{
    public partial class _00030_Rubro : FormularioConsulta
    {
        private readonly RubroServicio _rubroServicio;

        public _00030_Rubro()
            : this(new RubroServicio())
        {
            InitializeComponent();
        }

        public _00030_Rubro(RubroServicio rubroServicio)
            : base("Lista Rubros")
        {
            _rubroServicio = rubroServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _rubroServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Rubro";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadoStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadoStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoRubro = new _00031_ABM_Rubro(TipoOperacion.Insert);
            fNuevoRubro.ShowDialog();
            return fNuevoRubro.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarRubro = new _00031_ABM_Rubro(TipoOperacion.Delete, entidadId);
            fEliminarRubro.ShowDialog();
            return fEliminarRubro.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarRubro = new _00031_ABM_Rubro(TipoOperacion.Update, entidadId);
            fModificarRubro.ShowDialog();
            return fModificarRubro.RealizoAlgunaOperacion;
        }
    }
}
