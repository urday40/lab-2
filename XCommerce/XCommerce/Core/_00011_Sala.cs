﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Sala;

namespace XCommerce.Core
{
    public partial class _00011_Sala : FormularioConsulta
    {
        private readonly SalaServicio _salaServicio;

        public _00011_Sala()
            : this(new SalaServicio())
        {
            InitializeComponent();
        }

        public _00011_Sala(SalaServicio salaServicio)
            : base("Lista de Salas")
        {
            _salaServicio = salaServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _salaServicio.Obtener(cadenaBuscar, Identidad.EmpresaId);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Descripción";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["CantidadMesas"].Visible = true;
            dgvGrilla.Columns["CantidadMesas"].Width = 100;
            dgvGrilla.Columns["CantidadMesas"].HeaderText = @"Cant. Mesas";

            dgvGrilla.Columns["EstaEliminadaStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadaStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadaStr"].HeaderText = @"Eliminada";
        }

        public override bool EjecutarNuevaEntidad()
        {
            if (Identidad.EmpresaId != 0)
            {
                var fNuevoSala = new _00012_ABM_Sala(TipoOperacion.Insert);
                fNuevoSala.ShowDialog();
                return fNuevoSala.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Sala.", Mensaje.Tipo.Informacion);
                return false;
            }
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fEliminarSala = new _00012_ABM_Sala(TipoOperacion.Delete, entidadId);
                fEliminarSala.ShowDialog();
                return fEliminarSala.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Sala.", Mensaje.Tipo.Informacion);
                return false;
            }
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fModificarSala = new _00012_ABM_Sala(TipoOperacion.Update, entidadId);
                fModificarSala.ShowDialog();
                return fModificarSala.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Sala.", Mensaje.Tipo.Informacion);
                return false;
            }
        }
    }
}
