﻿using System;
using System.Net.Mime;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Empresa.DTOs;
using XCommerce.LogicaNegocio.EmpresaEmpleado;

namespace XCommerce.Core
{
    public partial class _00010_EmpresaParaTrabajar : FormularioBase
    {
        private long _empleadoId;
        private EmpresaDto _empresaDto;

        private readonly EmpresaEmpleadoServicio _empresaEmpleadoServicio;

        public _00010_EmpresaParaTrabajar()
            : this(new EmpresaEmpleadoServicio())
        {
            InitializeComponent();
        }

        public _00010_EmpresaParaTrabajar(long empleadoId)
            : this()
        {
            _empleadoId = empleadoId;
        }

        public _00010_EmpresaParaTrabajar(EmpresaEmpleadoServicio empresaEmpleadoServicio)
        {
            _empresaEmpleadoServicio = empresaEmpleadoServicio;
        }

        private void btnCancelar_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void _00010_EmpresaParaTrabajar_Load(object sender, System.EventArgs e)
        {
            ActualizarDatos(_empleadoId);
        }

        private void ActualizarDatos(long empleadoId)
        {
            dgvGrilla.DataSource = _empresaEmpleadoServicio.ObtenerEmpresasDelEmpleado(empleadoId);

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Sucursal"].Visible = true;
            dgvGrilla.Columns["Sucursal"].Width = 80;
            dgvGrilla.Columns["Sucursal"].HeaderText = @"Sucursal";

            dgvGrilla.Columns["RazonSocial"].Visible = true;
            dgvGrilla.Columns["RazonSocial"].HeaderText = @"Razon Social";
            dgvGrilla.Columns["RazonSocial"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["CUIT"].Visible = true;
            dgvGrilla.Columns["CUIT"].Width = 80;
            dgvGrilla.Columns["CUIT"].HeaderText = @"CUIT";
        }

        private void dgvGrilla_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvGrilla.RowCount > 0)
            {
                _empresaDto = (EmpresaDto) dgvGrilla.Rows[e.RowIndex].DataBoundItem;
            }
            else
            {
                _empresaDto = null;
            }
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (_empresaDto == null)
            {
                Mensaje.Mostrar("Por favor seleccione una Empresa", Mensaje.Tipo.Informacion);
                return;
            }

            Identidad.Empresa = _empresaDto.RazonSocial;
            Identidad.EmpresaId = _empresaDto.Id;
            Identidad.LogoEmpresa = Imagen.Convertir_Bytes_Imagen(_empresaDto.Logo);

            Close();
        }

        private void dgvGrilla_DoubleClick(object sender, EventArgs e)
        {
            btnIngresar.PerformClick();
        }
    }
}
