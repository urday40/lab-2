﻿using System;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Cliente;
using XCommerce.LogicaNegocio.Configuracion;
using XCommerce.LogicaNegocio.Configuracion.DTOs;
using XCommerce.LogicaNegocio.Deposito;
using XCommerce.LogicaNegocio.ListaPrecio;
using XCommerce.LogicaNegocio.TipoComprobante;

namespace XCommerce.Core
{
    public partial class _00036_ConfiguracionSistema : FormularioBase
    {
        private readonly ConfiguracionServicio _configuracionServicio;
        private readonly TipoComprobanteServicio _tipoComprobanteServicio;
        private readonly DepositoServicio _depositoServicio;
        private readonly ListaPrecioServicio _listaPrecioServicio;
        private readonly ClienteServicio _clienteServicio;

        public _00036_ConfiguracionSistema()
            : this(new ConfiguracionServicio(),
                  new TipoComprobanteServicio(), 
                  new DepositoServicio(), 
                  new ListaPrecioServicio(), 
                  new ClienteServicio())
        {
            InitializeComponent();
        }

        public _00036_ConfiguracionSistema(ConfiguracionServicio configuracionServicio,
            TipoComprobanteServicio tipoComprobanteServicio,
            DepositoServicio depositoServicio,
            ListaPrecioServicio listaPrecioServicio,
            ClienteServicio clienteServicio)
        {
            _configuracionServicio = configuracionServicio;
            _tipoComprobanteServicio = tipoComprobanteServicio;
            _depositoServicio = depositoServicio;
            _listaPrecioServicio = listaPrecioServicio;
            _clienteServicio = clienteServicio;
        }

        private void _00036_ConfiguracionSistema_Load(object sender, System.EventArgs e)
        {
            CargarCombosDepositos(Identidad.EmpresaId);
            CargarComboListaPrecio(Identidad.EmpresaId);
            CargarComboTipoComprobante(Identidad.EmpresaId);
            CargarDatos();
        }

        private void CargarComboTipoComprobante(long empresaId)
        {
            cmbTipoComprobante.DataSource = _tipoComprobanteServicio.Obtener(empresaId, string.Empty); ;
            cmbTipoComprobante.DisplayMember = "Descripcion";
            cmbTipoComprobante.ValueMember = "Id";
        }

        private void CargarComboListaPrecio(long empresaId)
        {
            cmbListaPrecioPorDefecto.DataSource = _listaPrecioServicio.Obtener(empresaId, string.Empty);
            cmbListaPrecioPorDefecto.DisplayMember = "Descripcion";
            cmbListaPrecioPorDefecto.ValueMember = "Id";
        }

        private void CargarCombosDepositos(long empresaId)
        {
            cmbDepositoPorDefecto.DataSource = _depositoServicio.Obtener(empresaId, string.Empty);
            cmbDepositoPorDefecto.DisplayMember = "Descripcion";
            cmbDepositoPorDefecto.ValueMember = "Id";

            cmbDepositoCompraPorDefecto.DataSource = _depositoServicio.Obtener(empresaId, string.Empty);
            cmbDepositoCompraPorDefecto.DisplayMember = "Descripcion";
            cmbDepositoCompraPorDefecto.ValueMember = "Id";
        }

        private void CargarDatos()
        {
            var config = _configuracionServicio.Obtener(Identidad.EmpresaId);

            if (config == null)
            {
                // Primera Vez
                nudMontoCubierto.Value = 0;
                nudMontoCubierto.Enabled = false;

                chkCobrarCubierto.Checked = false;

                chkAsignarProductoTodosDepositos.Checked = true;
                chkPuestoCajaSeparado.Checked = false;
            }
            else
            {
                if (cmbTipoComprobante.Items.Count > 0)
                {
                    cmbTipoComprobante.SelectedValue = config.TipoComprobantePorDefectoId;
                }

                if (cmbListaPrecioPorDefecto.Items.Count > 0)
                {
                    cmbListaPrecioPorDefecto.SelectedValue = config.ListaPrecioPorDefectoId;
                }

                if (cmbDepositoPorDefecto.Items.Count > 0)
                {
                    cmbDepositoPorDefecto.SelectedValue = config.DepositoPorDefectoId;
                }

                chkCobrarCubierto.Checked = config.SeCobraCubiertos;
                chkAsignarProductoTodosDepositos.Checked = config.GrabarProductoEnTodosDepositos;
                chkPuestoCajaSeparado.Checked = config.PuestoDeCajaSeparado;
                nudMontoCubierto.Value = config.MontoPorCubierto;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void chkCobrarCubierto_CheckedChanged(object sender, EventArgs e)
        {
            nudMontoCubierto.Enabled = chkCobrarCubierto.Checked;
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            try
            {
                var configuracion = new ConfiguracionDto
                {
                    EmpresaId = Identidad.EmpresaId,
                    DepositoPorDefectoId = (long)cmbDepositoPorDefecto.SelectedValue,
                    GrabarProductoEnTodosDepositos = chkAsignarProductoTodosDepositos.Checked,
                    ListaPrecioPorDefectoId = (long)cmbListaPrecioPorDefecto.SelectedValue,
                    MontoPorCubierto = nudMontoCubierto.Value,
                    PuestoDeCajaSeparado = chkPuestoCajaSeparado.Checked,
                    SeCobraCubiertos = chkCobrarCubierto.Checked,
                    TipoComprobantePorDefectoId = (long)cmbTipoComprobante.SelectedValue
                };

                _configuracionServicio.Insertar(configuracion);
                Mensaje.Mostrar("Los datos se grabaron correctamente.", Mensaje.Tipo.Informacion);
                Close();
            }
            catch (Exception)
            {
                Mensaje.Mostrar("Ocurrió un error al grabar la Configuración", Mensaje.Tipo.Error);
            }
        }
    }
}
