﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Deposito;

namespace XCommerce.Core
{
    public partial class _00037_Deposito : FormularioConsulta
    {
        private readonly DepositoServicio _depositoServicio;

        public _00037_Deposito()
            : this(new DepositoServicio())
        {
            InitializeComponent();
        }

        public _00037_Deposito(DepositoServicio depositoServicio)
            : base("Lista Depositos")
        {
            _depositoServicio = depositoServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _depositoServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Deposito";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadoStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadoStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoDeposito = new _00038_ABM_Deposito(TipoOperacion.Insert);
            fNuevoDeposito.ShowDialog();
            return fNuevoDeposito.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarDeposito = new _00038_ABM_Deposito(TipoOperacion.Delete, entidadId);
            fEliminarDeposito.ShowDialog();
            return fEliminarDeposito.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarDeposito = new _00038_ABM_Deposito(TipoOperacion.Update, entidadId);
            fModificarDeposito.ShowDialog();
            return fModificarDeposito.RealizoAlgunaOperacion;
        }
    }
}
