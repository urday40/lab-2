﻿using System.Windows.Forms;
using XCommerce.Base.Clases;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Seguridad;
using XCommerce.LogicaNegocio.Usuario;

namespace XCommerce.Core
{
    public partial class _00000_Login : Form
    {
        private int CantidadDeIntentosFallidos;

        private readonly SeguridadServicio _seguridadServicio;
        private readonly UsuarioServicio _usuarioServicio;

        public bool PuedeAccederAlSistema { get; private set; }


        public _00000_Login()
            : this(new SeguridadServicio(), new UsuarioServicio())
        {
            PuedeAccederAlSistema = false;
            CantidadDeIntentosFallidos = 0;
            InitializeComponent();

        }

        public _00000_Login(SeguridadServicio seguridadServicio, UsuarioServicio usuarioServicio)
        {
            _seguridadServicio = seguridadServicio;
            _usuarioServicio = usuarioServicio;

         
        }

        private void btnIngresar_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (VerificarDatosObligatorios(txtUsuario.Text, txtPassword.Text))
                {
                    var passwordEncriptada = Encriptar.EncriptarCadena(txtPassword.Text);

                    var usuarioLogin = _seguridadServicio.VerificarAccesoAlSistema(txtUsuario.Text, passwordEncriptada);

                    if (usuarioLogin != null)
                    {
                        if (!usuarioLogin.Bloqueado)
                        {
                            PuedeAccederAlSistema = true;

                            Identidad.UsuarioLogin = usuarioLogin.NombreUsuario;
                            Identidad.ApellidoEmpleado = usuarioLogin.Apellido;
                            Identidad.EmpleadoLoginId = usuarioLogin.EmpleadoId;
                            Identidad.Foto = usuarioLogin.Foto;
                            Identidad.NombreEmpleado = usuarioLogin.Nombre;
                            Identidad.EmpleadoLoginId = usuarioLogin.EmpleadoId;
                            Identidad.UsuarioLoginId = usuarioLogin.UsuarioId ?? 0; 

                            Close();
                        }
                        else
                        {
                            Mensaje.Mostrar("El Usuario esta Bloqueado, por favor comuniquese con el administrador.",
                                Mensaje.Tipo.Informacion);
                            txtUsuario.Clear();
                            txtPassword.Clear();
                            txtUsuario.Focus();
                        }
                    }
                    else
                    {
                        if (CantidadDeIntentosFallidos == ConstanteSeguridad.CantidadIngresosFallidos)
                        {
                            _usuarioServicio.CambiarEstado(txtUsuario.Text);
                            Mensaje.Mostrar("Por seguridad se Bloqueo el Usuario.", Mensaje.Tipo.Informacion);
                            Close();
                            return;
                        }

                        Mensaje.Mostrar("El Usuario o la Contraseña son Incorrectos.", Mensaje.Tipo.Error);
                        txtPassword.Clear();
                        txtPassword.Focus();
                        CantidadDeIntentosFallidos++;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Mensaje.Mostrar(ex, Mensaje.Tipo.Error);
                Close();
            }
        }

        private bool VerificarDatosObligatorios(string usuario, string password)
        {
            if (string.IsNullOrEmpty(usuario))
            {
                Mensaje.Mostrar("Por favor Ingrese un Usuario.", Mensaje.Tipo.Error);
                return false;
            }

            if (string.IsNullOrEmpty(password))
            {
                Mensaje.Mostrar("Por favor Ingrese una Contraseña.", Mensaje.Tipo.Error);
                return false;
            }

            return true;
        }

        private void btnCancelar_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }
    }
}
