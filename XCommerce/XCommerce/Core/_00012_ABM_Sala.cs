﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.CondicionIva.DTOs;
using XCommerce.LogicaNegocio.Sala;
using XCommerce.LogicaNegocio.Sala.DTOs;

namespace XCommerce.Core
{
    public partial class _00012_ABM_Sala : FormularioAbm
    {
        private readonly SalaServicio _salaServicio;

        public _00012_ABM_Sala()
        {
            InitializeComponent();
        }

        public _00012_ABM_Sala(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _salaServicio = new SalaServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Sala");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _salaServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var SalaServicio = _salaServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = SalaServicio.Codigo;
                txtDescripcion.Text = SalaServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Sala", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var nuevaSala = new SalaDto
                {
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                    EmpresaId = Identidad.EmpresaId
                };

                _salaServicio.Insertar(nuevaSala);
                LimpiarControles(this);
                txtDescripcion.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var modificarSala = new SalaDto
                {
                    Id = EntidadId.Value,
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                    EmpresaId = Identidad.EmpresaId
                };

                _salaServicio.Modificar(modificarSala);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _salaServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _salaServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
