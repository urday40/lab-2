﻿namespace XCommerce.Core
{
    partial class _00016_PuntoVentaPorMesa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTestigo = new System.Windows.Forms.Panel();
            this.nudComensales = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApyNomEmpleado = new System.Windows.Forms.TextBox();
            this.txtCodigoEmpleado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnArticulosPorLista = new System.Windows.Forms.Button();
            this.btnArticulosPorControles = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblMonto = new System.Windows.Forms.Label();
            this.lblMesa = new System.Windows.Forms.Label();
            this.imgMesa = new System.Windows.Forms.PictureBox();
            this.pnlLinea = new System.Windows.Forms.Panel();
            this.pnlItems = new System.Windows.Forms.Panel();
            this.dgvGrilla = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.nudCantidad = new System.Windows.Forms.NumericUpDown();
            this.txtArticulo = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.nudTotal = new System.Windows.Forms.NumericUpDown();
            this.lblTotal = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.pnlTestigo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudComensales)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgMesa)).BeginInit();
            this.pnlItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrilla)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidad)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsuarioLogin
            // 
            this.lblUsuarioLogin.Size = new System.Drawing.Size(8305, 18);
            // 
            // pnlTestigo
            // 
            this.pnlTestigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlTestigo.Controls.Add(this.nudComensales);
            this.pnlTestigo.Controls.Add(this.label2);
            this.pnlTestigo.Controls.Add(this.txtApyNomEmpleado);
            this.pnlTestigo.Controls.Add(this.txtCodigoEmpleado);
            this.pnlTestigo.Controls.Add(this.label1);
            this.pnlTestigo.Controls.Add(this.btnArticulosPorLista);
            this.pnlTestigo.Controls.Add(this.btnArticulosPorControles);
            this.pnlTestigo.Controls.Add(this.btnVolver);
            this.pnlTestigo.Controls.Add(this.panel2);
            this.pnlTestigo.Controls.Add(this.pnlLinea);
            this.pnlTestigo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTestigo.Location = new System.Drawing.Point(0, 0);
            this.pnlTestigo.Name = "pnlTestigo";
            this.pnlTestigo.Size = new System.Drawing.Size(984, 69);
            this.pnlTestigo.TabIndex = 7;
            // 
            // nudComensales
            // 
            this.nudComensales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudComensales.Location = new System.Drawing.Point(305, 36);
            this.nudComensales.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudComensales.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudComensales.Name = "nudComensales";
            this.nudComensales.Size = new System.Drawing.Size(87, 22);
            this.nudComensales.TabIndex = 9;
            this.nudComensales.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudComensales.ValueChanged += new System.EventHandler(this.nudComensales_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(211, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Comensal";
            // 
            // txtApyNomEmpleado
            // 
            this.txtApyNomEmpleado.Enabled = false;
            this.txtApyNomEmpleado.Location = new System.Drawing.Point(349, 10);
            this.txtApyNomEmpleado.Name = "txtApyNomEmpleado";
            this.txtApyNomEmpleado.Size = new System.Drawing.Size(257, 20);
            this.txtApyNomEmpleado.TabIndex = 7;
            // 
            // txtCodigoEmpleado
            // 
            this.txtCodigoEmpleado.Location = new System.Drawing.Point(305, 10);
            this.txtCodigoEmpleado.Name = "txtCodigoEmpleado";
            this.txtCodigoEmpleado.Size = new System.Drawing.Size(38, 20);
            this.txtCodigoEmpleado.TabIndex = 6;
            this.txtCodigoEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoEmpleado_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(247, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mozo";
            // 
            // btnArticulosPorLista
            // 
            this.btnArticulosPorLista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnArticulosPorLista.BackgroundImage = global::XCommerce.Properties.Resources.Lista;
            this.btnArticulosPorLista.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnArticulosPorLista.Location = new System.Drawing.Point(670, 23);
            this.btnArticulosPorLista.Name = "btnArticulosPorLista";
            this.btnArticulosPorLista.Size = new System.Drawing.Size(38, 36);
            this.btnArticulosPorLista.TabIndex = 4;
            this.btnArticulosPorLista.UseVisualStyleBackColor = true;
            this.btnArticulosPorLista.Click += new System.EventHandler(this.btnArticulosPorLista_Click);
            // 
            // btnArticulosPorControles
            // 
            this.btnArticulosPorControles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnArticulosPorControles.BackgroundImage = global::XCommerce.Properties.Resources.Controles;
            this.btnArticulosPorControles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnArticulosPorControles.Location = new System.Drawing.Point(628, 23);
            this.btnArticulosPorControles.Name = "btnArticulosPorControles";
            this.btnArticulosPorControles.Size = new System.Drawing.Size(36, 36);
            this.btnArticulosPorControles.TabIndex = 3;
            this.btnArticulosPorControles.UseVisualStyleBackColor = true;
            this.btnArticulosPorControles.Click += new System.EventHandler(this.btnArticulosPorControles_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVolver.Location = new System.Drawing.Point(872, 16);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(99, 30);
            this.btnVolver.TabIndex = 2;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblMonto);
            this.panel2.Controls.Add(this.lblMesa);
            this.panel2.Controls.Add(this.imgMesa);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 65);
            this.panel2.TabIndex = 1;
            // 
            // lblMonto
            // 
            this.lblMonto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonto.Location = new System.Drawing.Point(75, 33);
            this.lblMonto.Name = "lblMonto";
            this.lblMonto.Size = new System.Drawing.Size(125, 32);
            this.lblMonto.TabIndex = 2;
            this.lblMonto.Text = "$ 0,00";
            this.lblMonto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMesa
            // 
            this.lblMesa.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMesa.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMesa.Location = new System.Drawing.Point(75, 0);
            this.lblMesa.Name = "lblMesa";
            this.lblMesa.Size = new System.Drawing.Size(125, 33);
            this.lblMesa.TabIndex = 1;
            this.lblMesa.Text = "Mesa";
            this.lblMesa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imgMesa
            // 
            this.imgMesa.Dock = System.Windows.Forms.DockStyle.Left;
            this.imgMesa.Image = global::XCommerce.Properties.Resources.PlatoCubierto;
            this.imgMesa.Location = new System.Drawing.Point(0, 0);
            this.imgMesa.Name = "imgMesa";
            this.imgMesa.Size = new System.Drawing.Size(75, 65);
            this.imgMesa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMesa.TabIndex = 0;
            this.imgMesa.TabStop = false;
            // 
            // pnlLinea
            // 
            this.pnlLinea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pnlLinea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlLinea.Location = new System.Drawing.Point(0, 65);
            this.pnlLinea.Name = "pnlLinea";
            this.pnlLinea.Size = new System.Drawing.Size(984, 4);
            this.pnlLinea.TabIndex = 0;
            // 
            // pnlItems
            // 
            this.pnlItems.Controls.Add(this.dgvGrilla);
            this.pnlItems.Controls.Add(this.panel6);
            this.pnlItems.Controls.Add(this.panel4);
            this.pnlItems.Controls.Add(this.panel3);
            this.pnlItems.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlItems.Location = new System.Drawing.Point(631, 69);
            this.pnlItems.Name = "pnlItems";
            this.pnlItems.Size = new System.Drawing.Size(353, 364);
            this.pnlItems.TabIndex = 9;
            // 
            // dgvGrilla
            // 
            this.dgvGrilla.AllowUserToAddRows = false;
            this.dgvGrilla.AllowUserToDeleteRows = false;
            this.dgvGrilla.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dgvGrilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrilla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGrilla.Location = new System.Drawing.Point(0, 39);
            this.dgvGrilla.Name = "dgvGrilla";
            this.dgvGrilla.ReadOnly = true;
            this.dgvGrilla.Size = new System.Drawing.Size(353, 273);
            this.dgvGrilla.TabIndex = 13;
            this.dgvGrilla.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrilla_CellContentClick);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 312);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(353, 4);
            this.panel6.TabIndex = 12;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnAgregar);
            this.panel4.Controls.Add(this.nudCantidad);
            this.panel4.Controls.Add(this.txtArticulo);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(353, 39);
            this.panel4.TabIndex = 11;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAgregar.Location = new System.Drawing.Point(277, 4);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(71, 30);
            this.btnAgregar.TabIndex = 5;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            // 
            // nudCantidad
            // 
            this.nudCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCantidad.Location = new System.Drawing.Point(222, 6);
            this.nudCantidad.Name = "nudCantidad";
            this.nudCantidad.Size = new System.Drawing.Size(50, 26);
            this.nudCantidad.TabIndex = 4;
            // 
            // txtArticulo
            // 
            this.txtArticulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArticulo.Location = new System.Drawing.Point(6, 6);
            this.txtArticulo.Name = "txtArticulo";
            this.txtArticulo.Size = new System.Drawing.Size(210, 26);
            this.txtArticulo.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.Controls.Add(this.nudTotal);
            this.panel3.Controls.Add(this.lblTotal);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 316);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(353, 48);
            this.panel3.TabIndex = 10;
            // 
            // nudTotal
            // 
            this.nudTotal.DecimalPlaces = 2;
            this.nudTotal.Enabled = false;
            this.nudTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudTotal.Location = new System.Drawing.Point(128, 13);
            this.nudTotal.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.nudTotal.Name = "nudTotal";
            this.nudTotal.Size = new System.Drawing.Size(222, 38);
            this.nudTotal.TabIndex = 1;
            this.nudTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudTotal.ValueChanged += new System.EventHandler(this.nudTotal_ValueChanged);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.White;
            this.lblTotal.Location = new System.Drawing.Point(11, 11);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(107, 31);
            this.lblTotal.TabIndex = 0;
            this.lblTotal.Text = "TOTAL";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(627, 69);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(4, 364);
            this.panel5.TabIndex = 12;
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 69);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(627, 364);
            this.pnlContenedor.TabIndex = 13;
            // 
            // _00016_PuntoVentaPorMesa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pnlItems);
            this.Controls.Add(this.pnlTestigo);
            this.MinimumSize = new System.Drawing.Size(1000, 500);
            this.Name = "_00016_PuntoVentaPorMesa";
            this.Text = "Punto de Venta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this._00016_PuntoVentaPorMesa_Load);
            this.Controls.SetChildIndex(this.pnlTestigo, 0);
            this.Controls.SetChildIndex(this.pnlItems, 0);
            this.Controls.SetChildIndex(this.panel5, 0);
            this.Controls.SetChildIndex(this.pnlContenedor, 0);
            this.pnlTestigo.ResumeLayout(false);
            this.pnlTestigo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudComensales)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgMesa)).EndInit();
            this.pnlItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrilla)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidad)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTestigo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblMonto;
        private System.Windows.Forms.Label lblMesa;
        private System.Windows.Forms.PictureBox imgMesa;
        private System.Windows.Forms.Panel pnlLinea;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Panel pnlItems;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.DataGridView dgvGrilla;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.NumericUpDown nudCantidad;
        private System.Windows.Forms.TextBox txtArticulo;
        private System.Windows.Forms.NumericUpDown nudTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnArticulosPorLista;
        private System.Windows.Forms.Button btnArticulosPorControles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApyNomEmpleado;
        private System.Windows.Forms.TextBox txtCodigoEmpleado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudComensales;
    }
}