﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Rubro;
using XCommerce.LogicaNegocio.Rubro.DTOs;

namespace XCommerce.Core
{
    public partial class _00031_ABM_Rubro : FormularioAbm
    {
        private readonly RubroServicio _rubroServicio;

        public _00031_ABM_Rubro()
        {
            InitializeComponent();
        }

        public _00031_ABM_Rubro(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _rubroServicio = new RubroServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Rubro");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _rubroServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var rubro = _rubroServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = rubro.Codigo;
                txtDescripcion.Text = rubro.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Rubro", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_rubroServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text))
                {
                    var nuevaRubro = new RubroDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                        EmpresaId = Identidad.EmpresaId
                    };

                    _rubroServicio.Insertar(nuevaRubro);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_rubroServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text, EntidadId))
                {
                    var modificarRubro = new RubroDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                        EmpresaId = Identidad.EmpresaId
                    };

                    _rubroServicio.Modificar(modificarRubro);
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _rubroServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _rubroServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
