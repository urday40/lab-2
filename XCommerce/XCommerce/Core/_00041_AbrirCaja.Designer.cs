﻿namespace XCommerce.Core
{
    partial class _00041_AbrirCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAbrir = new System.Windows.Forms.Button();
            this.nudMontoInicial = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudMontoInicial)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAbrir
            // 
            this.btnAbrir.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbrir.Location = new System.Drawing.Point(60, 68);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(234, 102);
            this.btnAbrir.TabIndex = 0;
            this.btnAbrir.Text = "Abrir Caja";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // nudMontoInicial
            // 
            this.nudMontoInicial.DecimalPlaces = 3;
            this.nudMontoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudMontoInicial.Location = new System.Drawing.Point(442, 104);
            this.nudMontoInicial.Maximum = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.nudMontoInicial.Name = "nudMontoInicial";
            this.nudMontoInicial.Size = new System.Drawing.Size(120, 38);
            this.nudMontoInicial.TabIndex = 1;
            // 
            // _00041_AbrirCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 235);
            this.Controls.Add(this.nudMontoInicial);
            this.Controls.Add(this.btnAbrir);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(698, 274);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(698, 274);
            this.Name = "_00041_AbrirCaja";
            this.Text = "_00041_AbrirCaja";
            ((System.ComponentModel.ISupportInitialize)(this.nudMontoInicial)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.NumericUpDown nudMontoInicial;
    }
}