﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.SubSubRubro;

namespace XCommerce.Core
{
    public partial class _00032_SubRubro : FormularioConsulta
    {
        private readonly SubRubroServicio _marcaServicio;

        public _00032_SubRubro()
            : this(new SubRubroServicio())
        {
            InitializeComponent();
        }

        public _00032_SubRubro(SubRubroServicio marcaServicio)
            : base("Lista Sub-Rubros")
        {
            _marcaServicio = marcaServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _marcaServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"SubRubro";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["Rubro"].Visible = true;
            dgvGrilla.Columns["Rubro"].HeaderText = @"Rubro";
            dgvGrilla.Columns["Rubro"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadoStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadoStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoSubRubro = new _00033_ABM_SubRubro(TipoOperacion.Insert);
            fNuevoSubRubro.ShowDialog();
            return fNuevoSubRubro.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarSubRubro = new _00033_ABM_SubRubro(TipoOperacion.Delete, entidadId);
            fEliminarSubRubro.ShowDialog();
            return fEliminarSubRubro.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarSubRubro = new _00033_ABM_SubRubro(TipoOperacion.Update, entidadId);
            fModificarSubRubro.ShowDialog();
            return fModificarSubRubro.RealizoAlgunaOperacion;
        }
    }
}
