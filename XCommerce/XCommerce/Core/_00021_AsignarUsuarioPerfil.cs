﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Perfil;
using XCommerce.LogicaNegocio.UsuarioPerfil;
using XCommerce.LogicaNegocio.UsuarioPerfil.DTOs;

namespace XCommerce.Core
{
    public partial class _00021_AsignarUsuarioPerfil : FormularioBase
    {
        private long _perfilId;
        private readonly PerfilServicio _perfilServicio;
        private readonly UsuarioPerfilServicio _usuarioPerfilServicio;

        public _00021_AsignarUsuarioPerfil()
        {
            InitializeComponent();
        }

        public _00021_AsignarUsuarioPerfil(long perfilId)
            : this(new PerfilServicio(), new UsuarioPerfilServicio())
        {
            _perfilId = perfilId;
        }

        public _00021_AsignarUsuarioPerfil(PerfilServicio perfilServicio, UsuarioPerfilServicio usuarioPerfilServicio)
            : this()
        {
            _perfilServicio = perfilServicio;
            _usuarioPerfilServicio = usuarioPerfilServicio;
        }

        private void _00021_AsignarUsuarioPerfil_Load(object sender, System.EventArgs e)
        {
            CargarDatoPerfil();
            CargarDatos(string.Empty);
        }

        private void CargarDatoPerfil()
        {
            var perfil = _perfilServicio.ObtenerPorId(_perfilId);
            txtPerfil.Text = perfil.Descripcion;
        }

        private void CargarDatos(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _usuarioPerfilServicio
                .ObtenerUsuariosNoAsignados(cadenaBuscar, _perfilId);

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Item"].Visible = true;
            dgvGrilla.Columns["Item"].Width = 40;
            dgvGrilla.Columns["Item"].HeaderText = @"Item";

            dgvGrilla.Columns["ApyNomEmpleado"].Visible = true;
            dgvGrilla.Columns["ApyNomEmpleado"].HeaderText = @"Apellido y Nombre";
            dgvGrilla.Columns["ApyNomEmpleado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["DniEmpleado"].Visible = true;
            dgvGrilla.Columns["DniEmpleado"].Width = 120;
            dgvGrilla.Columns["DniEmpleado"].HeaderText = @"DNI";

            dgvGrilla.Columns["Usuario"].Visible = true;
            dgvGrilla.Columns["Usuario"].Width = 120;
            dgvGrilla.Columns["Usuario"].HeaderText = @"Usuario";
        }

        private void btnAsignarEmpleados_Click(object sender, EventArgs e)
        {
            try
            {
                var usuariosSeleccionados = (List<UsuarioPerfilDto>)dgvGrilla.DataSource;
                _usuarioPerfilServicio
                    .AsignarUsuarios(_perfilId, usuariosSeleccionados.Where(x => x.Item).ToList());
                CargarDatos(string.Empty);
            }
            catch (Exception exception)
            {
                Mensaje.Mostrar(exception, Mensaje.Tipo.Error);
                CargarDatos(string.Empty);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDatos(txtBuscar.Text);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarDatos(string.Empty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = false;
            }
        }

        private void dgvGrilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((DataGridView)sender).RowCount <= 0) return;

            if (e.RowIndex >= 0)
            {
                ((DataGridView)sender).EndEdit();
            }
        }
    }
}
