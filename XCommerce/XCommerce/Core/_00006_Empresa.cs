﻿using System;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Empresa;
using XCommerce.LogicaNegocio.Empresa.DTOs;

namespace XCommerce.Core
{
    public partial class _00006_Empresa : FormularioConsultaConDetalle
    {
        private readonly EmpresaServicio _empresaServicio;

        // Botones que se Creaaran por Codigo
        private readonly ToolStripButton btnAsignarEmpleados;
        private readonly ToolStripButton btnQuitarEmpleados;

        public _00006_Empresa()
            : this(new EmpresaServicio())
        {
            InitializeComponent();
            CrearBotones(btnAsignarEmpleados, "btnAsignarEmpleado");
            CrearBotones(btnQuitarEmpleados, "btnQuitarEmpleado");
        }
        
        public _00006_Empresa(EmpresaServicio empresaServicio)
            : base("Lista de Empresas")
        {
            _empresaServicio = empresaServicio;

            btnImprimir.Visible = false;
            btnAsignarEmpleados = new ToolStripButton("Asignar Empleados", Properties.Resources.Usuarios,btnAsignarEmpleados_Click);
            btnQuitarEmpleados = new ToolStripButton("Quitar Empleados", Properties.Resources.Usuarios, btnQuitarEmpleados_Click);
        }

        private void CrearBotones(ToolStripButton boton, string name)
        {
            boton.Alignment = ToolStripItemAlignment.Left;
            boton.ForeColor = System.Drawing.Color.White;
            boton.ImageTransparentColor = System.Drawing.Color.Magenta;
            boton.Name = name;
            boton.Size = new System.Drawing.Size(44, 59);
            boton.TextImageRelation = TextImageRelation.ImageAboveText;

            this.menu.Items.Add(boton);
        }


        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _empresaServicio.Obtener(cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Sucursal"].Visible = true;
            dgvGrilla.Columns["Sucursal"].Width = 120;
            dgvGrilla.Columns["Sucursal"].HeaderText = @"Sucursal";

            dgvGrilla.Columns["RazonSocial"].Visible = true;
            dgvGrilla.Columns["RazonSocial"].HeaderText = @"Razon Social";
            dgvGrilla.Columns["RazonSocial"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["CUIT"].Visible = true;
            dgvGrilla.Columns["CUIT"].Width = 120;
            dgvGrilla.Columns["CUIT"].HeaderText = @"CUIT";

            dgvGrilla.Columns["Telefono"].Visible = true;
            dgvGrilla.Columns["Telefono"].Width = 120;
            dgvGrilla.Columns["Telefono"].HeaderText = @"Teléfono";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevaEmpresa = new _00007_ABM_Empresa(TipoOperacion.Insert);
            fNuevaEmpresa.ShowDialog();
            return fNuevaEmpresa.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarEmpresa = new _00007_ABM_Empresa(TipoOperacion.Delete, entidadId);
            fEliminarEmpresa.ShowDialog();
            return fEliminarEmpresa.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarEmpresa = new _00007_ABM_Empresa(TipoOperacion.Update, entidadId);
            fModificarEmpresa.ShowDialog();
            return fModificarEmpresa.RealizoAlgunaOperacion;
        }

        public override void dgvGrilla_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            base.dgvGrilla_RowEnter(sender, e);

            var empresa = (EmpresaDto)ObjetoSeleccionado;
            if (empresa == null) return;

            imgLogoEmpresa.Image = Imagen.Convertir_Bytes_Imagen(empresa.Logo);
        }

        private void btnAsignarEmpleados_Click(object sender, EventArgs e)
        {
            var fAsingarEmpleados = new _00008_AsignarEmpleadoEmpresa(EntidadId);
            fAsingarEmpleados.ShowDialog();
        }

        private void btnQuitarEmpleados_Click(object sender, EventArgs e)
        {
            var fQuitarEmpleados = new _00009_QuitarEmpleadoEmpresa(EntidadId);
            fQuitarEmpleados.ShowDialog();
        }
    }
}
