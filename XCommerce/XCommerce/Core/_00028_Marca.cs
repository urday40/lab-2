﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Marca;

namespace XCommerce.Core
{
    public partial class _00028_Marca : FormularioConsulta
    {
        private readonly MarcaServicio _marcaServicio;

        public _00028_Marca()
            : this(new MarcaServicio())
        {
            InitializeComponent();
        }

        public _00028_Marca(MarcaServicio marcaServicio)
            : base("Lista Marcas")
        {
            _marcaServicio = marcaServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _marcaServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Marca";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadaStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadaStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadaStr"].HeaderText = @"Eliminada";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoMarca = new _00029_ABM_Marca(TipoOperacion.Insert);
            fNuevoMarca.ShowDialog();
            return fNuevoMarca.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarMarca = new _00029_ABM_Marca(TipoOperacion.Delete, entidadId);
            fEliminarMarca.ShowDialog();
            return fEliminarMarca.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarMarca = new _00029_ABM_Marca(TipoOperacion.Update, entidadId);
            fModificarMarca.ShowDialog();
            return fModificarMarca.RealizoAlgunaOperacion;
        }
    }
}
