﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Articulo.DTOs;

namespace XCommerce.Core.Controles
{
    public partial class ctrolArticulo : UserControl
    {
        public delegate void AgregarArticulo(ArticuloDto articulo);
        public event AgregarArticulo AgregarArticuloParalaVenta;

        public long Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Cantidad { get; set; }

        public string Nombre {
            set => lblNombre.Text = value;
        }

        public byte[] Foto
        {
            set => imgFoto.Image = Imagen.Convertir_Bytes_Imagen(value);
        }

        public ctrolArticulo()
        {
            InitializeComponent();
            RedondearImagen();
        }

        private void RedondearImagen()
        {
            var gp = new GraphicsPath();
            gp.AddEllipse(0, 0, this.imgFoto.Width - 3, this.imgFoto.Height - 3);
            var rg = new Region(gp);
            this.imgFoto.Region = rg;
        }

        private void ctrolArticulo_Load(object sender, System.EventArgs e)
        {

        }

        private void ctrolArticulo_Click(object sender, System.EventArgs e)
        {
            AgregarArticuloParalaVenta?.Invoke(new ArticuloDto
            {
                Id = this.Id,
                Codigo = this.Codigo,
                Descripcion = this.Descripcion,
            });
        }
    }
}
