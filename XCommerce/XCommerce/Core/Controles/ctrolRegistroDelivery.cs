﻿using System;
using System.Windows.Forms;

namespace XCommerce.Core.Controles
{
    public partial class ctrolRegistroDelivery : UserControl
    {
        private DateTime _horaInicial;

        public ctrolRegistroDelivery()
        {
            InitializeComponent();

            _horaInicial = DateTime.Now; // Hora del Sistema OP.

            // Configurar el cronometro
            cronometro.Interval = 1000;
            cronometro.Start();
        }

        private void cronometro_Tick(object sender, EventArgs e)
        {
            var resultado = DateTime.Now.Subtract(_horaInicial);
            lblTemporizador.Text = $@"{resultado.Hours.ToString().PadLeft(2,'0')}:{resultado.Minutes.ToString().PadLeft(2, '0')}:{resultado.Seconds.ToString().PadLeft(2, '0')}";
        }
    }
}
