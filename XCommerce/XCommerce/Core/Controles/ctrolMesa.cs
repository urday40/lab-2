﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Forms;
using XCommerce.AccesoDatos;
using XCommerce.Core.Controles.FormulariosVarios;
using XCommerce.LogicaNegocio.Mesa;
using Color = System.Drawing.Color;

namespace XCommerce.Core.Controles
{
    public partial class ctrolMesa : UserControl
    {
        private long _mesaId;
        private EstadoMesa _estado;

        private MesaServicio _mesaServicio;


        public long MesaId
        {
            set => _mesaId = value;
        }

        public int Numero
        {
            set => lblNumero.Text = $@"{value}";
        }

        public int NumeroMesa => int.Parse(lblNumero.Text);

        public EstadoMesa Estado
        {
            set
            {
                _estado = value;
                abrirMesaToolStripMenuItem.Visible = true;
                cerrarMesaToolStripMenuItem.Visible = true;
                cambiarDeMesaToolStripMenuItem.Visible = true;
                cancelarMesaToolStripMenuItem.Visible = true;
                unirMesaToolStripMenuItem.Visible = true;

                switch (value)
                {
                    case EstadoMesa.Abierta:
                        this.BackColor = Color.DarkGreen;
                        abrirMesaToolStripMenuItem.Visible = false;
                        reservarToolStripMenuItem.Visible = false;
                        fueraDeServicioToolStripMenuItem.Visible = false;
                        break;
                    case EstadoMesa.Cerrada:
                        this.BackColor = Color.DarkRed;
                        cerrarMesaToolStripMenuItem.Visible = false;
                        cambiarDeMesaToolStripMenuItem.Visible = false;
                        cancelarMesaToolStripMenuItem.Visible = false;
                        unirMesaToolStripMenuItem.Visible = false;
                        
                        break;
                    case EstadoMesa.Reservada:
                        this.BackColor = Color.DarkBlue;
                        cerrarMesaToolStripMenuItem.Visible = false;
                        cambiarDeMesaToolStripMenuItem.Visible = false;
                        cancelarMesaToolStripMenuItem.Visible = false;
                        unirMesaToolStripMenuItem.Visible = false;
                        reservarToolStripMenuItem.Visible = false;
                        fueraDeServicioToolStripMenuItem.Visible = false;

                        break;
                    case EstadoMesa.FueraServicio:
                        this.BackColor = Color.Black;
                       // abrirMesaToolStripMenuItem.Visible = false;
                        reservarToolStripMenuItem.Visible = false;
                        fueraDeServicioToolStripMenuItem.Visible = false;
                        cerrarMesaToolStripMenuItem.Visible = false;
                        cambiarDeMesaToolStripMenuItem.Visible = false;
                        cancelarMesaToolStripMenuItem.Visible = false;
                        unirMesaToolStripMenuItem.Visible = false;
                        break;
                }
            }
        }

        public decimal MontoConsumido
        {
            set => lblMontoConsumido.Text = $@"{value:C}";
        }


        // Constructor del Control Mesa
        public ctrolMesa()
        :this (new MesaServicio())
        {
            InitializeComponent();
        }

        public ctrolMesa(MesaServicio mesaServicio)
        {
            _mesaServicio = mesaServicio;
        }

        public void ClickAbrirMesa(object sender, EventArgs e)
        {
    
            var mesa =_mesaServicio.ObtenerPorId(_mesaId);

            mesa.EstadoMesa = EstadoMesa.Abierta;

            _mesaServicio.Modificar(mesa);

            ActualizarEstado(EstadoMesa.Abierta);

            
            AbrirPuntoVenta();
        }

        public void AbrirPuntoVenta()
        {
            if (_estado == EstadoMesa.Abierta)
            {
                var formPuntoVentaPorMesa = new _00016_PuntoVentaPorMesa(_mesaId);
                formPuntoVentaPorMesa.ActualizarTotalDelControlMesa += ActualizarTotal;
                formPuntoVentaPorMesa.ActualizarEstadoDelControlMesa += ActualizarEstado;
                formPuntoVentaPorMesa.ShowDialog();
            }
        }

        public void ActualizarTotal(decimal totalFacturado)
        {
            MontoConsumido = totalFacturado;
        }

        public void ActualizarEstado(EstadoMesa estadoMesa)
        {
            Estado = estadoMesa;
        }

        public void MesaCancelada(decimal monto)
        {
            MontoConsumido = monto;
        }
        public void lblNumero_DoubleClick(object sender, EventArgs e)
        {
            AbrirPuntoVenta();
        }

        private void cerrarMesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
                var mesa = _mesaServicio.ObtenerPorId(_mesaId);

                mesa.EstadoMesa = EstadoMesa.Cerrada;

                _mesaServicio.Modificar(mesa);
                    
            ActualizarEstado(EstadoMesa.Cerrada);

            
        }

        private void reservarToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var mesa = _mesaServicio.ObtenerPorId(_mesaId);

            mesa.EstadoMesa = EstadoMesa.Reservada;

            _mesaServicio.Modificar(mesa);

            ActualizarEstado(EstadoMesa.Reservada);

        }

        private void fueraDeServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var mesa = _mesaServicio.ObtenerPorId(_mesaId);

            mesa.EstadoMesa = EstadoMesa.FueraServicio;

            _mesaServicio.Modificar(mesa);

            ActualizarEstado(EstadoMesa.FueraServicio);

        }

        private void cancelarMesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var mesa = _mesaServicio.ObtenerPorId(_mesaId);

            mesa.EstadoMesa = EstadoMesa.Cerrada;
            mesa.Monto = 1m;

            _mesaServicio.Modificar(mesa);

            MesaCancelada(mesa.Monto);

            ActualizarEstado(EstadoMesa.Cerrada);
           
        }
    }
}
