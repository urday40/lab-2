﻿using System.Windows.Forms;

namespace XCommerce.Core.Controles
{
    public partial class RubroButton : Button
    {
        public long RubroId { get; set; }

        public RubroButton()
        {
            InitializeComponent();
        }
    }
}
