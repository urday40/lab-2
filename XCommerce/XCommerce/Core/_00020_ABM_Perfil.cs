﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.CondicionIva.DTOs;
using XCommerce.LogicaNegocio.Perfil;
using XCommerce.LogicaNegocio.Perfil.DTOs;

namespace XCommerce.Core
{
    public partial class _00020_ABM_Perfil : FormularioAbm
    {
        private readonly PerfilServicio _perfilServicio;

        public _00020_ABM_Perfil()
        {
            InitializeComponent();
        }

        public _00020_ABM_Perfil(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();

            _perfilServicio = new PerfilServicio();

            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Perfil / Grupo");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var condicionIvaServicio = _perfilServicio.ObtenerPorId(entidadId.Value);

                txtDescripcion.Text = condicionIvaServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro el Perfil", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_perfilServicio.VerificarSiExiste(txtDescripcion.Text, Identidad.EmpresaId))
                {
                    var nuevaPerfil = new PerfilDto
                    {
                        Descripcion = txtDescripcion.Text,
                        EmpresaId = Identidad.EmpresaId
                    };

                    _perfilServicio.Insertar(nuevaPerfil);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El perfil ya Existe", Mensaje.Tipo.Informacion);
                    txtDescripcion.Clear();
                    txtDescripcion.Focus();
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_perfilServicio.VerificarSiExiste(txtDescripcion.Text,Identidad.EmpresaId, EntidadId))
                {
                    var modificarPerfil = new PerfilDto
                    {
                        Id = EntidadId.Value,
                        EmpresaId = Identidad.EmpresaId,
                        Descripcion = txtDescripcion.Text,
                    };

                    _perfilServicio.Modificar(modificarPerfil);
                }
                else
                {
                    Mensaje.Mostrar("El perfil ya Existe", Mensaje.Tipo.Informacion);
                    txtDescripcion.Clear();
                    txtDescripcion.Focus();
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _perfilServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
        }
    }
}
