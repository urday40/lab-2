﻿using System;
using System.Data.Entity.Validation;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.TipoComprobante;
using XCommerce.LogicaNegocio.TipoComprobante.DTOs;

namespace XCommerce.Core
{
    public partial class _00027_ABM_TipoComprobante : FormularioAbm
    {
        private readonly TipoComprobanteServicio _tipoComprobanteServicio;

        public _00027_ABM_TipoComprobante()
        {
            InitializeComponent();
        }

        public _00027_ABM_TipoComprobante(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _tipoComprobanteServicio = new TipoComprobanteServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Tipo Comprobante");
            AgregarControlesObligatorios(txtLetra, "Letra");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            txtLetra.KeyPress += delegate(object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoNumeros(sender, args);
            };

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _tipoComprobanteServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var tipoComprobante = _tipoComprobanteServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = tipoComprobante.Codigo;
                txtDescripcion.Text = tipoComprobante.Descripcion;
                txtLetra.Text = tipoComprobante.Letra;
                
                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro el Tipo de Comprobante", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_tipoComprobanteServicio.VerificarSiExiste(Identidad.EmpresaId, (int) nudCodigo.Value, txtDescripcion.Text, txtLetra.Text))
                {
                    var nuevaTipoComprobante = new TipoComprobanteDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                        Letra = txtLetra.Text,
                        EmpresaId = Identidad.EmpresaId
                    };

                    _tipoComprobanteServicio.Insertar(nuevaTipoComprobante);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("Hay datos que ya existen.", Mensaje.Tipo.Informacion);
                }
            }
            catch (DbEntityValidationException ex)
            {
                Mensaje.Mostrar(ex, Mensaje.Tipo.Stop);
            }
            catch (Exception ex)
            {
                Mensaje.Mostrar($"Ocurrió un Error al Grabar los Datos. {ex.Message}", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_tipoComprobanteServicio.VerificarSiExiste(Identidad.EmpresaId, (int) nudCodigo.Value, txtDescripcion.Text, txtLetra.Text, EntidadId))
                {
                    var modificarTipoComprobante = new TipoComprobanteDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                        Letra = txtLetra.Text,
                        EmpresaId = Identidad.EmpresaId
                    };

                    _tipoComprobanteServicio.Modificar(modificarTipoComprobante);
                }
                else
                {
                    Mensaje.Mostrar("Hay datos que ya existen.", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _tipoComprobanteServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _tipoComprobanteServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
