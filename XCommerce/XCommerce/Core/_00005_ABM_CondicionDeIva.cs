﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.CondicionIva;
using XCommerce.LogicaNegocio.CondicionIva.DTOs;

namespace XCommerce.Core
{
    public partial class _00005_ABM_CondicionDeIva : FormularioAbm
    {
        private readonly CondicionIvaServicio _condicionIvaServicio;

        public _00005_ABM_CondicionDeIva()
        {
            InitializeComponent();
        }

        public _00005_ABM_CondicionDeIva(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _condicionIvaServicio = new CondicionIvaServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Condición de Iva");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _condicionIvaServicio.ObtenerSiguienteCodigo();
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var condicionIvaServicio = _condicionIvaServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = condicionIvaServicio.Codigo;
                txtDescripcion.Text = condicionIvaServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Condición de Iva", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var nuevaCondicionIva = new CondicionIvaDto
                {
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                };

                _condicionIvaServicio.Insertar(nuevaCondicionIva);
                LimpiarControles(this);
                txtDescripcion.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var modificarCondicionIva = new CondicionIvaDto
                {
                    Id = EntidadId.Value,
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                };

                _condicionIvaServicio.Modificar(modificarCondicionIva);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _condicionIvaServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _condicionIvaServicio.ObtenerSiguienteCodigo();
        }
    }
}
