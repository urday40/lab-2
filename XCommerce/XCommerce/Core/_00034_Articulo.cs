﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Articulo;

namespace XCommerce.Core
{
    public partial class _00034_Articulo : FormularioConsultaConDetalle
    {
        private readonly ArticuloServicio _articuloServicio;

        public _00034_Articulo()
            : this(new ArticuloServicio())
        {
            InitializeComponent();
        }

        public _00034_Articulo(ArticuloServicio articuloServicio)
            : base("Lista de Articulos")
        {
            _articuloServicio = articuloServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _articuloServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 100;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Abreviatura"].Visible = true;
            dgvGrilla.Columns["Abreviatura"].Width = 80;
            dgvGrilla.Columns["Abreviatura"].HeaderText = @"Abreviatura";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Articulo";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["TipoArticulo"].Visible = true;
            dgvGrilla.Columns["TipoArticulo"].Width = 100;
            dgvGrilla.Columns["TipoArticulo"].HeaderText = @"Tipo Articulo";

            dgvGrilla.Columns["EstaEliminadoStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadoStr"].Width = 80;
            dgvGrilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoArticulo = new _00035_ABM_Articulo(TipoOperacion.Insert);
            fNuevoArticulo.ShowDialog();
            return fNuevoArticulo.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarArticulo = new _00035_ABM_Articulo(TipoOperacion.Delete, entidadId);
            fEliminarArticulo.ShowDialog();
            return fEliminarArticulo.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarArticulo = new _00035_ABM_Articulo(TipoOperacion.Update, entidadId);
            fModificarArticulo.ShowDialog();
            return fModificarArticulo.RealizoAlgunaOperacion;
        }
    }
}
