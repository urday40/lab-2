﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.Core.Controles;
using XCommerce.LogicaNegocio.Mesa;
using XCommerce.LogicaNegocio.Sala;
using XCommerce.LogicaNegocio.Sala.DTOs;

namespace XCommerce.Core
{
    public partial class _00015_VentaSalon : FormularioBase
    {
        private readonly SalaServicio _salaServicio;
        private readonly MesaServicio _mesaServicio;

        public _00015_VentaSalon()
            : this(new SalaServicio(), new MesaServicio())
        {
            InitializeComponent();
            Inicializador();
        }

        public _00015_VentaSalon(SalaServicio salaServicio, MesaServicio mesaServicio)
        {
            _salaServicio = salaServicio;
            _mesaServicio = mesaServicio;
        }

        private void Inicializador()
        {
            var salasAsignadas = _salaServicio.Obtener(string.Empty, Identidad.EmpresaId);
            CrearSalas(salasAsignadas);
        }

        private void CrearSalas(IEnumerable<SalaDto> listaSalas)
        {
            var contenedorSalas = new TabControl();
            
            var contador = 0;
            foreach (var sala in listaSalas.ToList())
            {
                var mesas = _mesaServicio.Obtener(sala.Id);

                var flowContenedor = new FlowLayoutPanel
                {
                    Dock = DockStyle.Fill,
                    Location = new Point(3, 3),
                    Name = $"{contador}FlowSala",
                    Size = new Size(848, 351),
                    TabIndex = 0,
                };

                foreach (var mesa in mesas)
                {
                    var controlMesa = new ctrolMesa
                    {
                        MesaId = mesa.Id,
                        Numero = mesa.Codigo,
                        Estado = mesa.EstadoMesa,
                        MontoConsumido = mesa.Monto
                    };
                    
                    flowContenedor.Controls.Add(controlMesa);
                }

                var paginaSala = new TabPage
                {
                    Location = new System.Drawing.Point(4, 22),
                    Name = $"{contador}{sala.Descripcion}",
                    Padding = new Padding(3),
                    Size = new System.Drawing.Size(854, 357),
                    TabIndex = contador,
                    Text = sala.Descripcion,
                    UseVisualStyleBackColor = true
                };
                paginaSala.Controls.Add(flowContenedor);

                contenedorSalas.Controls.Add(paginaSala);
                contador++;
            }

            contenedorSalas.Dock = DockStyle.Fill;
            contenedorSalas.Location = new System.Drawing.Point(0, 66);
            contenedorSalas.Name = "tabVentaSalon";
            contenedorSalas.SelectedIndex = 0;
            contenedorSalas.Size = new System.Drawing.Size(862, 383);
            contenedorSalas.TabIndex = 9;
            contenedorSalas.Padding = new Point(20,15);

            Controls.Add(contenedorSalas);
            Controls.SetChildIndex(contenedorSalas, 0);
            contenedorSalas.ResumeLayout(false);
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void txtMesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                var objMesa = ObtenerMesa(((TextBox) sender).Text);
                objMesa.AbrirPuntoVenta();
            }
        }

        private ctrolMesa ObtenerMesa(string mesaBuscar)
        {
            int numeroMesa = -1;
            int.TryParse(mesaBuscar, out numeroMesa);

            foreach (var ctrol in this.Controls)
            {
                if (ctrol is TabControl)
                {
                    foreach (var tabControl in ((TabControl)ctrol).Controls)
                    {
                        if (tabControl is TabPage)
                        {
                            foreach (var tabPageControl in ((TabPage)tabControl).Controls)
                            {
                                if (tabPageControl is FlowLayoutPanel)
                                {
                                    foreach (var control in ((FlowLayoutPanel)tabPageControl).Controls)
                                    {
                                        if (control is ctrolMesa)
                                        {
                                            if (((ctrolMesa) control).NumeroMesa == numeroMesa)
                                            {
                                                return ((ctrolMesa) control);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var objMesa = ObtenerMesa(((TextBox)sender).Text);
            objMesa.AbrirPuntoVenta();
        }

        private void _00015_VentaSalon_Activated(object sender, EventArgs e)
        {
            txtMesa.Clear();
            txtMesa.Focus();
        }
    }
}
