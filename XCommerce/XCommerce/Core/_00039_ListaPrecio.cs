﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.ListaPrecio;

namespace XCommerce.Core
{
    public partial class _00039_ListaPrecio : FormularioConsulta
    {
        private readonly ListaPrecioServicio _listaPrecioServicio;

        public _00039_ListaPrecio()
            : this(new ListaPrecioServicio())
        {
            InitializeComponent();
        }

        public _00039_ListaPrecio(ListaPrecioServicio listaPrecioServicio)
            : base("Lista de Precios")
        {
            _listaPrecioServicio = listaPrecioServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _listaPrecioServicio.Obtener(Identidad.EmpresaId, cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"ListaPrecio";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["Rentabilidad"].Visible = true;
            dgvGrilla.Columns["Rentabilidad"].Width = 120;
            dgvGrilla.Columns["Rentabilidad"].HeaderText = @"Rentabilidad";

            dgvGrilla.Columns["EstaEliminadaStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadaStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadaStr"].HeaderText = @"Eliminado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoListaPrecio = new _00040_ABM_ListaPrecio(TipoOperacion.Insert);
            fNuevoListaPrecio.ShowDialog();
            return fNuevoListaPrecio.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarListaPrecio = new _00040_ABM_ListaPrecio(TipoOperacion.Delete, entidadId);
            fEliminarListaPrecio.ShowDialog();
            return fEliminarListaPrecio.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarListaPrecio = new _00040_ABM_ListaPrecio(TipoOperacion.Update, entidadId);
            fModificarListaPrecio.ShowDialog();
            return fModificarListaPrecio.RealizoAlgunaOperacion;
        }
    }
}
