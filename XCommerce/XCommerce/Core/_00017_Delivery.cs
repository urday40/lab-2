﻿using System;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Core.Controles;

namespace XCommerce.Core
{
    public partial class _00017_Delivery : Form
    {
        public _00017_Delivery()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000; // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        private void _00017_Delivery_Load(object sender, System.EventArgs e)
        {
            CargarPedidos();
        }

        private void CargarPedidos()
        {
            for (int i = 0; i < 10; i++)
            {
                var controlNuevo = new ctrolRegistroDelivery();
                controlNuevo.Width = (flpControles.Width/2) - 15;
                flpControles.Controls.Add(controlNuevo); 
            }
        }
    }
}
