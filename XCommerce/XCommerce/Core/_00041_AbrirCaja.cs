﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Caja;
using XCommerce.LogicaNegocio.Caja.DTOs;

namespace XCommerce.Core
{
    public partial class _00041_AbrirCaja : Form
    {
        private readonly CajaServicio _cajaServicio;

        public _00041_AbrirCaja()
        :this(new CajaServicio())
        {
            InitializeComponent();
        }

        public _00041_AbrirCaja(CajaServicio cajaServicio)
        {
            _cajaServicio = cajaServicio;
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            if (nudMontoInicial.Value == 0)
            {
                var mensaje = MessageBox.Show($"Esta seguro?", "Atencion", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (mensaje == DialogResult.OK)
                {
                    try
                    {
                        var nuevaCaja = new CajaDto
                        {
                            Diferencia = 0,
                            EmpresaId = Identidad.EmpresaId,
                            FechaApertura = DateTime.Now,
                            FechaCierre = null,
                            MontoApertura = 0,
                            MontoCierre = null,
                            MontoSistema = 0,
                            UsuarioAperturaId = Identidad.UsuarioLoginId,
                            UsuarioCierreId = null
                        };

                        _cajaServicio.Insert(nuevaCaja);
                        CajaConstante.CajaId = nuevaCaja.Id;

                    }
                    catch
                    {
                        MessageBox.Show($"Faltan Datos por llenar");
                    }
                }
                
            }

            else
            {
                try
                {
                    var nuevaCaja = new CajaDto
                    {
                        Diferencia = 0,
                        EmpresaId = Identidad.EmpresaId,
                        FechaApertura = DateTime.Now,
                        FechaCierre = null,
                        MontoApertura = nudMontoInicial.Value,
                        MontoCierre = null,
                        MontoSistema = 0,
                        UsuarioAperturaId = Identidad.UsuarioLoginId,
                        UsuarioCierreId = null
                    };

                    _cajaServicio.Insert(nuevaCaja);
                    CajaConstante.CajaId = nuevaCaja.Id;

                }
                catch
                {
                    MessageBox.Show($"Faltan Datos por llenar");
                }
            }
                
        }
    }
}
