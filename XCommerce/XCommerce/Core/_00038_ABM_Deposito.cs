﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Deposito;
using XCommerce.LogicaNegocio.Deposito.DTOs;

namespace XCommerce.Core
{
    public partial class _00038_ABM_Deposito : FormularioAbm
    {
        private readonly DepositoServicio _depositoServicio;

        public _00038_ABM_Deposito()
        {
            InitializeComponent();
        }

        public _00038_ABM_Deposito(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _depositoServicio = new DepositoServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Deposito");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _depositoServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var depositoServicio = _depositoServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = depositoServicio.Codigo;
                txtDescripcion.Text = depositoServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Deposito", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_depositoServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text))
                {
                    var nuevaDeposito = new DepositoDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                    };

                    _depositoServicio.Insertar(nuevaDeposito);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_depositoServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text, EntidadId))
                {
                    var modificarDeposito = new DepositoDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                    };

                    _depositoServicio.Modificar(modificarDeposito);
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _depositoServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _depositoServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
