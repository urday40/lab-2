﻿using System;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Usuario;

namespace XCommerce.Core
{
    public partial class _00025_CambiarPassword : FormularioBase
    {
        private readonly UsuarioServicio _usuarioServicio;

        public _00025_CambiarPassword()
            : this(new UsuarioServicio())
        {
            InitializeComponent();
        }

        public _00025_CambiarPassword(UsuarioServicio usuarioServicio)
        {
            _usuarioServicio = usuarioServicio;
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnEjecutar_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPasswordActual.Text))
                {
                    Mensaje.Mostrar("Por favor ingrese la contraseña Actual.", Mensaje.Tipo.Informacion);
                    return;
                }

                if (string.IsNullOrEmpty(txtNuevaPassword.Text))
                {
                    Mensaje.Mostrar("Por favor ingrese la nueva contraseña.", Mensaje.Tipo.Informacion);
                    return;
                }

                if (string.IsNullOrEmpty(txtRepetirPassword.Text))
                {
                    Mensaje.Mostrar("Por favor repita la nueva contraseña.", Mensaje.Tipo.Informacion);
                    return;
                }

                _usuarioServicio.CambiarPassword(Identidad.UsuarioLoginId, txtPasswordActual.Text,
                    txtNuevaPassword.Text);

                Mensaje.Mostrar("La contraseña se cambio correctamente.", Mensaje.Tipo.Informacion);
                Close();
            }
            catch (Exception exception)
            {
                Mensaje.Mostrar(exception, Mensaje.Tipo.Informacion);
                txtNuevaPassword.Clear();
                txtRepetirPassword.Clear();
                txtNuevaPassword.Focus();
            }
        }
    }
}
