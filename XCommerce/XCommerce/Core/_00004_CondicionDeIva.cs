﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.CondicionIva;

namespace XCommerce.Core
{
    public partial class _00004_CondicionDeIva : FormularioConsulta
    {
        private readonly CondicionIvaServicio _empleadoServicio;

        public _00004_CondicionDeIva()
            : this(new CondicionIvaServicio())
        {
            InitializeComponent();
        }

        public _00004_CondicionDeIva(CondicionIvaServicio empleadoServicio)
            : base("Lista de Condiciones de Iva")
        {
            _empleadoServicio = empleadoServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _empleadoServicio.Obtener(cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Condición de Iva";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstaEliminadaStr"].Visible = true;
            dgvGrilla.Columns["EstaEliminadaStr"].Width = 120;
            dgvGrilla.Columns["EstaEliminadaStr"].HeaderText = @"Eliminada";
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoCondicionIva = new _00005_ABM_CondicionDeIva(TipoOperacion.Insert);
            fNuevoCondicionIva.ShowDialog();
            return fNuevoCondicionIva.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarCondicionIva = new _00005_ABM_CondicionDeIva(TipoOperacion.Delete, entidadId);
            fEliminarCondicionIva.ShowDialog();
            return fEliminarCondicionIva.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarCondicionIva = new _00005_ABM_CondicionDeIva(TipoOperacion.Update, entidadId);
            fModificarCondicionIva.ShowDialog();
            return fModificarCondicionIva.RealizoAlgunaOperacion;
        }
    }
}
