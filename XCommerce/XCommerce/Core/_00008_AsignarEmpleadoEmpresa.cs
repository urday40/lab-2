﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Empleado.DTOs;
using XCommerce.LogicaNegocio.Empresa;
using XCommerce.LogicaNegocio.EmpresaEmpleado;
using XCommerce.LogicaNegocio.Usuario.DTOs;

namespace XCommerce.Core
{
    public partial class _00008_AsignarEmpleadoEmpresa : FormularioBase
    {
        private long _empresaId;
        private readonly EmpresaServicio _empresaServicio;
        private readonly EmpresaEmpleadoServicio _empresaEmpleadoServicio;

        public _00008_AsignarEmpleadoEmpresa(long? empresaId)
            : this(new EmpresaServicio(), new EmpresaEmpleadoServicio())
        {
            InitializeComponent();
            CargarDatosEmpresa(empresaId);

        }

     
        

        public _00008_AsignarEmpleadoEmpresa(EmpresaServicio empresaServicio, EmpresaEmpleadoServicio empresaEmpleadoServicio)
        {
            _empresaServicio = empresaServicio;
            _empresaEmpleadoServicio = empresaEmpleadoServicio;
        }

        private void CargarDatosEmpresa(long? empresaId)
        {
            if(!empresaId.HasValue) Mensaje.Mostrar("Ocurrió un Error al Obtener la Empresa", Mensaje.Tipo.Error);

            if (empresaId != null)
            {
                var empresa = _empresaServicio.ObtenerPorId(empresaId.Value);

                if (empresa == null)
                {
                    Mensaje.Mostrar("Ocurrió un Error al Obtener la Empresa", Mensaje.Tipo.Error);
                    return;
                }

                _empresaId = empresaId.Value;
                txtRazonSocial.Text = empresa.RazonSocial;
                txtNombreFantasia.Text = empresa.NombreFantasia;
                txtCuit.Text = empresa.Cuit;
                txtSucursal.Text = empresa.Sucursal.ToString().PadLeft(5, '0');
                txtCondicionIva.Text = empresa.CondicionIva;
                imgFoto.Image = Imagen.Convertir_Bytes_Imagen(empresa.Logo);
            }
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void _00008_AsignarEmpleadoEmpresa_Load(object sender, EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }

        private void ActualizarDatos(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _empresaEmpleadoServicio.ObtenerEmpleadosNoAsignados(_empresaId, cadenaBuscar);

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Item"].Visible = true;
            dgvGrilla.Columns["Item"].Width = 40;
            dgvGrilla.Columns["Item"].HeaderText = @"Item";

            dgvGrilla.Columns["Legajo"].Visible = true;
            dgvGrilla.Columns["Legajo"].Width = 120;
            dgvGrilla.Columns["Legajo"].HeaderText = @"Legajo";

            dgvGrilla.Columns["ApyNom"].Visible = true;
            dgvGrilla.Columns["ApyNom"].HeaderText = @"Apellido y Nombre";
            dgvGrilla.Columns["ApyNom"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["Dni"].Visible = true;
            dgvGrilla.Columns["Dni"].Width = 120;
            dgvGrilla.Columns["Dni"].HeaderText = @"DNI";

            dgvGrilla.Columns["Telefono"].Visible = true;
            dgvGrilla.Columns["Telefono"].Width = 120;
            dgvGrilla.Columns["Telefono"].HeaderText = @"Teléfono";

            dgvGrilla.Columns["Celular"].Visible = true;
            dgvGrilla.Columns["Celular"].Width = 120;
            dgvGrilla.Columns["Celular"].HeaderText = @"Celular";
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ActualizarDatos(txtBuscar.Text);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }

        private void btnMarcarTodo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = true;
            }
        }

        private void btnDesmarcarTodo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = false;
            }
        }

        private void dgvGrilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((DataGridView) sender).RowCount <= 0) return;

            if (e.RowIndex >= 0)
            {
                ((DataGridView) sender).EndEdit();
            }
        }

        private void btnAsignarEmpleados_Click(object sender, EventArgs e)
        {
            try
            {
                var empleados = (List<EmpleadoDto>)dgvGrilla.DataSource;

                if (empleados.Any())
                {
                    _empresaEmpleadoServicio.AsignarEmpleado(_empresaId, empleados.Where(x => x.Item).ToList());
                    ActualizarDatos(string.Empty);
                }
                else
                {
                    Mensaje.Mostrar("No hay Empleados Cargados", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Crear los Usuarios", Mensaje.Tipo.Error);
            }
        }
    }
}
