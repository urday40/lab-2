﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Formulario;
using XCommerce.LogicaNegocio.Formulario.DTOs;

namespace XCommerce.Core
{
    public partial class _00018_Formulario : FormularioBase
    {
        private readonly FormularioServicio _formularioServicio;

        public _00018_Formulario()
            : this(new FormularioServicio())
        {
            InitializeComponent();
        }

        public _00018_Formulario(FormularioServicio formularioServicio)
        {
            _formularioServicio = formularioServicio;
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnActualizar_Click(object sender, System.EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }

        private void ActualizarDatos(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _formularioServicio.Obtener(cadenaBuscar, Assembly.GetExecutingAssembly());

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgv.Columns["Codigo"].Visible = true;
            dgv.Columns["Codigo"].HeaderText = @"Código";
            dgv.Columns["Codigo"].Width = 100;

            dgv.Columns["Descripcion"].Visible = true;
            dgv.Columns["Descripcion"].HeaderText = @"Nombre";
            dgv.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Columns["Descripcion"].ReadOnly = true;

            dgv.Columns["DescripcionCompleta"].Visible = true;
            dgv.Columns["DescripcionCompleta"].HeaderText = @"Nombre Completo";
            dgv.Columns["DescripcionCompleta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Columns["DescripcionCompleta"].ReadOnly = true;

            dgv.Columns["ExisteStr"].Visible = true;
            dgv.Columns["ExisteStr"].HeaderText = @"Existe BD";
            dgv.Columns["ExisteStr"].Width = 100;
            dgv.Columns["ExisteStr"].ReadOnly = true;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ActualizarDatos(txtBuscar.Text);
        }

        private void btnCrearFormularios_Click(object sender, EventArgs e)
        {
            try
            {
                var formularios = (List<FormularioDto>) dgvGrilla.DataSource;
                _formularioServicio.Insertar(formularios.Where(x=>!x.Existe).ToList());
                ActualizarDatos(string.Empty);
            }
            catch 
            {
                Mensaje.Mostrar("Ocurrió un error al grabar los datos.", Mensaje.Tipo.Error);
                ActualizarDatos(string.Empty);
            }
        }

        private void _00018_Formulario_Load(object sender, EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }
    }
}
