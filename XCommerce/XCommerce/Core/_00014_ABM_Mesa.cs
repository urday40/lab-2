﻿using System;
using XCommerce.AccesoDatos;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Mesa;
using XCommerce.LogicaNegocio.Mesa.DTOs;
using XCommerce.LogicaNegocio.Sala;
using XCommerce.LogicaNegocio.Seguridad;

namespace XCommerce.Core
{
    public partial class _00014_ABM_Mesa : FormularioAbm
    {
        private readonly MesaServicio _mesaServicio;
        private readonly SalaServicio _salaServicio;
        private readonly SeguridadServicio _seguridadServicio;

        public _00014_ABM_Mesa()
        {
            InitializeComponent();
        }

        public _00014_ABM_Mesa(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _mesaServicio = new MesaServicio();
            _salaServicio = new SalaServicio();
            _seguridadServicio = new SeguridadServicio();

            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            PoblarComboSalas();

            AgregarControlesObligatorios(txtDescripcion, "Sala");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _mesaServicio.ObtenerSiguienteCodigo();
        }

        private void PoblarComboSalas()
        {
            cmbSala.DataSource = _salaServicio.Obtener(string.Empty, Identidad.EmpresaId);
            cmbSala.DisplayMember = "Descripcion";
            cmbSala.ValueMember = "Id";
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var MesaServicio = _mesaServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = MesaServicio.Codigo;
                txtDescripcion.Text = MesaServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Sala", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var nuevaMesa = new MesaDto
                {
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                    SalaId = (long)cmbSala.SelectedValue,
                    EstadoMesa = EstadoMesa.Cerrada
                };

                _mesaServicio.Insertar(nuevaMesa);
                LimpiarControles(this);
                txtDescripcion.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var modificarMesa = new MesaDto
                {
                    Id = EntidadId.Value,
                    Descripcion = txtDescripcion.Text,
                    Codigo = (int)nudCodigo.Value,
                    SalaId = (long)cmbSala.SelectedValue
                };

                _mesaServicio.Modificar(modificarMesa);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _mesaServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _mesaServicio.ObtenerSiguienteCodigo();
        }

        private void btnNuevaSala_Click(object sender, EventArgs e)
        {
            var fSalaNueva = new _00012_ABM_Sala(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fSalaNueva, Identidad.UsuarioLogin))
            {
                fSalaNueva.ShowDialog();
                if (fSalaNueva.RealizoAlgunaOperacion)
                {
                    PoblarComboSalas();
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }
    }
}
