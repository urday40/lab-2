﻿using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Mesa;

namespace XCommerce.Core
{
    public partial class _00013_Mesa : FormularioConsulta
    {
        private readonly MesaServicio _mesaServicio;

        public _00013_Mesa()
            : this(new MesaServicio())
        {
            InitializeComponent();
        }

        public _00013_Mesa(MesaServicio salaServicio)
            : base("Lista de Mesas")
        {
            _mesaServicio = salaServicio;
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _mesaServicio.ObtenerTodas(cadenaBuscar);
            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Descripción";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["Sala"].Visible = true;
            dgvGrilla.Columns["Sala"].HeaderText = @"Sala";
            dgvGrilla.Columns["Sala"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvGrilla.Columns["EstadoMesa"].Visible = true;
            dgvGrilla.Columns["EstadoMesa"].Width = 120;
            dgvGrilla.Columns["EstadoMesa"].HeaderText = @"Estado";
        }

        public override bool EjecutarNuevaEntidad()
        {
            if (Identidad.EmpresaId != 0)
            {
                var fNuevoMesa = new _00014_ABM_Mesa(TipoOperacion.Insert);
                fNuevoMesa.ShowDialog();
                return fNuevoMesa.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Mesa.", Mensaje.Tipo.Informacion);
                return false;
            }
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fEliminarMesa = new _00014_ABM_Mesa(TipoOperacion.Delete, entidadId);
                fEliminarMesa.ShowDialog();
                return fEliminarMesa.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Mesa.", Mensaje.Tipo.Informacion);
                return false;
            }
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            if (Identidad.EmpresaId != 0)
            {
                var fModificarMesa = new _00014_ABM_Mesa(TipoOperacion.Update, entidadId);
                fModificarMesa.ShowDialog();
                return fModificarMesa.RealizoAlgunaOperacion;
            }
            else
            {
                Mensaje.Mostrar("Debe seleccionar una Mesa.", Mensaje.Tipo.Informacion);
                return false;
            }
        }
    }
}
