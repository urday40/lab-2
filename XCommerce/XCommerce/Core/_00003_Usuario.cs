﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Usuario;
using XCommerce.LogicaNegocio.Usuario.DTOs;

namespace XCommerce.Core
{
    public partial class _00003_Usuario : FormularioBase
    {
        private readonly UsuarioServicio _usuarioServicio;

        public _00003_Usuario()
            : this(new UsuarioServicio())
        {
            InitializeComponent();
        }

        public _00003_Usuario(UsuarioServicio usuarioServicio)
        {
            _usuarioServicio = usuarioServicio;
        }

        private void _00003_Usuario_Load(object sender, System.EventArgs e)
        {
            lblUsuarioLogin.Text = $"Usuario: {Identidad.ApyNomEmpleadoLogin}";
            ActualizarDatos(string.Empty);
        }

        private void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _usuarioServicio.Obtener(cadenaBuscar);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgv.Columns["Item"].Visible = true;
            dgv.Columns["Item"].HeaderText = string.Empty;
            dgv.Columns["Item"].Width = 35;

            dgv.Columns["ApyNomEmpleado"].Visible = true;
            dgv.Columns["ApyNomEmpleado"].HeaderText = @"Apellido y Nombre";
            dgv.Columns["ApyNomEmpleado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Columns["ApyNomEmpleado"].ReadOnly = true;

            dgv.Columns["NombreUsuario"].Visible = true;
            dgv.Columns["NombreUsuario"].Width = 150;
            dgv.Columns["NombreUsuario"].HeaderText = @"Usuario";
            dgv.Columns["NombreUsuario"].ReadOnly = true;

            dgv.Columns["Bloqueado"].Visible = true;
            dgv.Columns["Bloqueado"].HeaderText = @"Bloqueado";
            dgv.Columns["Bloqueado"].Width = 70;
            dgv.Columns["Bloqueado"].ReadOnly = true;
        }

        private void dgvGrilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((DataGridView)sender).RowCount <= 0) return;

            if (e.RowIndex >= 0)
            {
                ((DataGridView)sender).EndEdit();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ActualizarDatos(txtBuscar.Text);
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)Keys.Enter == e.KeyChar)
            {
                btnBuscar.PerformClick();
            }

            if (((TextBox)sender).TextLength >= 3)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnCrearUsuarios_Click(object sender, EventArgs e)
        {
            try
            {
                var usuarios = (List<UsuarioDto>)dgvGrilla.DataSource;

                if (usuarios.Any())
                {
                    _usuarioServicio.Crear(usuarios.Where(x => x.Item && x.NombreUsuario == "NO ASIGNADO").ToList());
                    ActualizarDatos(string.Empty);
                }
                else
                {
                    Mensaje.Mostrar("No hay Empleados Cargados", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Crear los Usuarios", Mensaje.Tipo.Error);
            }
        }

        private void btnMarcarTodo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = true;
            }
        }

        private void btnDesmarcarTodo_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = false;
            }
        }

        private void btnBloquearUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                var usuarios = (List<UsuarioDto>)dgvGrilla.DataSource;

                if (usuarios.Any())
                {
                    _usuarioServicio.CambiarEstado(usuarios.Where(x => x.Item && x.NombreUsuario != "NO ASIGNADO").ToList());
                    ActualizarDatos(string.Empty);
                }
                else
                {
                    Mensaje.Mostrar("No hay Empleados Cargados", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Crear los Usuarios", Mensaje.Tipo.Error);
            }
        }

        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                var usuarios = (List<UsuarioDto>)dgvGrilla.DataSource;

                if (usuarios.Any())
                {
                    _usuarioServicio.ResetPassword(usuarios.Where(x => x.Item && x.NombreUsuario != "NO ASIGNADO").ToList());
                    ActualizarDatos(string.Empty);
                }
                else
                {
                    Mensaje.Mostrar("No hay Empleados Cargados", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Crear los Usuarios", Mensaje.Tipo.Error);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            ActualizarDatos(string.Empty);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
