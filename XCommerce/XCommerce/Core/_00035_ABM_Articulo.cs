﻿using System;
using System.Windows.Forms;
using XCommerce.AccesoDatos;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Articulo;
using XCommerce.LogicaNegocio.Articulo.DTOs;
using XCommerce.LogicaNegocio.Marca;
using XCommerce.LogicaNegocio.Rubro;
using XCommerce.LogicaNegocio.Seguridad;
using XCommerce.LogicaNegocio.SubSubRubro;

namespace XCommerce.Core
{
    public partial class _00035_ABM_Articulo : FormularioAbm
    {
        private readonly RubroServicio _rubroServicio;
        private readonly SubRubroServicio _subRubroServicio;
        private readonly ArticuloServicio _articuloServicio;
        private readonly SeguridadServicio _seguridadServicio;
        private readonly MarcaServicio _marcaServicio;

        public _00035_ABM_Articulo()
        {
            InitializeComponent();
        }

        public _00035_ABM_Articulo(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();

            // ====   Inicializacion de los Servicios  === //
            _rubroServicio = new RubroServicio();
            _subRubroServicio = new SubRubroServicio();
            _articuloServicio = new ArticuloServicio();
            _seguridadServicio = new SeguridadServicio();
            _marcaServicio = new MarcaServicio();
            // =========================================== //

            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();
            CargarComboMarca();
            CargarComboTipoArticulos();
            CargarComboRubro();
            if (cmbRubro.Items.Count > 0)
            {
                CargarComboSubRubro((long) cmbRubro.SelectedValue);
            }

            AgregarControlesObligatorios(txtCodigo, "Codigo");
            AgregarControlesObligatorios(txtDescripcion, "Descripción");
            AgregarControlesObligatorios(cmbMarca, "Marca");
            AgregarControlesObligatorios(cmbRubro, "Rubro");
            AgregarControlesObligatorios(cmbSubRubro, "Sub-Rubro");

            txtDescripcion.KeyPress += Validar.NoInyeccion;
            txtCodigo.KeyPress += Validar.NoInyeccion;
            
            txtCodigoBarra.KeyPress += delegate(object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoLetras(sender, args);
                Validar.NoSimbolos(sender, args);
            };

            txtAbreviatura.KeyPress += delegate (object sender, KeyPressEventArgs args)
            {
                Validar.NoInyeccion(sender, args);
                Validar.NoSimbolos(sender, args);
            };
        }

        private void CargarComboTipoArticulos()
        {
            cmbTipoArticulo.DataSource = Enum.GetValues(typeof(TipoArticulo));
        }

        private void CargarComboMarca()
        {
            cmbMarca.DataSource = _marcaServicio.Obtener(Identidad.EmpresaId, string.Empty);
            cmbMarca.DisplayMember = "Descripcion";
            cmbMarca.ValueMember = "Id";
        }

        private void CargarComboSubRubro(long rubroId)
        {
            cmbSubRubro.DataSource = _subRubroServicio.Obtener(rubroId);
            cmbSubRubro.DisplayMember = "Descripcion";
            cmbSubRubro.ValueMember = "Id";
        }

        private void CargarComboRubro()
        {
            cmbRubro.DataSource = _rubroServicio.Obtener(Identidad.EmpresaId, string.Empty);
            cmbRubro.DisplayMember = "Descripcion";
            cmbRubro.ValueMember = "Id";
        }

        private void cmbRubro_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbRubro.Items.Count > 0)
            {
                CargarComboSubRubro((long)cmbRubro.SelectedValue);
            }
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var articulo = _articuloServicio.ObtenerPorId(entidadId.Value);

                txtCodigo.Text = articulo.Codigo;
                txtCodigoBarra.Text = articulo.CodigoBarra;
                txtDescripcion.Text = articulo.Descripcion;
                txtAbreviatura.Text = articulo.Abreviatura;
                txtDetalle.Text = articulo.Detalle;
                cmbMarca.SelectedValue = articulo.MarcaId;
                cmbRubro.SelectedValue = articulo.RubroId;
                CargarComboSubRubro(articulo.RubroId);
                cmbSubRubro.SelectedValue = articulo.SubRubroId;
                chkActivarLimiteVenta.Checked = articulo.ActivarLimiteVenta;
                nudCantidadLimiteVenta.Value = articulo.CantidadLimiteVenta;
                cmbTipoArticulo.SelectedValue = articulo.TipoArticulo;
                nudStockMinimo.Value = articulo.StockMinimo;
                nudStockMaximo.Value = articulo.StockMaximo;
                chkDescuentaStock.Checked = articulo.DescuentaStock;
                chkDiscontinuar.Checked = articulo.EstaDiscontinuado;
                chkPermiteStockNegativo.Checked = articulo.PermiteStockNegativo;
                chkSePuedeFraccionar.Checked = articulo.SePuedeFraccionar;
                ctrolFotoArticulo.imgFoto.Image = Imagen.Convertir_Bytes_Imagen(articulo.Foto);
            }
            else
            {
                Mensaje.Mostrar("Error No se Encontro la Sala", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                var articuloNuevo = new ArticuloDto
                {
                    Codigo = txtCodigo.Text,
                    CodigoBarra = txtCodigoBarra.Text,
                    Abreviatura = txtAbreviatura.Text,
                    Descripcion = txtDescripcion.Text,
                    Detalle = txtDetalle.Text,
                    MarcaId = (long)cmbMarca.SelectedValue,
                    SubRubroId = (long)cmbSubRubro.SelectedValue,
                    ActivarLimiteVenta = chkActivarLimiteVenta.Checked,
                    CantidadLimiteVenta = nudCantidadLimiteVenta.Value,
                    DescuentaStock = chkDescuentaStock.Checked,
                    EstaDiscontinuado = chkDiscontinuar.Checked,
                    EstaEliminado = false,
                    Foto = Imagen.Convertir_Imagen_Bytes(ctrolFotoArticulo.imgFoto.Image),
                    PermiteStockNegativo = chkPermiteStockNegativo.Checked,
                    RubroId = (long)cmbRubro.SelectedValue,
                    SePuedeFraccionar = chkSePuedeFraccionar.Checked,
                    StockMaximo = nudStockMaximo.Value,
                    StockMinimo = nudStockMinimo.Value,
                    TipoArticulo = (TipoArticulo)cmbTipoArticulo.SelectedValue
                };

                _articuloServicio.Insertar(articuloNuevo, Identidad.EmpresaId);

                LimpiarControles(this);
                txtCodigo.Focus();
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                var articuloNuevo = new ArticuloDto
                {
                    Id = EntidadId.Value,
                    Codigo = txtCodigo.Text,
                    CodigoBarra = txtCodigoBarra.Text,
                    Abreviatura = txtAbreviatura.Text,
                    Descripcion = txtDescripcion.Text,
                    Detalle = txtDetalle.Text,
                    MarcaId = (long)cmbMarca.SelectedValue,
                    SubRubroId = (long)cmbSubRubro.SelectedValue,
                    ActivarLimiteVenta = chkActivarLimiteVenta.Checked,
                    CantidadLimiteVenta = nudCantidadLimiteVenta.Value,
                    DescuentaStock = chkDescuentaStock.Checked,
                    EstaDiscontinuado = chkDiscontinuar.Checked,
                    EstaEliminado = false,
                    Foto = Imagen.Convertir_Imagen_Bytes(ctrolFotoArticulo.imgFoto.Image),
                    PermiteStockNegativo = chkPermiteStockNegativo.Checked,
                    RubroId = (long)cmbRubro.SelectedValue,
                    SePuedeFraccionar = chkSePuedeFraccionar.Checked,
                    StockMaximo = nudStockMaximo.Value,
                    StockMinimo = nudStockMinimo.Value,
                    TipoArticulo = (TipoArticulo)cmbTipoArticulo.SelectedValue
                };

                _articuloServicio.Modificar(articuloNuevo);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _articuloServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
        }

        private void btnNuevaMarca_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00029_ABM_Marca(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
                if (fFormularioNuevo.RealizoAlgunaOperacion)
                {
                    CargarComboMarca();
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnNuevoRubro_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00031_ABM_Rubro(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
                if (fFormularioNuevo.RealizoAlgunaOperacion)
                {
                    CargarComboRubro();
                    if (cmbRubro.Items.Count > 0)
                    {
                        CargarComboSubRubro((long)cmbRubro.SelectedValue);
                    }
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }

        private void btnNuevoSubRubro_Click(object sender, EventArgs e)
        {
            var fFormularioNuevo = new _00033_ABM_SubRubro(TipoOperacion.Insert);
            if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fFormularioNuevo, Identidad.UsuarioLogin))
            {
                fFormularioNuevo.ShowDialog();
                if (fFormularioNuevo.RealizoAlgunaOperacion)
                {
                    CargarComboSubRubro((long)cmbRubro.SelectedValue);
                }
            }
            else
            {
                Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
            }
        }
    }
}
