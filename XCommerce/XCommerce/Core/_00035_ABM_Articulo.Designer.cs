﻿namespace XCommerce.Core
{
    partial class _00035_ABM_Articulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkSePuedeFraccionar = new System.Windows.Forms.CheckBox();
            this.chkDescuentaStock = new System.Windows.Forms.CheckBox();
            this.chkPermiteStockNegativo = new System.Windows.Forms.CheckBox();
            this.chkDiscontinuar = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.nudCantidadLimiteVenta = new System.Windows.Forms.NumericUpDown();
            this.chkActivarLimiteVenta = new System.Windows.Forms.CheckBox();
            this.btnNuevoSubRubro = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbSubRubro = new System.Windows.Forms.ComboBox();
            this.btnNuevoRubro = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbRubro = new System.Windows.Forms.ComboBox();
            this.btnNuevaMarca = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbMarca = new System.Windows.Forms.ComboBox();
            this.txtDetalle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAbreviatura = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodigoBarra = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.ctrolFotoArticulo = new XCommerce.Core.Controles.ctrolFoto();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTipoArticulo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudStockMinimo = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.nudStockMaximo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidadLimiteVenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStockMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStockMaximo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsuarioLogin
            // 
            this.lblUsuarioLogin.Size = new System.Drawing.Size(1872, 18);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Beige;
            this.panel2.Controls.Add(this.chkSePuedeFraccionar);
            this.panel2.Controls.Add(this.chkDescuentaStock);
            this.panel2.Controls.Add(this.chkPermiteStockNegativo);
            this.panel2.Controls.Add(this.chkDiscontinuar);
            this.panel2.Location = new System.Drawing.Point(97, 411);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(368, 70);
            this.panel2.TabIndex = 64;
            // 
            // chkSePuedeFraccionar
            // 
            this.chkSePuedeFraccionar.AutoSize = true;
            this.chkSePuedeFraccionar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSePuedeFraccionar.Location = new System.Drawing.Point(189, 38);
            this.chkSePuedeFraccionar.Name = "chkSePuedeFraccionar";
            this.chkSePuedeFraccionar.Size = new System.Drawing.Size(140, 19);
            this.chkSePuedeFraccionar.TabIndex = 20;
            this.chkSePuedeFraccionar.Text = "Se puede Fraccionar";
            this.chkSePuedeFraccionar.UseVisualStyleBackColor = true;
            // 
            // chkDescuentaStock
            // 
            this.chkDescuentaStock.AutoSize = true;
            this.chkDescuentaStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDescuentaStock.Location = new System.Drawing.Point(15, 13);
            this.chkDescuentaStock.Name = "chkDescuentaStock";
            this.chkDescuentaStock.Size = new System.Drawing.Size(118, 19);
            this.chkDescuentaStock.TabIndex = 19;
            this.chkDescuentaStock.Text = "Descuenta Stock";
            this.chkDescuentaStock.UseVisualStyleBackColor = true;
            // 
            // chkPermiteStockNegativo
            // 
            this.chkPermiteStockNegativo.AutoSize = true;
            this.chkPermiteStockNegativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPermiteStockNegativo.Location = new System.Drawing.Point(189, 13);
            this.chkPermiteStockNegativo.Name = "chkPermiteStockNegativo";
            this.chkPermiteStockNegativo.Size = new System.Drawing.Size(153, 19);
            this.chkPermiteStockNegativo.TabIndex = 18;
            this.chkPermiteStockNegativo.Text = "Permite Stock Negativo";
            this.chkPermiteStockNegativo.UseVisualStyleBackColor = true;
            // 
            // chkDiscontinuar
            // 
            this.chkDiscontinuar.AutoSize = true;
            this.chkDiscontinuar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiscontinuar.Location = new System.Drawing.Point(15, 38);
            this.chkDiscontinuar.Name = "chkDiscontinuar";
            this.chkDiscontinuar.Size = new System.Drawing.Size(168, 19);
            this.chkDiscontinuar.TabIndex = 17;
            this.chkDiscontinuar.Text = "Discontinuar para la venta";
            this.chkDiscontinuar.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.nudCantidadLimiteVenta);
            this.groupBox3.Controls.Add(this.chkActivarLimiteVenta);
            this.groupBox3.Location = new System.Drawing.Point(97, 297);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(368, 55);
            this.groupBox3.TabIndex = 61;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "[ Limite de Venta ]";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(214, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Cantidad";
            // 
            // nudCantidadLimiteVenta
            // 
            this.nudCantidadLimiteVenta.DecimalPlaces = 2;
            this.nudCantidadLimiteVenta.Location = new System.Drawing.Point(269, 23);
            this.nudCantidadLimiteVenta.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudCantidadLimiteVenta.Name = "nudCantidadLimiteVenta";
            this.nudCantidadLimiteVenta.Size = new System.Drawing.Size(78, 20);
            this.nudCantidadLimiteVenta.TabIndex = 1;
            // 
            // chkActivarLimiteVenta
            // 
            this.chkActivarLimiteVenta.AutoSize = true;
            this.chkActivarLimiteVenta.Location = new System.Drawing.Point(15, 25);
            this.chkActivarLimiteVenta.Name = "chkActivarLimiteVenta";
            this.chkActivarLimiteVenta.Size = new System.Drawing.Size(135, 17);
            this.chkActivarLimiteVenta.TabIndex = 0;
            this.chkActivarLimiteVenta.Text = "Activar Limite de Venta";
            this.chkActivarLimiteVenta.UseVisualStyleBackColor = true;
            // 
            // btnNuevoSubRubro
            // 
            this.btnNuevoSubRubro.Location = new System.Drawing.Point(436, 268);
            this.btnNuevoSubRubro.Name = "btnNuevoSubRubro";
            this.btnNuevoSubRubro.Size = new System.Drawing.Size(29, 23);
            this.btnNuevoSubRubro.TabIndex = 68;
            this.btnNuevoSubRubro.Text = "...";
            this.btnNuevoSubRubro.UseVisualStyleBackColor = true;
            this.btnNuevoSubRubro.Click += new System.EventHandler(this.btnNuevoSubRubro_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 272);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Sub-Rubro";
            // 
            // cmbSubRubro
            // 
            this.cmbSubRubro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubRubro.FormattingEnabled = true;
            this.cmbSubRubro.Location = new System.Drawing.Point(97, 269);
            this.cmbSubRubro.Name = "cmbSubRubro";
            this.cmbSubRubro.Size = new System.Drawing.Size(333, 21);
            this.cmbSubRubro.TabIndex = 53;
            // 
            // btnNuevoRubro
            // 
            this.btnNuevoRubro.Location = new System.Drawing.Point(436, 241);
            this.btnNuevoRubro.Name = "btnNuevoRubro";
            this.btnNuevoRubro.Size = new System.Drawing.Size(29, 23);
            this.btnNuevoRubro.TabIndex = 66;
            this.btnNuevoRubro.Text = "...";
            this.btnNuevoRubro.UseVisualStyleBackColor = true;
            this.btnNuevoRubro.Click += new System.EventHandler(this.btnNuevoRubro_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 65;
            this.label6.Text = "Rubro";
            // 
            // cmbRubro
            // 
            this.cmbRubro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRubro.FormattingEnabled = true;
            this.cmbRubro.Location = new System.Drawing.Point(97, 242);
            this.cmbRubro.Name = "cmbRubro";
            this.cmbRubro.Size = new System.Drawing.Size(333, 21);
            this.cmbRubro.TabIndex = 52;
            this.cmbRubro.SelectionChangeCommitted += new System.EventHandler(this.cmbRubro_SelectionChangeCommitted);
            // 
            // btnNuevaMarca
            // 
            this.btnNuevaMarca.Location = new System.Drawing.Point(436, 214);
            this.btnNuevaMarca.Name = "btnNuevaMarca";
            this.btnNuevaMarca.Size = new System.Drawing.Size(29, 23);
            this.btnNuevaMarca.TabIndex = 63;
            this.btnNuevaMarca.Text = "...";
            this.btnNuevaMarca.UseVisualStyleBackColor = true;
            this.btnNuevaMarca.Click += new System.EventHandler(this.btnNuevaMarca_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 62;
            this.label5.Text = "Marca";
            // 
            // cmbMarca
            // 
            this.cmbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMarca.FormattingEnabled = true;
            this.cmbMarca.Location = new System.Drawing.Point(97, 215);
            this.cmbMarca.Name = "cmbMarca";
            this.cmbMarca.Size = new System.Drawing.Size(333, 21);
            this.cmbMarca.TabIndex = 50;
            // 
            // txtDetalle
            // 
            this.txtDetalle.Location = new System.Drawing.Point(97, 163);
            this.txtDetalle.Multiline = true;
            this.txtDetalle.Name = "txtDetalle";
            this.txtDetalle.Size = new System.Drawing.Size(368, 46);
            this.txtDetalle.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Detalle";
            // 
            // txtAbreviatura
            // 
            this.txtAbreviatura.Location = new System.Drawing.Point(97, 137);
            this.txtAbreviatura.Name = "txtAbreviatura";
            this.txtAbreviatura.Size = new System.Drawing.Size(129, 20);
            this.txtAbreviatura.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Abreviatura";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(97, 111);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(368, 20);
            this.txtDescripcion.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "Descripcion";
            // 
            // txtCodigoBarra
            // 
            this.txtCodigoBarra.Location = new System.Drawing.Point(336, 85);
            this.txtCodigoBarra.Name = "txtCodigoBarra";
            this.txtCodigoBarra.Size = new System.Drawing.Size(129, 20);
            this.txtCodigoBarra.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(249, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Código de Barra";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(97, 85);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(129, 20);
            this.txtCodigo.TabIndex = 43;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(48, 88);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(40, 13);
            this.lblCodigo.TabIndex = 45;
            this.lblCodigo.Text = "Código";
            // 
            // ctrolFotoArticulo
            // 
            this.ctrolFotoArticulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.ctrolFotoArticulo.Location = new System.Drawing.Point(483, 85);
            this.ctrolFotoArticulo.Name = "ctrolFotoArticulo";
            this.ctrolFotoArticulo.Size = new System.Drawing.Size(230, 277);
            this.ctrolFotoArticulo.TabIndex = 69;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 361);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 71;
            this.label8.Text = "Tipo Articulo";
            // 
            // cmbTipoArticulo
            // 
            this.cmbTipoArticulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoArticulo.FormattingEnabled = true;
            this.cmbTipoArticulo.Location = new System.Drawing.Point(97, 358);
            this.cmbTipoArticulo.Name = "cmbTipoArticulo";
            this.cmbTipoArticulo.Size = new System.Drawing.Size(368, 21);
            this.cmbTipoArticulo.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 389);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 73;
            this.label9.Text = "Stock Mínimo";
            // 
            // nudStockMinimo
            // 
            this.nudStockMinimo.DecimalPlaces = 2;
            this.nudStockMinimo.Location = new System.Drawing.Point(97, 385);
            this.nudStockMinimo.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudStockMinimo.Name = "nudStockMinimo";
            this.nudStockMinimo.Size = new System.Drawing.Size(129, 20);
            this.nudStockMinimo.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(256, 389);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 75;
            this.label10.Text = "Stock Máximo";
            // 
            // nudStockMaximo
            // 
            this.nudStockMaximo.DecimalPlaces = 2;
            this.nudStockMaximo.Location = new System.Drawing.Point(336, 385);
            this.nudStockMaximo.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudStockMaximo.Name = "nudStockMaximo";
            this.nudStockMaximo.Size = new System.Drawing.Size(129, 20);
            this.nudStockMaximo.TabIndex = 74;
            // 
            // _00035_ABM_Articulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 538);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.nudStockMaximo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudStockMinimo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbTipoArticulo);
            this.Controls.Add(this.ctrolFotoArticulo);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnNuevoSubRubro);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbSubRubro);
            this.Controls.Add(this.btnNuevoRubro);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbRubro);
            this.Controls.Add(this.btnNuevaMarca);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbMarca);
            this.Controls.Add(this.txtDetalle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAbreviatura);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCodigoBarra);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.lblCodigo);
            this.MaximumSize = new System.Drawing.Size(751, 577);
            this.MinimumSize = new System.Drawing.Size(751, 577);
            this.Name = "_00035_ABM_Articulo";
            this.Text = "Artículo";
            this.Controls.SetChildIndex(this.lblCodigo, 0);
            this.Controls.SetChildIndex(this.txtCodigo, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtCodigoBarra, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtDescripcion, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtAbreviatura, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtDetalle, 0);
            this.Controls.SetChildIndex(this.cmbMarca, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.btnNuevaMarca, 0);
            this.Controls.SetChildIndex(this.cmbRubro, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.btnNuevoRubro, 0);
            this.Controls.SetChildIndex(this.cmbSubRubro, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.btnNuevoSubRubro, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.ctrolFotoArticulo, 0);
            this.Controls.SetChildIndex(this.cmbTipoArticulo, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.nudStockMinimo, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.nudStockMaximo, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCantidadLimiteVenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStockMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStockMaximo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkPermiteStockNegativo;
        private System.Windows.Forms.CheckBox chkDiscontinuar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudCantidadLimiteVenta;
        private System.Windows.Forms.CheckBox chkActivarLimiteVenta;
        private System.Windows.Forms.Button btnNuevoSubRubro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbSubRubro;
        private System.Windows.Forms.Button btnNuevoRubro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbRubro;
        private System.Windows.Forms.Button btnNuevaMarca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbMarca;
        private System.Windows.Forms.TextBox txtDetalle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAbreviatura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodigoBarra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblCodigo;
        private Controles.ctrolFoto ctrolFotoArticulo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTipoArticulo;
        private System.Windows.Forms.CheckBox chkSePuedeFraccionar;
        private System.Windows.Forms.CheckBox chkDescuentaStock;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudStockMinimo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudStockMaximo;
    }
}