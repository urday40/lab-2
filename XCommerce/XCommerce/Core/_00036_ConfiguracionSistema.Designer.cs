﻿namespace XCommerce.Core
{
    partial class _00036_ConfiguracionSistema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBorde = new System.Windows.Forms.Panel();
            this.menu = new System.Windows.Forms.ToolStrip();
            this.btnEjecutar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnNuevoTipoComprobante = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTipoComprobante = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPuestoCajaSeparado = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnNuevaListaDePrecios = new System.Windows.Forms.Button();
            this.cmbListaPrecioPorDefecto = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnNuevoDepositoCompra = new System.Windows.Forms.Button();
            this.cmbDepositoCompraPorDefecto = new System.Windows.Forms.ComboBox();
            this.chkAsignarProductoTodosDepositos = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNuevoDeposito = new System.Windows.Forms.Button();
            this.cmbDepositoPorDefecto = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudMontoCubierto = new System.Windows.Forms.NumericUpDown();
            this.chkCobrarCubierto = new System.Windows.Forms.CheckBox();
            this.menu.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMontoCubierto)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsuarioLogin
            // 
            this.lblUsuarioLogin.Size = new System.Drawing.Size(929, 18);
            // 
            // pnlBorde
            // 
            this.pnlBorde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pnlBorde.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBorde.Location = new System.Drawing.Point(0, 62);
            this.pnlBorde.Name = "pnlBorde";
            this.pnlBorde.Size = new System.Drawing.Size(573, 4);
            this.pnlBorde.TabIndex = 8;
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.SteelBlue;
            this.menu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.menu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnEjecutar,
            this.btnSalir});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Margin = new System.Windows.Forms.Padding(5);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(573, 62);
            this.menu.TabIndex = 7;
            this.menu.Text = "toolStrip1";
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.ForeColor = System.Drawing.Color.White;
            this.btnEjecutar.Image = global::XCommerce.Properties.Resources.Guardar;
            this.btnEjecutar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(53, 59);
            this.btnEjecutar.Text = "Guardar";
            this.btnEjecutar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSalir.ForeColor = System.Drawing.Color.White;
            this.btnSalir.Image = global::XCommerce.Properties.Resources.Salir;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(44, 59);
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(10, 10);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 376);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 36);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(565, 336);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Configuración General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnNuevoTipoComprobante);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.cmbTipoComprobante);
            this.groupBox5.Location = new System.Drawing.Point(9, 206);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(547, 59);
            this.groupBox5.TabIndex = 38;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "[ Lista Tipo de Comprobantes ]";
            // 
            // btnNuevoTipoComprobante
            // 
            this.btnNuevoTipoComprobante.Location = new System.Drawing.Point(496, 24);
            this.btnNuevoTipoComprobante.Name = "btnNuevoTipoComprobante";
            this.btnNuevoTipoComprobante.Size = new System.Drawing.Size(37, 21);
            this.btnNuevoTipoComprobante.TabIndex = 3;
            this.btnNuevoTipoComprobante.Text = "...";
            this.btnNuevoTipoComprobante.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 26);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tipo de Comprobante \r\npor Defecto. ";
            // 
            // cmbTipoComprobante
            // 
            this.cmbTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoComprobante.FormattingEnabled = true;
            this.cmbTipoComprobante.Location = new System.Drawing.Point(160, 24);
            this.cmbTipoComprobante.Name = "cmbTipoComprobante";
            this.cmbTipoComprobante.Size = new System.Drawing.Size(330, 21);
            this.cmbTipoComprobante.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPuestoCajaSeparado);
            this.groupBox3.Location = new System.Drawing.Point(9, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(547, 53);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "[ Caja ]";
            // 
            // chkPuestoCajaSeparado
            // 
            this.chkPuestoCajaSeparado.AutoSize = true;
            this.chkPuestoCajaSeparado.Location = new System.Drawing.Point(17, 25);
            this.chkPuestoCajaSeparado.Name = "chkPuestoCajaSeparado";
            this.chkPuestoCajaSeparado.Size = new System.Drawing.Size(418, 17);
            this.chkPuestoCajaSeparado.TabIndex = 0;
            this.chkPuestoCajaSeparado.Text = "Los puestos de ventas y los puestos de Caja se encuentran separados fisicamente.";
            this.chkPuestoCajaSeparado.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnNuevaListaDePrecios);
            this.groupBox2.Controls.Add(this.cmbListaPrecioPorDefecto);
            this.groupBox2.Location = new System.Drawing.Point(9, 141);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(547, 59);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "[ Lista de precios ]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lista de precios por Defecto\r\npara ventas de productos";
            // 
            // btnNuevaListaDePrecios
            // 
            this.btnNuevaListaDePrecios.Location = new System.Drawing.Point(496, 24);
            this.btnNuevaListaDePrecios.Name = "btnNuevaListaDePrecios";
            this.btnNuevaListaDePrecios.Size = new System.Drawing.Size(37, 21);
            this.btnNuevaListaDePrecios.TabIndex = 1;
            this.btnNuevaListaDePrecios.Text = "...";
            this.btnNuevaListaDePrecios.UseVisualStyleBackColor = true;
            // 
            // cmbListaPrecioPorDefecto
            // 
            this.cmbListaPrecioPorDefecto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbListaPrecioPorDefecto.FormattingEnabled = true;
            this.cmbListaPrecioPorDefecto.Location = new System.Drawing.Point(160, 24);
            this.cmbListaPrecioPorDefecto.Name = "cmbListaPrecioPorDefecto";
            this.cmbListaPrecioPorDefecto.Size = new System.Drawing.Size(330, 21);
            this.cmbListaPrecioPorDefecto.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnNuevoDepositoCompra);
            this.groupBox1.Controls.Add(this.cmbDepositoCompraPorDefecto);
            this.groupBox1.Controls.Add(this.chkAsignarProductoTodosDepositos);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnNuevoDeposito);
            this.groupBox1.Controls.Add(this.cmbDepositoPorDefecto);
            this.groupBox1.Location = new System.Drawing.Point(9, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(547, 129);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Depósito ]";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 26);
            this.label5.TabIndex = 6;
            this.label5.Text = "Depósito por Defecto \r\npara Compras";
            // 
            // btnNuevoDepositoCompra
            // 
            this.btnNuevoDepositoCompra.Location = new System.Drawing.Point(496, 64);
            this.btnNuevoDepositoCompra.Name = "btnNuevoDepositoCompra";
            this.btnNuevoDepositoCompra.Size = new System.Drawing.Size(37, 21);
            this.btnNuevoDepositoCompra.TabIndex = 5;
            this.btnNuevoDepositoCompra.Text = "...";
            this.btnNuevoDepositoCompra.UseVisualStyleBackColor = true;
            // 
            // cmbDepositoCompraPorDefecto
            // 
            this.cmbDepositoCompraPorDefecto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepositoCompraPorDefecto.FormattingEnabled = true;
            this.cmbDepositoCompraPorDefecto.Location = new System.Drawing.Point(160, 64);
            this.cmbDepositoCompraPorDefecto.Name = "cmbDepositoCompraPorDefecto";
            this.cmbDepositoCompraPorDefecto.Size = new System.Drawing.Size(330, 21);
            this.cmbDepositoCompraPorDefecto.TabIndex = 4;
            // 
            // chkAsignarProductoTodosDepositos
            // 
            this.chkAsignarProductoTodosDepositos.AutoSize = true;
            this.chkAsignarProductoTodosDepositos.Location = new System.Drawing.Point(160, 104);
            this.chkAsignarProductoTodosDepositos.Name = "chkAsignarProductoTodosDepositos";
            this.chkAsignarProductoTodosDepositos.Size = new System.Drawing.Size(308, 17);
            this.chkAsignarProductoTodosDepositos.TabIndex = 3;
            this.chkAsignarProductoTodosDepositos.Text = "Al grabar un nuevo producto asignarlo a todos los depósitos";
            this.chkAsignarProductoTodosDepositos.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Depósito por Defecto\r\npara descuento de Stock";
            // 
            // btnNuevoDeposito
            // 
            this.btnNuevoDeposito.Location = new System.Drawing.Point(496, 24);
            this.btnNuevoDeposito.Name = "btnNuevoDeposito";
            this.btnNuevoDeposito.Size = new System.Drawing.Size(37, 21);
            this.btnNuevoDeposito.TabIndex = 1;
            this.btnNuevoDeposito.Text = "...";
            this.btnNuevoDeposito.UseVisualStyleBackColor = true;
            // 
            // cmbDepositoPorDefecto
            // 
            this.cmbDepositoPorDefecto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepositoPorDefecto.FormattingEnabled = true;
            this.cmbDepositoPorDefecto.Location = new System.Drawing.Point(160, 24);
            this.cmbDepositoPorDefecto.Name = "cmbDepositoPorDefecto";
            this.cmbDepositoPorDefecto.Size = new System.Drawing.Size(330, 21);
            this.cmbDepositoPorDefecto.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 36);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(565, 336);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Salón";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.nudMontoCubierto);
            this.groupBox4.Controls.Add(this.chkCobrarCubierto);
            this.groupBox4.Location = new System.Drawing.Point(9, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(434, 53);
            this.groupBox4.TabIndex = 38;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "[ Cubiertos ]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Monto del Cubierto";
            // 
            // nudMontoCubierto
            // 
            this.nudMontoCubierto.DecimalPlaces = 2;
            this.nudMontoCubierto.Enabled = false;
            this.nudMontoCubierto.Location = new System.Drawing.Point(311, 17);
            this.nudMontoCubierto.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nudMontoCubierto.Name = "nudMontoCubierto";
            this.nudMontoCubierto.Size = new System.Drawing.Size(108, 20);
            this.nudMontoCubierto.TabIndex = 1;
            this.nudMontoCubierto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkCobrarCubierto
            // 
            this.chkCobrarCubierto.AutoSize = true;
            this.chkCobrarCubierto.Location = new System.Drawing.Point(17, 19);
            this.chkCobrarCubierto.Name = "chkCobrarCubierto";
            this.chkCobrarCubierto.Size = new System.Drawing.Size(154, 17);
            this.chkCobrarCubierto.TabIndex = 0;
            this.chkCobrarCubierto.Text = "Se deberá cobrar cubiertos";
            this.chkCobrarCubierto.UseVisualStyleBackColor = true;
            this.chkCobrarCubierto.CheckedChanged += new System.EventHandler(this.chkCobrarCubierto_CheckedChanged);
            // 
            // _00036_ConfiguracionSistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 470);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pnlBorde);
            this.Controls.Add(this.menu);
            this.MaximumSize = new System.Drawing.Size(589, 509);
            this.MinimumSize = new System.Drawing.Size(589, 509);
            this.Name = "_00036_ConfiguracionSistema";
            this.Text = "Configuración del Sistema";
            this.Load += new System.EventHandler(this._00036_ConfiguracionSistema_Load);
            this.Controls.SetChildIndex(this.menu, 0);
            this.Controls.SetChildIndex(this.pnlBorde, 0);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMontoCubierto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBorde;
        public System.Windows.Forms.ToolStrip menu;
        private System.Windows.Forms.ToolStripButton btnEjecutar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTipoComprobante;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkPuestoCajaSeparado;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNuevaListaDePrecios;
        private System.Windows.Forms.ComboBox cmbListaPrecioPorDefecto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkAsignarProductoTodosDepositos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnNuevoDeposito;
        private System.Windows.Forms.ComboBox cmbDepositoPorDefecto;
        private System.Windows.Forms.Button btnNuevoTipoComprobante;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudMontoCubierto;
        private System.Windows.Forms.CheckBox chkCobrarCubierto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnNuevoDepositoCompra;
        private System.Windows.Forms.ComboBox cmbDepositoCompraPorDefecto;
    }
}