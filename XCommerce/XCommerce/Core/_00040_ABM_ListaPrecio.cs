﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.ListaPrecio;
using XCommerce.LogicaNegocio.ListaPrecio.DTOs;

namespace XCommerce.Core
{
    public partial class _00040_ABM_ListaPrecio : FormularioAbm
    {
        private readonly ListaPrecioServicio _listaPrecioServicio;

        public _00040_ABM_ListaPrecio()
        {
            InitializeComponent();
        }

        public _00040_ABM_ListaPrecio(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _listaPrecioServicio = new ListaPrecioServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Lista de Precio");
            AgregarControlesObligatorios(nudCodigo, "Código");
            AgregarControlesObligatorios(nudRentabilidad, "Rentabilidad");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _listaPrecioServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var listaPrecio = _listaPrecioServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = listaPrecio.Codigo;
                txtDescripcion.Text = listaPrecio.Descripcion;
                nudRentabilidad.Value = listaPrecio.Rentabilidad;


                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Lista de Precio", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_listaPrecioServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text))
                {
                    var nuevaListaPrecio = new ListaPrecioDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                        Rentabilidad = nudRentabilidad.Value
                    };

                    _listaPrecioServicio.Insertar(nuevaListaPrecio);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_listaPrecioServicio.VerificarSiExiste(Identidad.EmpresaId, (int)nudCodigo.Value, txtDescripcion.Text, EntidadId))
                {
                    var modificarListaPrecio = new ListaPrecioDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int)nudCodigo.Value,
                        Rentabilidad = nudRentabilidad.Value
                    };

                    _listaPrecioServicio.Modificar(modificarListaPrecio);
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _listaPrecioServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _listaPrecioServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
