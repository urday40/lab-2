﻿using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Marca;
using XCommerce.LogicaNegocio.Marca.DTOs;

namespace XCommerce.Core
{
    public partial class _00029_ABM_Marca : FormularioAbm
    {
        private readonly MarcaServicio _marcaServicio;

        public _00029_ABM_Marca()
        {
            InitializeComponent();
        }

        public _00029_ABM_Marca(TipoOperacion operacion, long? entidadId = null)
            : base(operacion, entidadId)
        {
            InitializeComponent();
            _marcaServicio = new MarcaServicio();
            Inicializador();
        }

        public override void Inicializador()
        {
            base.Inicializador();

            AgregarControlesObligatorios(txtDescripcion, "Marca");
            AgregarControlesObligatorios(nudCodigo, "Código");

            // Validacion de Datos
            txtDescripcion.KeyPress += Validar.NoInyeccion;

            if (Operacion == TipoOperacion.Insert)
                nudCodigo.Value = _marcaServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }

        public override void CargarDatos(long? entidadId)
        {
            if (entidadId.HasValue)
            {
                var marcaServicio = _marcaServicio.ObtenerPorId(entidadId.Value);

                nudCodigo.Value = marcaServicio.Codigo;
                txtDescripcion.Text = marcaServicio.Descripcion;

                if (Operacion == TipoOperacion.Delete)
                {
                    ActivarControles(this, false);
                }
                else
                {
                    nudCodigo.Enabled = false;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                Mensaje.Mostrar("Error se encontro la Marca", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoInsert()
        {
            try
            {
                if (!_marcaServicio.VerificarSiExiste(Identidad.EmpresaId, (int) nudCodigo.Value, txtDescripcion.Text))
                {
                    var nuevaMarca = new MarcaDto
                    {
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                        EmpresaId = Identidad.EmpresaId,
                        EstaEliminada = false
                    };

                    _marcaServicio.Insertar(nuevaMarca);
                    LimpiarControles(this);
                    txtDescripcion.Focus();
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Grabar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoUpdate()
        {
            try
            {
                if (!_marcaServicio.VerificarSiExiste(Identidad.EmpresaId, (int) nudCodigo.Value, txtDescripcion.Text, EntidadId))
                {
                    var modificarMarca = new MarcaDto
                    {
                        Id = EntidadId.Value,
                        Descripcion = txtDescripcion.Text,
                        Codigo = (int) nudCodigo.Value,
                    };

                    _marcaServicio.Modificar(modificarMarca);
                }
                else
                {
                    Mensaje.Mostrar("El código o la descripición ya Existen", Mensaje.Tipo.Informacion);
                }
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Modificar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void EjecutarComandoDelete()
        {
            try
            {
                _marcaServicio.Eliminar(EntidadId.Value);
            }
            catch
            {
                Mensaje.Mostrar("Ocurrió un Error al Eliminar los Datos.", Mensaje.Tipo.Stop);
            }
        }

        public override void LimpiarControles()
        {
            base.LimpiarControles(this);
            nudCodigo.Value = _marcaServicio.ObtenerSiguienteCodigo(Identidad.EmpresaId);
        }
    }
}
