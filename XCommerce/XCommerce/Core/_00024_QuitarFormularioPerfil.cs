﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.FormularioPerfil;
using XCommerce.LogicaNegocio.FormularioPerfil.DTOs;
using XCommerce.LogicaNegocio.Perfil;

namespace XCommerce.Core
{
    public partial class _00024_QuitarFormularioPerfil : FormularioBase
    {
        private long _perfilId;
        private readonly PerfilServicio _perfilServicio;
        private readonly FormularioPerfilServicio _formularioPerfilServicio;

        public _00024_QuitarFormularioPerfil()
        {
            InitializeComponent();
        }

        public _00024_QuitarFormularioPerfil(long perfilId)
            : this(new PerfilServicio(), new FormularioPerfilServicio())
        {
            _perfilId = perfilId;
        }

        public _00024_QuitarFormularioPerfil(PerfilServicio perfilServicio, FormularioPerfilServicio usuarioPerfilServicio)
            : this()
        {
            _perfilServicio = perfilServicio;
            _formularioPerfilServicio = usuarioPerfilServicio;
        }

        private void _00024_AsignarFormularioPerfil_Load(object sender, System.EventArgs e)
        {
            CargarDatoPerfil();
            CargarDatos(string.Empty);
        }

        private void CargarDatoPerfil()
        {
            var perfil = _perfilServicio.ObtenerPorId(_perfilId);
            txtPerfil.Text = perfil.Descripcion;
        }

        private void CargarDatos(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _formularioPerfilServicio
                .ObtenerFormulariosAsignados(cadenaBuscar, _perfilId);

            FormatearGrilla(dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Item"].Visible = true;
            dgvGrilla.Columns["Item"].Width = 40;
            dgvGrilla.Columns["Item"].HeaderText = @"Item";

            dgvGrilla.Columns["Codigo"].Visible = true;
            dgvGrilla.Columns["Codigo"].Width = 120;
            dgvGrilla.Columns["Codigo"].HeaderText = @"Código";

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Nombre Formulario";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }

        private void btnQuitarFormularios_Click(object sender, EventArgs e)
        {
            try
            {
                var formulariosSeleccionados = (List<FormularioPerfilDto>)dgvGrilla.DataSource;
                _formularioPerfilServicio
                    .QuitarFormularios(_perfilId, formulariosSeleccionados.Where(x => x.Item).ToList());
                CargarDatos(string.Empty);
            }
            catch (Exception exception)
            {
                Mensaje.Mostrar(exception, Mensaje.Tipo.Error);
                CargarDatos(string.Empty);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDatos(txtBuscar.Text);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarDatos(string.Empty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGrilla.RowCount; i++)
            {
                dgvGrilla["Item", i].Value = false;
            }
        }
        private void dgvGrilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((DataGridView)sender).RowCount <= 0) return;

            if (e.RowIndex >= 0)
            {
                ((DataGridView)sender).EndEdit();
            }
        }
    }
}
