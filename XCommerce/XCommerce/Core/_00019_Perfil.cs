﻿using System;
using System.Drawing;
using System.Windows.Forms;
using XCommerce.Base.Formularios;
using XCommerce.Base.Helpers;
using XCommerce.LogicaNegocio.Perfil;
using XCommerce.LogicaNegocio.Seguridad;

namespace XCommerce.Core
{
    public partial class _00019_Perfil : FormularioConsulta
    {
        private readonly PerfilServicio _perfilServicio;

        private readonly SeguridadServicio _seguridadServicio;

        // Botones que se Creaaran por Codigo
        private System.Windows.Forms.ToolStripButton btnAsignarUsuario;
        private System.Windows.Forms.ToolStripButton btnQuitarUsuario;
        private System.Windows.Forms.ToolStripButton btnAsignarFormulario;
        private System.Windows.Forms.ToolStripButton btnQuitarFormulario;

        public _00019_Perfil()
            : this(new PerfilServicio(), new SeguridadServicio())
        {
            InitializeComponent();

            CrearBotones(btnAsignarUsuario, "btnAsignarUsuario");
            CrearBotones(btnQuitarUsuario, "btnQuitarUsuario");

            CrearBotones(btnAsignarFormulario, "btnAsignarFormulario");
            CrearBotones(btnQuitarFormulario, "btnQuitarFormulario");
        }

        public _00019_Perfil(PerfilServicio perfilServicio, SeguridadServicio seguridadServicio)
            : base("Lista de Perfiles / Grupos")
        {
            _perfilServicio = perfilServicio;
            _seguridadServicio = seguridadServicio;

            btnAsignarUsuario = new ToolStripButton("Asignar Usuarios", Properties.Resources.Usuarios, btnAsignarUsuario_Click);
            btnQuitarUsuario = new ToolStripButton("Quitar Usuarios", Properties.Resources.Usuarios, btnQuitarUsuario_Click);
            btnAsignarFormulario = new ToolStripButton("Asignar Formularios", Properties.Resources.Formularios, btnAsignarFormulario_Click);
            btnQuitarFormulario = new ToolStripButton("Quitar Formularios", Properties.Resources.Formularios, btnQuitarFormulario_Click);
        }

        private void CrearBotones(ToolStripButton boton, string name)
        {
            boton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Left;
            boton.ForeColor = System.Drawing.Color.White;
            boton.ImageTransparentColor = System.Drawing.Color.Magenta;
            boton.Name = name;
            boton.Size = new System.Drawing.Size(44, 59);
            boton.TextImageRelation = TextImageRelation.ImageAboveText;
            boton.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            boton.TextImageRelation = TextImageRelation.ImageAboveText;
            
            this.menu.Items.Add(boton);
        }

        public override void ActualizarDatos(string cadenaBuscar)
        {
            this.dgvGrilla.DataSource = _perfilServicio.Obtener(cadenaBuscar, Identidad.EmpresaId);
            FormatearGrilla(this.dgvGrilla);
        }

        public override void FormatearGrilla(DataGridView dgv)
        {
            base.FormatearGrilla(dgv);

            dgvGrilla.Columns["Descripcion"].Visible = true;
            dgvGrilla.Columns["Descripcion"].HeaderText = @"Perfil / Grupo";
            dgvGrilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        public override bool EjecutarNuevaEntidad()
        {
            var fNuevoPerfil = new _00020_ABM_Perfil(TipoOperacion.Insert);
            fNuevoPerfil.ShowDialog();
            return fNuevoPerfil.RealizoAlgunaOperacion;
        }

        public override bool EjecutarEliminarEntidad(long? entidadId)
        {
            var fEliminarPerfil = new _00020_ABM_Perfil(TipoOperacion.Delete, entidadId);
            fEliminarPerfil.ShowDialog();
            return fEliminarPerfil.RealizoAlgunaOperacion;
        }

        public override bool EjecutarModificarEntidad(long? entidadId)
        {
            var fModificarPerfil = new _00020_ABM_Perfil(TipoOperacion.Update, entidadId);
            fModificarPerfil.ShowDialog();
            return fModificarPerfil.RealizoAlgunaOperacion;
        }

        public void btnAsignarUsuario_Click(object sender, EventArgs e)
        {
            if (_entidadId.HasValue)
            {
                var fAsignarUsuario = new _00021_AsignarUsuarioPerfil(_entidadId.Value);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fAsignarUsuario, Identidad.UsuarioLogin))
                {
                    fAsignarUsuario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Por favor seleccione un Perfil", Mensaje.Tipo.Informacion);
            }
        }

        public void btnQuitarUsuario_Click(object sender, EventArgs e)
        {
            if (_entidadId.HasValue)
            {
                var fQuitarUsuario = new _00022_QuitarUsuarioPerfil(_entidadId.Value);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fQuitarUsuario, Identidad.UsuarioLogin))
                {
                    fQuitarUsuario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Por favor seleccione un Perfil", Mensaje.Tipo.Informacion);
            }
        }

        public void btnAsignarFormulario_Click(object sender, EventArgs e)
        {
            if (_entidadId.HasValue)
            {
                var fAsignarFormulario = new _00023_AsignarFormularioPerfil(_entidadId.Value);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fAsignarFormulario, Identidad.UsuarioLogin))
                {
                    fAsignarFormulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Por favor seleccione un Perfil", Mensaje.Tipo.Informacion);
            }
        }

        public void btnQuitarFormulario_Click(object sender, EventArgs e)
        {
            if (_entidadId.HasValue)
            {
                var fQuitarFormulario = new _00024_QuitarFormularioPerfil(_entidadId.Value);
                if (_seguridadServicio.VerificarSiTieneAccesoAlFormulario(fQuitarFormulario, Identidad.UsuarioLogin))
                {
                    fQuitarFormulario.ShowDialog();
                }
                else
                {
                    Mensaje.Mostrar("Acceso Denegado.", Mensaje.Tipo.Informacion);
                }
            }
            else
            {
                Mensaje.Mostrar("Por favor seleccione un Perfil", Mensaje.Tipo.Informacion);
            }
        }
    }
}
