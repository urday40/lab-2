﻿namespace XCommerce
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.administracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeEmpleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoEmpleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulariosVentanasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaPerfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoPerfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.condicionDeIvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeCondicionIvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaCondiciónIvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaEmpresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaEmpresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.depositoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDepositoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoDepositoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarEmpleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitarEmpleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeSalasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaSalaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mesaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copnsultaDeMesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaMesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipoDeComprobanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaTipoComprobanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoTipoDeComprobanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.articuloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeArticuloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoArticuloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.marcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rubroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeRubroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoRubroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.subRibroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeSubRubroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoSubRubroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.listaDePreciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaListaPrecioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaListaPrecioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlBusqueda = new System.Windows.Forms.Panel();
            this.ctrolEmpleado = new XCommerce.Core.Controles.ctrolEmpleadoLogin();
            this.pnlBorde = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.menuAccesoRapido = new System.Windows.Forms.ToolStrip();
            this.btnVentaSalon = new System.Windows.Forms.ToolStripButton();
            this.btnDelivery = new System.Windows.Forms.ToolStripButton();
            this.pnlDatosEmpresa = new System.Windows.Forms.Panel();
            this.imgEmpresa = new System.Windows.Forms.PictureBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            this.pnlBusqueda.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.menuAccesoRapido.SuspendLayout();
            this.pnlDatosEmpresa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgEmpresa)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administracionToolStripMenuItem,
            this.articuloToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(932, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // administracionToolStripMenuItem
            // 
            this.administracionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.seguridadToolStripMenuItem,
            this.condicionDeIvaToolStripMenuItem,
            this.empresaToolStripMenuItem,
            this.salaToolStripMenuItem1,
            this.mesaToolStripMenuItem1,
            this.tipoDeComprobanteToolStripMenuItem,
            this.toolStripMenuItem6,
            this.configuracionToolStripMenuItem});
            this.administracionToolStripMenuItem.Name = "administracionToolStripMenuItem";
            this.administracionToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.administracionToolStripMenuItem.Text = "Administracion";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeEmpleadosToolStripMenuItem,
            this.nuevoEmpleadoToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.toolStripMenuItem1.Text = "Empleado";
            // 
            // consultaDeEmpleadosToolStripMenuItem
            // 
            this.consultaDeEmpleadosToolStripMenuItem.Name = "consultaDeEmpleadosToolStripMenuItem";
            this.consultaDeEmpleadosToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.consultaDeEmpleadosToolStripMenuItem.Text = "Consulta de Empleados";
            this.consultaDeEmpleadosToolStripMenuItem.Click += new System.EventHandler(this.consultaDeEmpleadosToolStripMenuItem_Click);
            // 
            // nuevoEmpleadoToolStripMenuItem
            // 
            this.nuevoEmpleadoToolStripMenuItem.Name = "nuevoEmpleadoToolStripMenuItem";
            this.nuevoEmpleadoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.nuevoEmpleadoToolStripMenuItem.Text = "Nuevo Empleado";
            this.nuevoEmpleadoToolStripMenuItem.Click += new System.EventHandler(this.nuevoEmpleadoToolStripMenuItem_Click);
            // 
            // seguridadToolStripMenuItem
            // 
            this.seguridadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.formulariosVentanasToolStripMenuItem,
            this.perfilToolStripMenuItem});
            this.seguridadToolStripMenuItem.Name = "seguridadToolStripMenuItem";
            this.seguridadToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.seguridadToolStripMenuItem.Text = "Seguridad";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // formulariosVentanasToolStripMenuItem
            // 
            this.formulariosVentanasToolStripMenuItem.Name = "formulariosVentanasToolStripMenuItem";
            this.formulariosVentanasToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.formulariosVentanasToolStripMenuItem.Text = "Formularios/Ventanas";
            this.formulariosVentanasToolStripMenuItem.Click += new System.EventHandler(this.formulariosVentanasToolStripMenuItem_Click);
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaPerfilToolStripMenuItem,
            this.nuevoPerfilToolStripMenuItem});
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // consultaPerfilToolStripMenuItem
            // 
            this.consultaPerfilToolStripMenuItem.Name = "consultaPerfilToolStripMenuItem";
            this.consultaPerfilToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.consultaPerfilToolStripMenuItem.Text = "Consulta Perfil";
            this.consultaPerfilToolStripMenuItem.Click += new System.EventHandler(this.consultaPerfilToolStripMenuItem_Click);
            // 
            // nuevoPerfilToolStripMenuItem
            // 
            this.nuevoPerfilToolStripMenuItem.Name = "nuevoPerfilToolStripMenuItem";
            this.nuevoPerfilToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.nuevoPerfilToolStripMenuItem.Text = "Nuevo Perfil";
            this.nuevoPerfilToolStripMenuItem.Click += new System.EventHandler(this.nuevoPerfilToolStripMenuItem_Click);
            // 
            // condicionDeIvaToolStripMenuItem
            // 
            this.condicionDeIvaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeCondicionIvaToolStripMenuItem,
            this.nuevaCondiciónIvaToolStripMenuItem});
            this.condicionDeIvaToolStripMenuItem.Name = "condicionDeIvaToolStripMenuItem";
            this.condicionDeIvaToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.condicionDeIvaToolStripMenuItem.Text = "Condicion de Iva";
            // 
            // consultaDeCondicionIvaToolStripMenuItem
            // 
            this.consultaDeCondicionIvaToolStripMenuItem.Name = "consultaDeCondicionIvaToolStripMenuItem";
            this.consultaDeCondicionIvaToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.consultaDeCondicionIvaToolStripMenuItem.Text = "Consulta  Condición Iva";
            this.consultaDeCondicionIvaToolStripMenuItem.Click += new System.EventHandler(this.consultaDeCondicionIvaToolStripMenuItem_Click);
            // 
            // nuevaCondiciónIvaToolStripMenuItem
            // 
            this.nuevaCondiciónIvaToolStripMenuItem.Name = "nuevaCondiciónIvaToolStripMenuItem";
            this.nuevaCondiciónIvaToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.nuevaCondiciónIvaToolStripMenuItem.Text = "Nueva Condición Iva";
            this.nuevaCondiciónIvaToolStripMenuItem.Click += new System.EventHandler(this.nuevaCondiciónIvaToolStripMenuItem_Click);
            // 
            // empresaToolStripMenuItem
            // 
            this.empresaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaEmpresaToolStripMenuItem,
            this.nuevaEmpresaToolStripMenuItem,
            this.toolStripMenuItem5,
            this.depositoToolStripMenuItem,
            this.asignarEmpleadoToolStripMenuItem,
            this.quitarEmpleadoToolStripMenuItem});
            this.empresaToolStripMenuItem.Name = "empresaToolStripMenuItem";
            this.empresaToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.empresaToolStripMenuItem.Text = "Empresa";
            // 
            // consultaEmpresaToolStripMenuItem
            // 
            this.consultaEmpresaToolStripMenuItem.Name = "consultaEmpresaToolStripMenuItem";
            this.consultaEmpresaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.consultaEmpresaToolStripMenuItem.Text = "Consulta Empresa";
            this.consultaEmpresaToolStripMenuItem.Click += new System.EventHandler(this.consultaEmpresaToolStripMenuItem_Click);
            // 
            // nuevaEmpresaToolStripMenuItem
            // 
            this.nuevaEmpresaToolStripMenuItem.Name = "nuevaEmpresaToolStripMenuItem";
            this.nuevaEmpresaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.nuevaEmpresaToolStripMenuItem.Text = "Nueva Empresa";
            this.nuevaEmpresaToolStripMenuItem.Click += new System.EventHandler(this.nuevaEmpresaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(167, 6);
            // 
            // depositoToolStripMenuItem
            // 
            this.depositoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDepositoToolStripMenuItem,
            this.nuevoDepositoToolStripMenuItem});
            this.depositoToolStripMenuItem.Name = "depositoToolStripMenuItem";
            this.depositoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.depositoToolStripMenuItem.Text = "Deposito";
            // 
            // consultaDepositoToolStripMenuItem
            // 
            this.consultaDepositoToolStripMenuItem.Name = "consultaDepositoToolStripMenuItem";
            this.consultaDepositoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.consultaDepositoToolStripMenuItem.Text = "Consulta Deposito";
            this.consultaDepositoToolStripMenuItem.Click += new System.EventHandler(this.consultaDepositoToolStripMenuItem_Click);
            // 
            // nuevoDepositoToolStripMenuItem
            // 
            this.nuevoDepositoToolStripMenuItem.Name = "nuevoDepositoToolStripMenuItem";
            this.nuevoDepositoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.nuevoDepositoToolStripMenuItem.Text = "Nuevo Deposito";
            this.nuevoDepositoToolStripMenuItem.Click += new System.EventHandler(this.nuevoDepositoToolStripMenuItem_Click);
            // 
            // asignarEmpleadoToolStripMenuItem
            // 
            this.asignarEmpleadoToolStripMenuItem.Name = "asignarEmpleadoToolStripMenuItem";
            this.asignarEmpleadoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.asignarEmpleadoToolStripMenuItem.Text = "Asignar Empleado";
            this.asignarEmpleadoToolStripMenuItem.Click += new System.EventHandler(this.asignarEmpleadoToolStripMenuItem_Click);
            // 
            // quitarEmpleadoToolStripMenuItem
            // 
            this.quitarEmpleadoToolStripMenuItem.Name = "quitarEmpleadoToolStripMenuItem";
            this.quitarEmpleadoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.quitarEmpleadoToolStripMenuItem.Text = "Quitar Empleado";
            this.quitarEmpleadoToolStripMenuItem.Click += new System.EventHandler(this.quitarEmpleadoToolStripMenuItem_Click);
            // 
            // salaToolStripMenuItem1
            // 
            this.salaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeSalasToolStripMenuItem,
            this.nuevaSalaToolStripMenuItem1});
            this.salaToolStripMenuItem1.Name = "salaToolStripMenuItem1";
            this.salaToolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.salaToolStripMenuItem1.Text = "Sala";
            // 
            // consultaDeSalasToolStripMenuItem
            // 
            this.consultaDeSalasToolStripMenuItem.Name = "consultaDeSalasToolStripMenuItem";
            this.consultaDeSalasToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.consultaDeSalasToolStripMenuItem.Text = "Consulta de Salas";
            this.consultaDeSalasToolStripMenuItem.Click += new System.EventHandler(this.consultaDeSalasToolStripMenuItem_Click);
            // 
            // nuevaSalaToolStripMenuItem1
            // 
            this.nuevaSalaToolStripMenuItem1.Name = "nuevaSalaToolStripMenuItem1";
            this.nuevaSalaToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.nuevaSalaToolStripMenuItem1.Text = "Nueva Sala";
            this.nuevaSalaToolStripMenuItem1.Click += new System.EventHandler(this.nuevaSalaToolStripMenuItem1_Click);
            // 
            // mesaToolStripMenuItem1
            // 
            this.mesaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copnsultaDeMesaToolStripMenuItem,
            this.nuevaMesaToolStripMenuItem});
            this.mesaToolStripMenuItem1.Name = "mesaToolStripMenuItem1";
            this.mesaToolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.mesaToolStripMenuItem1.Text = "Mesa";
            // 
            // copnsultaDeMesaToolStripMenuItem
            // 
            this.copnsultaDeMesaToolStripMenuItem.Name = "copnsultaDeMesaToolStripMenuItem";
            this.copnsultaDeMesaToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.copnsultaDeMesaToolStripMenuItem.Text = "Consulta de Mesa";
            this.copnsultaDeMesaToolStripMenuItem.Click += new System.EventHandler(this.copnsultaDeMesaToolStripMenuItem_Click);
            // 
            // nuevaMesaToolStripMenuItem
            // 
            this.nuevaMesaToolStripMenuItem.Name = "nuevaMesaToolStripMenuItem";
            this.nuevaMesaToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.nuevaMesaToolStripMenuItem.Text = "Nueva Mesa";
            this.nuevaMesaToolStripMenuItem.Click += new System.EventHandler(this.nuevaMesaToolStripMenuItem_Click);
            // 
            // tipoDeComprobanteToolStripMenuItem
            // 
            this.tipoDeComprobanteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaTipoComprobanteToolStripMenuItem,
            this.nuevoTipoDeComprobanteToolStripMenuItem});
            this.tipoDeComprobanteToolStripMenuItem.Name = "tipoDeComprobanteToolStripMenuItem";
            this.tipoDeComprobanteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.tipoDeComprobanteToolStripMenuItem.Text = "Tipo de Comprobante";
            // 
            // consultaTipoComprobanteToolStripMenuItem
            // 
            this.consultaTipoComprobanteToolStripMenuItem.Name = "consultaTipoComprobanteToolStripMenuItem";
            this.consultaTipoComprobanteToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.consultaTipoComprobanteToolStripMenuItem.Text = "Consulta de Tipo Comprobante";
            this.consultaTipoComprobanteToolStripMenuItem.Click += new System.EventHandler(this.consultaTipoComprobanteToolStripMenuItem_Click);
            // 
            // nuevoTipoDeComprobanteToolStripMenuItem
            // 
            this.nuevoTipoDeComprobanteToolStripMenuItem.Name = "nuevoTipoDeComprobanteToolStripMenuItem";
            this.nuevoTipoDeComprobanteToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.nuevoTipoDeComprobanteToolStripMenuItem.Text = "Nuevo Tipo de Comprobante";
            this.nuevoTipoDeComprobanteToolStripMenuItem.Click += new System.EventHandler(this.nuevoTipoDeComprobanteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(188, 6);
            // 
            // configuracionToolStripMenuItem
            // 
            this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
            this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.configuracionToolStripMenuItem.Text = "Configuracion";
            this.configuracionToolStripMenuItem.Click += new System.EventHandler(this.configuracionToolStripMenuItem_Click);
            // 
            // articuloToolStripMenuItem
            // 
            this.articuloToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeArticuloToolStripMenuItem,
            this.nuevoArticuloToolStripMenuItem,
            this.toolStripMenuItem2,
            this.marcaToolStripMenuItem,
            this.rubroToolStripMenuItem,
            this.toolStripMenuItem4,
            this.listaDePreciosToolStripMenuItem});
            this.articuloToolStripMenuItem.Name = "articuloToolStripMenuItem";
            this.articuloToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.articuloToolStripMenuItem.Text = "Articulo";
            // 
            // consultaDeArticuloToolStripMenuItem
            // 
            this.consultaDeArticuloToolStripMenuItem.Name = "consultaDeArticuloToolStripMenuItem";
            this.consultaDeArticuloToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.consultaDeArticuloToolStripMenuItem.Text = "Consulta de Articulo";
            this.consultaDeArticuloToolStripMenuItem.Click += new System.EventHandler(this.consultaDeArticuloToolStripMenuItem_Click);
            // 
            // nuevoArticuloToolStripMenuItem
            // 
            this.nuevoArticuloToolStripMenuItem.Name = "nuevoArticuloToolStripMenuItem";
            this.nuevoArticuloToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.nuevoArticuloToolStripMenuItem.Text = "Nuevo Articulo";
            this.nuevoArticuloToolStripMenuItem.Click += new System.EventHandler(this.nuevoArticuloToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(179, 6);
            // 
            // marcaToolStripMenuItem
            // 
            this.marcaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeMarcaToolStripMenuItem,
            this.nuevaMarcaToolStripMenuItem});
            this.marcaToolStripMenuItem.Name = "marcaToolStripMenuItem";
            this.marcaToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.marcaToolStripMenuItem.Text = "Marca";
            // 
            // consultaDeMarcaToolStripMenuItem
            // 
            this.consultaDeMarcaToolStripMenuItem.Name = "consultaDeMarcaToolStripMenuItem";
            this.consultaDeMarcaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.consultaDeMarcaToolStripMenuItem.Text = "Consulta de Marca";
            this.consultaDeMarcaToolStripMenuItem.Click += new System.EventHandler(this.consultaDeMarcaToolStripMenuItem_Click);
            // 
            // nuevaMarcaToolStripMenuItem
            // 
            this.nuevaMarcaToolStripMenuItem.Name = "nuevaMarcaToolStripMenuItem";
            this.nuevaMarcaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.nuevaMarcaToolStripMenuItem.Text = "Nueva Marca";
            this.nuevaMarcaToolStripMenuItem.Click += new System.EventHandler(this.nuevaMarcaToolStripMenuItem_Click);
            // 
            // rubroToolStripMenuItem
            // 
            this.rubroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeRubroToolStripMenuItem,
            this.nuevoRubroToolStripMenuItem,
            this.toolStripMenuItem3,
            this.subRibroToolStripMenuItem});
            this.rubroToolStripMenuItem.Name = "rubroToolStripMenuItem";
            this.rubroToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.rubroToolStripMenuItem.Text = "Rubro";
            // 
            // consultaDeRubroToolStripMenuItem
            // 
            this.consultaDeRubroToolStripMenuItem.Name = "consultaDeRubroToolStripMenuItem";
            this.consultaDeRubroToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.consultaDeRubroToolStripMenuItem.Text = "Consulta de Rubro";
            this.consultaDeRubroToolStripMenuItem.Click += new System.EventHandler(this.consultaDeRubroToolStripMenuItem_Click);
            // 
            // nuevoRubroToolStripMenuItem
            // 
            this.nuevoRubroToolStripMenuItem.Name = "nuevoRubroToolStripMenuItem";
            this.nuevoRubroToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.nuevoRubroToolStripMenuItem.Text = "Nuevo Rubro";
            this.nuevoRubroToolStripMenuItem.Click += new System.EventHandler(this.nuevoRubroToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(169, 6);
            // 
            // subRibroToolStripMenuItem
            // 
            this.subRibroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaDeSubRubroToolStripMenuItem,
            this.nuevoSubRubroToolStripMenuItem});
            this.subRibroToolStripMenuItem.Name = "subRibroToolStripMenuItem";
            this.subRibroToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.subRibroToolStripMenuItem.Text = "Sub-Rubro";
            // 
            // consultaDeSubRubroToolStripMenuItem
            // 
            this.consultaDeSubRubroToolStripMenuItem.Name = "consultaDeSubRubroToolStripMenuItem";
            this.consultaDeSubRubroToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.consultaDeSubRubroToolStripMenuItem.Text = "Consulta de Sub-Rubro";
            this.consultaDeSubRubroToolStripMenuItem.Click += new System.EventHandler(this.consultaDeSubRubroToolStripMenuItem_Click);
            // 
            // nuevoSubRubroToolStripMenuItem
            // 
            this.nuevoSubRubroToolStripMenuItem.Name = "nuevoSubRubroToolStripMenuItem";
            this.nuevoSubRubroToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.nuevoSubRubroToolStripMenuItem.Text = "Nuevo Sub-Rubro";
            this.nuevoSubRubroToolStripMenuItem.Click += new System.EventHandler(this.nuevoSubRubroToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(179, 6);
            // 
            // listaDePreciosToolStripMenuItem
            // 
            this.listaDePreciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaListaPrecioToolStripMenuItem,
            this.nuevaListaPrecioToolStripMenuItem});
            this.listaDePreciosToolStripMenuItem.Name = "listaDePreciosToolStripMenuItem";
            this.listaDePreciosToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.listaDePreciosToolStripMenuItem.Text = "Lista de Precios";
            // 
            // consultaListaPrecioToolStripMenuItem
            // 
            this.consultaListaPrecioToolStripMenuItem.Name = "consultaListaPrecioToolStripMenuItem";
            this.consultaListaPrecioToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.consultaListaPrecioToolStripMenuItem.Text = "Consulta Lista Precio";
            this.consultaListaPrecioToolStripMenuItem.Click += new System.EventHandler(this.consultaListaPrecioToolStripMenuItem_Click);
            // 
            // nuevaListaPrecioToolStripMenuItem
            // 
            this.nuevaListaPrecioToolStripMenuItem.Name = "nuevaListaPrecioToolStripMenuItem";
            this.nuevaListaPrecioToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.nuevaListaPrecioToolStripMenuItem.Text = "Nueva Lista Precio";
            this.nuevaListaPrecioToolStripMenuItem.Click += new System.EventHandler(this.nuevaListaPrecioToolStripMenuItem_Click);
            // 
            // pnlBusqueda
            // 
            this.pnlBusqueda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlBusqueda.Controls.Add(this.ctrolEmpleado);
            this.pnlBusqueda.Controls.Add(this.pnlBorde);
            this.pnlBusqueda.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBusqueda.Location = new System.Drawing.Point(0, 24);
            this.pnlBusqueda.Name = "pnlBusqueda";
            this.pnlBusqueda.Size = new System.Drawing.Size(932, 49);
            this.pnlBusqueda.TabIndex = 8;
            // 
            // ctrolEmpleado
            // 
            this.ctrolEmpleado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ctrolEmpleado.Dock = System.Windows.Forms.DockStyle.Right;
            this.ctrolEmpleado.Location = new System.Drawing.Point(518, 0);
            this.ctrolEmpleado.Name = "ctrolEmpleado";
            this.ctrolEmpleado.Size = new System.Drawing.Size(414, 45);
            this.ctrolEmpleado.TabIndex = 5;
            // 
            // pnlBorde
            // 
            this.pnlBorde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pnlBorde.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBorde.Location = new System.Drawing.Point(0, 45);
            this.pnlBorde.Name = "pnlBorde";
            this.pnlBorde.Size = new System.Drawing.Size(932, 4);
            this.pnlBorde.TabIndex = 4;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlMenu.Controls.Add(this.menuAccesoRapido);
            this.pnlMenu.Controls.Add(this.pnlDatosEmpresa);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 73);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(221, 388);
            this.pnlMenu.TabIndex = 9;
            // 
            // menuAccesoRapido
            // 
            this.menuAccesoRapido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuAccesoRapido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuAccesoRapido.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.menuAccesoRapido.ImageScalingSize = new System.Drawing.Size(50, 50);
            this.menuAccesoRapido.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnVentaSalon,
            this.btnDelivery,
            this.toolStripButton1});
            this.menuAccesoRapido.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menuAccesoRapido.Location = new System.Drawing.Point(0, 134);
            this.menuAccesoRapido.Name = "menuAccesoRapido";
            this.menuAccesoRapido.Padding = new System.Windows.Forms.Padding(3);
            this.menuAccesoRapido.Size = new System.Drawing.Size(221, 254);
            this.menuAccesoRapido.TabIndex = 4;
            this.menuAccesoRapido.Text = "toolStrip1";
            // 
            // btnVentaSalon
            // 
            this.btnVentaSalon.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVentaSalon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnVentaSalon.Image = ((System.Drawing.Image)(resources.GetObject("btnVentaSalon.Image")));
            this.btnVentaSalon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentaSalon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnVentaSalon.Name = "btnVentaSalon";
            this.btnVentaSalon.Padding = new System.Windows.Forms.Padding(5);
            this.btnVentaSalon.Size = new System.Drawing.Size(214, 64);
            this.btnVentaSalon.Text = "Salón";
            this.btnVentaSalon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentaSalon.Click += new System.EventHandler(this.btnVentaSalon_Click);
            // 
            // btnDelivery
            // 
            this.btnDelivery.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelivery.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelivery.Image = ((System.Drawing.Image)(resources.GetObject("btnDelivery.Image")));
            this.btnDelivery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelivery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelivery.Name = "btnDelivery";
            this.btnDelivery.Padding = new System.Windows.Forms.Padding(5);
            this.btnDelivery.Size = new System.Drawing.Size(214, 64);
            this.btnDelivery.Text = "Delivery";
            this.btnDelivery.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelivery.Click += new System.EventHandler(this.btnDelivery_Click);
            // 
            // pnlDatosEmpresa
            // 
            this.pnlDatosEmpresa.Controls.Add(this.imgEmpresa);
            this.pnlDatosEmpresa.Controls.Add(this.lblEmpresa);
            this.pnlDatosEmpresa.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDatosEmpresa.Location = new System.Drawing.Point(0, 0);
            this.pnlDatosEmpresa.Name = "pnlDatosEmpresa";
            this.pnlDatosEmpresa.Size = new System.Drawing.Size(221, 134);
            this.pnlDatosEmpresa.TabIndex = 0;
            // 
            // imgEmpresa
            // 
            this.imgEmpresa.Location = new System.Drawing.Point(72, 6);
            this.imgEmpresa.Name = "imgEmpresa";
            this.imgEmpresa.Size = new System.Drawing.Size(70, 70);
            this.imgEmpresa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgEmpresa.TabIndex = 1;
            this.imgEmpresa.TabStop = false;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblEmpresa.Location = new System.Drawing.Point(0, 76);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(221, 58);
            this.lblEmpresa.TabIndex = 0;
            this.lblEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButton1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(214, 54);
            this.toolStripButton1.Text = "Kiosco";
            this.toolStripButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(932, 461);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pnlBusqueda);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "X-Commerce";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlBusqueda.ResumeLayout(false);
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            this.menuAccesoRapido.ResumeLayout(false);
            this.menuAccesoRapido.PerformLayout();
            this.pnlDatosEmpresa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgEmpresa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem administracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultaDeEmpleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoEmpleadoToolStripMenuItem;
        private System.Windows.Forms.Panel pnlBusqueda;
        private Core.Controles.ctrolEmpleadoLogin ctrolEmpleado;
        private System.Windows.Forms.Panel pnlBorde;
        private System.Windows.Forms.ToolStripMenuItem seguridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem condicionDeIvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeCondicionIvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaCondiciónIvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empresaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaEmpresaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaEmpresaToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlDatosEmpresa;
        private System.Windows.Forms.PictureBox imgEmpresa;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ToolStrip menuAccesoRapido;
        private System.Windows.Forms.ToolStripButton btnVentaSalon;
        private System.Windows.Forms.ToolStripMenuItem salaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultaDeSalasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaSalaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mesaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copnsultaDeMesaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaMesaToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnDelivery;
        private System.Windows.Forms.ToolStripMenuItem formulariosVentanasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaPerfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoPerfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipoDeComprobanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaTipoComprobanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoTipoDeComprobanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem articuloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeArticuloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoArticuloToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem marcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rubroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeRubroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoRubroToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem subRibroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeSubRubroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoSubRubroToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem listaDePreciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaListaPrecioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaListaPrecioToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem depositoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDepositoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoDepositoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarEmpleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitarEmpleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}

